import java.util.Arrays;

public class Array {
    //attributes
    private int[] array;

    //constructors
    public Array() {
        this.array = new int[0];
    }

    public Array(int[] array) {
        this.array = array;
    }

    //methods
    public int[] toArray() {
        return this.array;
    }

    public int[] copyArray() {
        int[] arrayCopy = new int[this.array.length];
        int index = 0;
        for (int element : this.array) {
            arrayCopy[index] = element;
        }
        return arrayCopy;
    }

    public void add(int element) {
        int[] newArray = new int[this.array.length + 1];
        System.arraycopy(this.array, 0, newArray, 0, this.array.length);
        int lastPosition = newArray.length - 1;
        newArray[lastPosition] = element;
        this.array = newArray;
    }

    public void removeElement(int element) {
        int[] newArray = new int[this.array.length - 1];
        int eventCount = 0;
        for (int index = 0; index < this.array.length; index++) {
            if( this.array[index] == element && eventCount == 0) {
                eventCount++;
            }
            else {
                newArray[index - eventCount] = this.array[index];
            }
        }
        this.array = newArray;
    }

    public void removeIndex(int index) {
        int[] newArray = new int[this.array.length - 1];
        int eventCount = 0;
        for (int i = 0; i < this.array.length; i++) {
            if(  i == index && eventCount == 0) {
                eventCount++;
            }
            else {
                newArray[i - eventCount] = this.array[i];
            }
        }
        this.array = newArray;
    }

    public int select(int index) {
        return this.array[index];
    }

    public int length() {
        return this.array.length;
    }

    public int max() {
        int max = this.array[0];
        for (int i = 1; i < this.array.length; i++) {
            if (this.array[i] > max) {
                max = this.array[i];
            }
        }
        return max;
    }

    public int min() {
        int min = this.array[0];
        for (int i = 1; i < this.array.length; i++) {
            if (this.array[i] < min) {
                min = this.array[i];
            }
        }
        return min;
    }

    public double average() {
        double sum = 0.0;
        for (int element : array) {
            sum += element;
        }
        return sum / this.array.length;
    }

    public void selectEven() {
        int[] newArray = new int[this.array.length];
        int index = 0;
        for (int element : this.array) {
            if (element % 2 == 0) {
                newArray[index] = element;
                index++;
            }
        }
        this.array = newArray;
        this.array = truncate(index, this.array);
    }

    public void selectOdd() {
        int[] newArray = new int[this.array.length];
        int index = 0;
        for (int element : this.array) {
            if (element % 2 != 0) {
                newArray[index] = element;
                index++;
            }
        }
        this.array = newArray;
        this.array = truncate(index, this.array);
    }

    public void selectMultiples(int number) {
        int[] newArray = new int[this.array.length];
        int index = 0;
        for (int element : this.array) {
            if (element % number == 0) {
                newArray[index] = element;
                index++;
            }
        }
        this.array = newArray;
        this.array = truncate(index, this.array);
    }

    private int[] truncate(int length, int[] array) {
        int[] newArray = new int[length];
        if (length >= 0) System.arraycopy(array, 0, newArray, 0, length);
        return newArray;
    }

    public void sort(String direction) {
        int n = 0;
        int eventCount = -1;
        while (n < this.array.length - 1 && eventCount != 0) {
            eventCount = 0;
            for (int i = 0; i + 1 < array.length; i++) {
                if (direction.equals("ascending")) {
                    if (this.array[i] > this.array[i + 1]) {
                        Array.this.swap(i, i + 1);
                        eventCount++;
                    }
                }
                else if (direction.equals("descending")) {
                    if (this.array[i] < this.array[i + 1]) {
                        Array.this.swap(i, i + 1);
                        eventCount++;
                    }
                }
            }
            n++;
        }
    }

    private void swap(int index1, int index2) {
        int temp = this.array[index2];
        this.array[index2] = this.array[index1];
        this.array[index1] = temp;
    }

    public boolean isSize(int length) {
        boolean isSize = false;
        if(this.array.length == length) {
            isSize = true;
        }
        return isSize;
    }

    public boolean isEven() {
        boolean isEven = true;
        for (int element : array) {
            if (element % 2 != 0) {
                isEven = false;
                break;
            }
        }
        return isEven;
    }

    public boolean isOdd() {
        boolean isOdd = true;
        for (int element : array) {
            if (element % 2 == 0) {
                isOdd = false;
                break;
            }
        }
        return isOdd;
    }

    public boolean isRepeated() {
        boolean isRepeated = false;
        for (int index1 = 0; index1 < this.array.length - 1; index1++) {
            for (int index2 = index1 + 1; index2 < this.array.length; index2++) {
                if (this.array[index1] == this.array[index2]) {
                    isRepeated = true;
                    break;
                }
            }
        }
        return isRepeated;
    }

    public int[] elementsWhoseDigitCountIsSuperiorThanAverageDigitCount() {
        int[] selectedElements = new int[array.length];
        int index = 0;
        for (int element : this.array) {
            if (digitCount(element) > averageDigitCount()) {
                selectedElements[index] = element;
                index++;
            }
        }
        return truncate(index, selectedElements);
    }

    private double averageDigitCount() {
        double sum = 0.0;
        for (int element : this.array) {
            sum += digitCount(element);
        }
        return sum / this.array.length;
    }

    private int digitCount(int number) {
        String digits = String.valueOf(number);
        return digits.length();
    }

    public int[] elementsWhichPercentageOfEvenDigitsOfElementIsSuperiorThanAverageOfPercentageOfEvenDigitsOfAllElements() {
        int[] selectedElements = new int[this.array.length];
        int index = 0;
        for (int element : this.array) {
            if (percentageOfEvenDigitsOfElement(element) > averageOfPercentageOfEvenDigitsOfAllElements(this.array)) {
                selectedElements[index] = element;
                index++;
            }
        }
        return truncate(index, selectedElements);
    }

    private double percentageOfEvenDigitsOfElement(int number) {
        String digits = String.valueOf(number);
        double evenCount = 0;
        for (int digit = 0; digit < digits.length(); digit++) {
            if (Character.getNumericValue(digits.charAt(digit)) % 2 == 0) {
                evenCount++;
            }
        }
        return (evenCount / digits.length()) * 100;
    }

    private double averageOfPercentageOfEvenDigitsOfAllElements(int[] array) {
        double sum = 0.0;
        for (int element : array) {
            sum += percentageOfEvenDigitsOfElement(element);
        }
        return sum / array.length;
    }

    public int[] elementsComposedByEvenDigits() {
        int[] evenDigitsElement = new int[this.array.length];
        int index = 0;
        for (int i : this.array) {
            String digits = String.valueOf(i);
            int evenCount = 0;
            for (int digit = 0; digit < digits.length(); digit++) {
                if (Character.getNumericValue(digits.charAt(digit)) % 2 == 0) {
                    evenCount++;
                }
            }
            if (evenCount == digits.length()) {
                evenDigitsElement[index] = i;
                index++;
            }
        }
        return truncate(index, evenDigitsElement);
    }

    public int[] elementsThatAreAscendingSequences() {
        int[] selectedElements = new int[this.array.length];
        int index = 0;
        for (int element : this.array) {
            if (isAscendingSequence(element)) {
                selectedElements[index] = element;
                index++;
            }
        }
        return  truncate(index, selectedElements);
    }

    private boolean isAscendingSequence(int number) {
        boolean isAscendingSequence = false;
        String digits = String.valueOf(number);
        if(digits.length() != 1) {
            int eventCount = 0;
            for (int digitIndex = 0; digitIndex < digits.length() - 1; digitIndex++) {
                if (digits.charAt(digitIndex) < digits.charAt(digitIndex + 1)) {
                    eventCount++;
                }
            }
            if (eventCount == digits.length() - 1) {
                isAscendingSequence = true;
            }
        }
        return isAscendingSequence;
    }

    public int[] elementsThatArePalindromes() {
        int[] selectedElements = new int[this.array.length];
        int index = 0;
        for (int element : this.array) {
            if (isPalindrome(element)) {
                selectedElements[index] = element;
                index++;
            }
        }
        return  truncate(index, selectedElements);
    }

    private boolean isPalindrome(int number) {
        boolean isPalindrome = false;
        String digits = String.valueOf(number);
        int eventCount = 0;
        if (digits.length() != 1) {
            for (int digitIndex = 0; digitIndex < digits.length() / 2; digitIndex++) {
                if (digits.charAt(digitIndex) == digits.charAt(digits.length() - 1 - digitIndex)) {
                    eventCount++;
                }
            }
            if (eventCount == digits.length() / 2) {
                isPalindrome = true;
            }
        }
        else {
            isPalindrome = true;
        }
        return isPalindrome;
    }

    public int[] elementsThatAreEqualDigits() {
        int[] selectedElements = new int[this.array.length];
        int index = 0;
        for (int element : this.array) {
            if (isEqualDigits(element)) {
                selectedElements[index] = element;
                index++;
            }
        }
        return  truncate(index, selectedElements);
    }

    private boolean isEqualDigits (int number) {
        boolean isEqualDigits = false;
        String digits = String.valueOf(number);
        if (digits.length() != 1) {
            int eventCount = 0;
            for (int digitIndex = 0; digitIndex < digits.length() - 1; digitIndex++) {
                if (digits.charAt(digitIndex) == digits.charAt(digitIndex + 1)) {
                    eventCount++;
                }
            }
            if (eventCount == digits.length() - 1) {
                isEqualDigits = true;
            }
        }
        else {
            isEqualDigits = true;
        }
        return isEqualDigits;
    }

    public int[] elementsThatAreNotAmstrong() {
        int[] selectedElements = new int[this.array.length];
        int index = 0;
        for (int element : this.array) {
            if (!isAmstrong(element)) {
                selectedElements[index] = element;
                index++;
            }
        }
        return  truncate(index, selectedElements);
    }


    private boolean isAmstrong(int number) {
        boolean isAmstrong = false;
        String digits = String.valueOf(number);
        int sum = 0;
        for (int digitIndex = 0; digitIndex < digits.length(); digitIndex++) {
            sum += Math.pow(Character.getNumericValue(digits.charAt(digitIndex)), digits.length());
        }
        if (sum == number) {
            isAmstrong = true;
        }
        return isAmstrong;
    }

    public int[] elementsThatAreAscendingSequencesOfDeterminedLength(int length) {
        int[] selectedElements = new int[this.array.length];
        int index = 0;
        for (int element : this.array) {
            if (isAscendingSequenceOfDeterminedLength(element, length)) {
                selectedElements[index] = element;
                index++;
            }
        }
        return truncate(index, selectedElements);
    }


    private boolean isAscendingSequenceOfDeterminedLength(int number, int length) {
        boolean isAscendingSequence = false;
        String digits = String.valueOf(number);
        if(digits.length() != 1) {
            int eventCount = 0;
            for (int digitIndex = 0; digitIndex < digits.length() - 1; digitIndex++) {
                if (digits.charAt(digitIndex) < digits.charAt(digitIndex + 1)) {
                    eventCount++;
                }
            }
            if (eventCount == digits.length() - 1 && eventCount >= length - 1) {
                isAscendingSequence = true;
            }
        }
        return isAscendingSequence;
    }

    public boolean isEqual(int[] otherArray) {
        boolean isEqual = false;
        if (Arrays.equals(this.array, otherArray)) {
            isEqual = true;
        }
        return isEqual;
    }
}
