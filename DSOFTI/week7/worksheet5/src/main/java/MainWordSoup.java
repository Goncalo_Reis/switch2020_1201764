public class MainWordSoup {

    public static void main(String[] args) {
        int[][] tunaCoordinates = {{0, 9}, {1, 8}, {2, 7}, {3, 6}};
        Word tuna = new Word("tuna", tunaCoordinates);
        int[][] officeCoordinates = {{1, 6}, {2, 6}, {3, 6}, {4, 6}, {5, 6}, {6, 6}};
        Word office = new Word("office", officeCoordinates);
        int[][] computerCoordinates = {{8, 9}, {7, 9}, {6, 9}, {5, 9}, {4, 9}, {3, 9}, {2, 9}, {1, 9}};
        Word computer = new Word("computer", computerCoordinates);
        Player player = new Player();
        char[][] gameMatrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                                {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                                {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                                {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                                {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                                {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                                {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                                {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                                {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                                {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        Word[] solutions = {tuna, office, computer};
        WordSoup wordSoup = new WordSoup(gameMatrix, solutions);

        while(!wordSoup.isGameWon(player)) {
            wordSoup.displayMatrix();
            player.play();
            if (wordSoup.isValidPlay(player)) {
                wordSoup.eraseWord(player);
                wordSoup.updateSuccessfulPlaysCount(player);
                System.out.println("word discovered!");
            }
        }
        System.out.println("Game finished!");
    }

}
