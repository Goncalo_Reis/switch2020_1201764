public class Word {

    private String content;
    private int[][] wordCoordinates;

    public Word() {}

    public Word(String content, int[][] coordinates) {
        this.content = content;
        this.wordCoordinates = coordinates;
    }

    public int[][] getWordCoordinates() {
        int[][] copyCoordinates = new int[this.wordCoordinates.length][2];
        int index = 0;
        for (int[] letterCoordinates : wordCoordinates) {
            copyCoordinates[index] = letterCoordinates;
        }
        return copyCoordinates;
    }

    public String getWordContent() {
        String wordContent = this.content;
        return wordContent;
    }

    public int[] getStartLetterCoordinates() {
        int[] startLetterCoordinates = new int[2];
        startLetterCoordinates[0] = this.wordCoordinates[0][0];
        startLetterCoordinates[1] = this.wordCoordinates[0][1];
        return startLetterCoordinates;
    }

    public int[] getEndLetterCoordinates() {
        int[] endLetterCoordinates = new int[2];
        endLetterCoordinates[0] = this.wordCoordinates[this.wordCoordinates.length - 1][0];
        endLetterCoordinates[1] = this.wordCoordinates[this.wordCoordinates.length - 1][1];
        return endLetterCoordinates;
    }

}
