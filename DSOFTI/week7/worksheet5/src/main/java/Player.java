import java.util.Scanner;

public class Player {

    private int[][] currentPlay;

    public Player() {}

    public void play() {
        int[][] startAndEndCoords = new int[2][2];
        Scanner read = new Scanner(System.in);
        System.out.println("Enter the row of the starter letter.");
        startAndEndCoords[0][0] = read.nextInt();
        System.out.println("Enter the column of the starter letter.");
        startAndEndCoords[0][1] = read.nextInt();
        System.out.println("Enter the row of the end letter.");
        startAndEndCoords[1][0] = read.nextInt();
        System.out.println("Enter the column of the end letter.");
        startAndEndCoords[1][1] = read.nextInt();
        this.currentPlay = startAndEndCoords;
    }

    public int[][] getCurrentPlay() {
        int[][] copyCurrentPlay = new int[2][2];
        copyCurrentPlay[0][0] = this.currentPlay[0][0];
        copyCurrentPlay[0][1] = this.currentPlay[0][1];
        copyCurrentPlay[1][0] = this.currentPlay[1][0];
        copyCurrentPlay[1][1] = this.currentPlay[1][1];
        return copyCurrentPlay;
    }

}
