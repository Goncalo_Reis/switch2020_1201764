public class SudokuGrid {

    private SudokuCell[][] gameGrid;

    public SudokuGrid() {
        SudokuCell[][] gameGrid = new SudokuCell[9][9];
        this.gameGrid = gameGrid;
    }

    public void enterCellAt(SudokuCell cell, int row, int column) {
        this.gameGrid[row][column] = cell;
    }

    public void setup(int[][] seedGrid) {
        for (int i = 0; i < seedGrid.length; i++) {
            for (int j = 0; j < seedGrid[i].length; j++) {
                SudokuCell cell = new SudokuCell();
                cell.enterContent(seedGrid[i][j]);
                cell.setFixed();
                cell.determineSection(i, j);
                enterCellAt(cell, i, j);
            }
        }
        displayGameGrid();
    }


    public void displayGameGrid() {
        for (SudokuCell[] row : this.gameGrid) {
            System.out.print("\n-------------------\n|");
            for (SudokuCell cell : row) {
                if (cell.getContent() == 0) {
                    System.out.print(" |");
                }
                else {
                    System.out.print(cell.getContent() + "|");
                }
            }
        }
        System.out.print("\n-------------------\n");
    }

    public boolean isGameWon() {
        boolean isGameWon = true;
        for (SudokuCell[] row : gameGrid) {
            for (SudokuCell cell : row) {
                if (cell.getContent() == 0) {
                    isGameWon = false;
                    break;
                }
            }
        }
        return isGameWon;
    }

    public SudokuCell[][] getGameGrid() {
        SudokuCell[][] gameGrid = this.gameGrid;
        return gameGrid;
    }
}
