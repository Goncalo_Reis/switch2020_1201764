import java.util.Scanner;

public class SudokuPlayer {

    private int isPlayOrErase;
    private int[] coordinates;
    private int playValue;

    public SudokuPlayer() {
        this.coordinates = new int[2];
        this.coordinates = new int[2];
    }

    public void setPlayOrErase() {
        Scanner read = new Scanner(System.in);
        System.out.println("Enter 0 if you wish to play or 1 if you wish to erase a previous play.");
        this.isPlayOrErase = read.nextInt();
        if (this.isPlayOrErase == 1) {
            this.playValue = 0;
        }
    }

    public void scanPlayOrErase() {
        Scanner read = new Scanner(System.in);
        if (isPlayOrErase == 0) {
            System.out.println("Enter the desired value from 1 to 9");
            this.playValue = read.nextInt();
            if (this.playValue <= 0 || this.playValue >= 10) {
                throw new IllegalArgumentException("Enter a value between 1 and 9.");
            }
            System.out.println("Enter the row you wish to enter the value in.");
            this.coordinates[0] = read.nextInt();
            System.out.println("Enter the column you wish to enter the value in.");
        }
        else {
            System.out.println("Enter the row you wish to delete the value from.");
            this.coordinates[0] = read.nextInt();
            System.out.println("Enter the column you wish to delete the value from.");
        }
        this.coordinates[1] = read.nextInt();
    }

    public int getPlayValue() {
        int playValue = this.playValue;
        return playValue;
    }

    public int getRowCoord() {
        int rowCoordOfPlay = this.coordinates[0];
        return rowCoordOfPlay;
    }

    public int getColumnCoord() {
        int columnCoordOfPlay = this.coordinates[1];
        return columnCoordOfPlay;
    }

    public int getIsPlayOrErase() {
        int isPlayOrErase = this.isPlayOrErase;
        return isPlayOrErase;
    }



}
