public class BiArray {

    private int[][] biArray;

    public BiArray() {
        this.biArray = new int[0][];
    }

    public BiArray(int[][] biArray) {
        this.biArray = new int[biArray.length][];
        for (int i = 0; i < biArray.length; i++) {
            this.biArray[i] = new int[biArray[i].length];
            System.arraycopy(biArray[i], 0, this.biArray[i], 0, biArray[i].length);
        }
    }

    public int[][] toBiArray() {
       int[][] newBiArray = new int[this.biArray.length][];
       for (int i = 0; i < this.biArray.length; i++) {
           newBiArray[i] = new int[this.biArray[i].length];
           System.arraycopy(this.biArray[i], 0, newBiArray[i], 0, this.biArray[i].length);
       }
       return newBiArray;
    }

    public void appendAt(int index, int value) {
        int[][] newBiArray = new int[this.biArray.length][];
        for (int i = 0; i < this.biArray.length; i++) {
            if (i == index) {
                newBiArray[i] = new int[this.biArray[i].length + 1];
                newBiArray[index][newBiArray[index].length - 1] = value;
            }
            else {
                newBiArray[i] = new int[this.biArray[i].length];
            }
            System.arraycopy(this.biArray[i], 0, newBiArray[i], 0, this.biArray.length);
        }
        this.biArray = newBiArray;
    }

    public void remove(int value) {
        int[][] newBiArray = new int[this.biArray.length][];
        int[] elementToRemoveCoords = getIndexToBeRemoved(value);
        int indexI = -1;
        for (int i = 0; i < this.biArray.length; i++) {
            if (i != elementToRemoveCoords[0]) {
                newBiArray[i] = new int[this.biArray[i].length];
            }
            else {
                newBiArray[i] = new int[this.biArray[i].length - 1];
            }
            int indexJ = 0;
            indexI++;
            for (int j = 0; j < this.biArray[i].length; j++) {
                if (i != elementToRemoveCoords[0] || j != elementToRemoveCoords[1]) {
                    newBiArray[indexI][indexJ] = this.biArray[i][j];
                    indexJ++;
                }
            }
        }
        this.biArray = newBiArray;
    }

    private int[] getIndexToBeRemoved(int value) {
        int[] elementToRemoveCoords = new int[2];
        boolean elementFound = false;
        for (int i = 0; i < this.biArray.length && !elementFound; i++) {
            for (int j = 0; j < this.biArray[i].length && !elementFound; j++) {
                if (value == this.biArray[i][j]) {
                    elementToRemoveCoords[0] = i;
                    elementToRemoveCoords[1] = j;
                    elementFound = true;
                }
            }
        }
        return elementToRemoveCoords;
    }

    public boolean isNull() {
        return this.biArray == null;
    }

    public int max() {
        int max = this.biArray[0][0];
        for (int[] rows : this.biArray) {
            for (int element : rows) {
                if (element > max) {
                    max = element;
                }
            }
        }
        return max;
    }

    public int min() {
        int min = this.biArray[0][0];
        for (int[] rows : this.biArray) {
            for (int element : rows) {
                if (element < min) {
                    min = element;
                }
            }
        }
        return min;
    }

    public double average() {
        double sum = 0;
        int count = 0;
        for (int[] rows : this.biArray) {
            for (int element : rows) {
                    sum += element;
                    count++;
            }
        }
        return sum / count;
    }

    public int[] sumsOfEachLine() {
        int[] sumsOfEachLine = new int[this.biArray.length];
        int index = 0;
        for (int[] rows : biArray) {
            for (int element : rows) {
                sumsOfEachLine[index] += element;
            }
            index++;
        }
        return sumsOfEachLine;
    }

    public int[] sumsOfEachColumn() {
        int[] sumsOfEachColumn = new int[getLongestLineCount()];
        for (int[] rows : biArray) {
            int index = 0;
            for (int element : rows) {
                sumsOfEachColumn[index] += element;
                index++;
            }
        }
        return sumsOfEachColumn;
    }

    private int getLongestLineCount() {
        int longestLine = 0;
        for (int[] rows : this.biArray) {
            int countColumns = 0;
            for (int element : rows) {
                countColumns++;
            }
            if (countColumns > longestLine) {
                longestLine = countColumns;
            }
        }
        return longestLine;
    }

    public int highestLineSumIndex() {
        int[] sumsOfEachLine = sumsOfEachLine();
        int highestLineSumIndex = sumsOfEachLine[0];
        for (int i = 1; i < sumsOfEachLine.length; i++) {
            if (sumsOfEachLine[i] > highestLineSumIndex) {
                highestLineSumIndex = i;
            }
        }
        return highestLineSumIndex;
    }

    public boolean isSquare() {
        boolean isSquare = true;
        int linesCount = this.biArray.length;
        for (int[] rows: this.biArray) {
            int columnsCount = 0;
            for (int element : rows) {
                columnsCount++;
            }
            if (linesCount != columnsCount) {
                isSquare = false;
                break;
            }
        }
        return isSquare;
    }

    public boolean isSymmetric() {
        if(!isSquare()) {
            return false;
        }
        boolean isSymmetric = true;
        for (int i = 0; i < this.biArray.length && isSymmetric; i++) {
            for (int j = 0; j < this.biArray[i].length && isSymmetric; j++) {
                if (this.biArray[i][j] != this.biArray[j][i]) {
                    isSymmetric = false;
                }
            }
        }
        return isSymmetric;
    }

    public int getNullOrMinusOneElementsOfMainDiagonal() {
        int nullOrMinusOneCount = 0;
        int[] mainDiagonal = getMainDiagonal();
        for (int element : mainDiagonal) {
            if (element == 0 || element == -1) {
                nullOrMinusOneCount++;
            }
        }
        return nullOrMinusOneCount;
    }

    private int[] getMainDiagonal() {
        int[] mainDiagonal = new int[this.biArray.length];
        int index = 0;
        for (int i = 0; i < this.biArray.length; i++) {
            for (int j = 0; j < this.biArray[i].length; j++) {
                if (i == j) {
                    mainDiagonal[index] = this.biArray[i][j];
                    index++;
                }
            }
        }
        return mainDiagonal;
    }

    public boolean isMainAndSecondaryDiagonalEqual() {
        boolean isMainAndSecondaryDiagonalEqual = true;
        int[] mainDiagonal = getMainDiagonal();
        int[] secondaryDiagonal = getSecondaryDiagonal();
        for (int i = 0; i < mainDiagonal.length && isMainAndSecondaryDiagonalEqual; i++) {
            if (mainDiagonal[i] != secondaryDiagonal[i]) {
                isMainAndSecondaryDiagonalEqual = false;
            }
        }
        return isMainAndSecondaryDiagonalEqual;
    }

    private int[] getSecondaryDiagonal() {
        int[] secondaryDiagonal = new int[this.biArray.length];
        int index = 0;
        for (int i = 0; i < this.biArray.length; i++) {
            for (int j = 0; j < this.biArray[i].length; j++) {
                if (i == this.biArray[i].length - 1 - j) {
                    secondaryDiagonal[index] = this.biArray[i][j];
                    index++;
                }
            }
        }
        return secondaryDiagonal;

    }

    public int[] valuesHigherThanAverageDigits() {
        int elementsCount = countElements();
        int[] valuesHigherThanAverageDigits = new int[elementsCount];
        double averageDigits = averageDigits();
        int numberDigits;
        int index = 0;
        for (int[] rows : this.biArray) {
            for (int element : rows) {
                numberDigits = String.valueOf(element).length();
                if (numberDigits > averageDigits) {
                    valuesHigherThanAverageDigits[index] = element;
                    index++;
                }
            }
        }
        return truncate(index, valuesHigherThanAverageDigits);
    }

    private double averageDigits() {
        double sum = 0;
        int count = 0;
        String number;
        for (int[] rows : this.biArray) {
            for (int element : rows) {
                number = String.valueOf(element);
                sum += number.length();
                count++;
            }
        }
        return sum / count;
    }

    private int countElements() {
        int count = 0;
        for (int[] rows : this.biArray) {
            for (int element : rows) {
                count++;
            }
        }
        return count;
    }

    private int[] truncate(int length, int[] array) {
        int[] newArray = new int[length];
        if (length >= 0) System.arraycopy(array, 0, newArray, 0, length);
        return newArray;
    }

    public int[] valuesWithHigherEvenPercentageThanAverageOfDigits() {
        int elementsCount = countElements();
        int[] valuesWithHigherEvenPercentageThanAverageOfDigits = new int[elementsCount];
        double averagePercentageEvenDigits = averagePercentageEvenDigits();
        double evenDigitsPercentage;
        int index = 0;
        for (int[] rows : this.biArray) {
            for (int element : rows) {
                evenDigitsPercentage = percentageEvenDigits(element);
                if (evenDigitsPercentage > averagePercentageEvenDigits) {
                    valuesWithHigherEvenPercentageThanAverageOfDigits[index] = element;
                    index++;
                }
            }
        }
        return truncate(index, valuesWithHigherEvenPercentageThanAverageOfDigits);
    }

    private double averagePercentageEvenDigits() {
        double sum = 0;
        int count = 0;
        for (int[] rows : this.biArray) {
            for (int element : rows) {
                sum += percentageEvenDigits(element);
                count++;
            }
        }
        return sum / count;
    }

    private double percentageEvenDigits(int value) {
        String number = String.valueOf(value);
        double evenCount = 0;
        for (int i = 0; i < number.length(); i++) {
            if (number.charAt(i) % 2 == 0) {
                evenCount++;
            }
        }
        return (evenCount / number.length()) * 100;
    }

    public void invertRowsOfMatrix() {
        int[][] invertedMatrixRows = new int[this.biArray.length][];
        for (int i = 0; i < this.biArray.length; i++) {
            invertedMatrixRows[i] = new int[this.biArray[i].length];
            invertedMatrixRows[i] = inverse(this.biArray[i]);
        }
        this.biArray = invertedMatrixRows;
    }

    private int[] inverse(int[] array) {
        int[] inverseArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            inverseArray[inverseArray.length - 1 - i] = array[i];
        }
        return inverseArray;
    }

    public void invertColumnsOfMatrix() {
        int[][] invertedMatrixColumns = new int[this.biArray.length][];
        for (int i = 0; i < this.biArray.length; i++) {
            invertedMatrixColumns[i] = new int[this.biArray[this.biArray.length - 1 - i].length];
            invertedMatrixColumns[i] = this.biArray[this.biArray.length - 1 - i];
        }
        this.biArray = invertedMatrixColumns;
    }

    public void rotateNinetyDegrees() {
        int[][] rotatedMatrix = new int[this.biArray.length][];
        for (int i = 0; i < this.biArray.length; i++) {
            rotatedMatrix[i] = new int[this.biArray[i].length];
        }
        for (int i = 0; i < this.biArray.length; i++) {
            for (int j = 0; j < this.biArray[i].length; j++) {
                rotatedMatrix[j][this.biArray[i].length - 1 - i] = this.biArray[i][j];
            }
        }
        this.biArray = rotatedMatrix;
    }

    public void rotateOneHundredAndEightyDegrees() {
        rotateNinetyDegrees();
        rotateNinetyDegrees();
    }

    public void rotateMinusNinetyDegrees() {
        rotateNinetyDegrees();
        rotateNinetyDegrees();
        rotateNinetyDegrees();
    }

}
