public class MainSudoku {

    public static void main(String[] args) {
        int[][] seedGrid = {{0, 0, 0, 8, 3, 0, 0, 7, 9},
                            {6, 0, 0, 0, 0, 0, 0, 0, 3},
                            {0, 0, 2, 9, 0, 0, 0, 1, 0},
                            {0, 1, 0, 0, 4, 0, 0, 9, 0},
                            {9, 0, 0, 3, 0, 0, 0, 0, 0},
                            {0, 0, 4, 5, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 2, 0, 0},
                            {1, 6, 0, 0, 0, 7, 0, 0, 0},
                            {0, 5, 9, 0, 0, 0, 0, 0, 4}};
        SudokuGrid mySudokuGrid = new SudokuGrid();
        mySudokuGrid.setup(seedGrid);
        SudokuPlayer player = new SudokuPlayer();
        while(!mySudokuGrid.isGameWon()) {
            player.setPlayOrErase();
            player.scanPlayOrErase();
            SudokuCell[][] gameGrid = mySudokuGrid.getGameGrid();
            int scannedRow = player.getRowCoord();
            int scannedColumn = player.getColumnCoord();
            if (gameGrid[scannedRow][scannedColumn].isValidPlayOrErase(mySudokuGrid, player)) {
                gameGrid[scannedRow][scannedColumn].setContent(player);
            }
            mySudokuGrid.displayGameGrid();
        }
        System.out.println("You finished the game!");
    }



}
