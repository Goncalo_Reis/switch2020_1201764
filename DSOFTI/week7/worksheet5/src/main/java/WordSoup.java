import java.util.Arrays;

public class WordSoup {

    private char[][] gameMatrix;
    private Word[] solutions;
    private int successfulPlaysCount = 0;

    public WordSoup(char[][] gameMatrix, Word[] solutions) {
        this.gameMatrix = gameMatrix;
        this.solutions = solutions;
    }

    public void displayMatrix() {
        System.out.println("---------------------");
        for (char[] row : gameMatrix){
            for (char character : row) {
                System.out.print("|" + character);
            }
            System.out.println("|");
            System.out.println("---------------------");
        }
    }

    public boolean isValidPlay(Player player) {
        boolean isValidPlay = false;
        int[][] startAndEndCoords = player.getCurrentPlay();
        for (int wordIndex = 0; wordIndex < solutions.length && !isValidPlay; wordIndex++) {
            int[] startCoords = solutions[wordIndex].getStartLetterCoordinates();
            int[] endCoords = solutions[wordIndex].getEndLetterCoordinates();
            if (Arrays.equals(startAndEndCoords[0], startCoords) && Arrays.equals(startAndEndCoords[1], endCoords)) {
                isValidPlay = true;
            }
        }
        return isValidPlay;
    }

    public void updateSuccessfulPlaysCount(Player player) {
        this.successfulPlaysCount++;
    }


    public void eraseWord(Player player) {
        Word word = matchPlayAndSolutions(player);
        int[][] wordCoords = word.getWordCoordinates();
        for (int[] letterCoords : wordCoords) {
            this.gameMatrix[letterCoords[0]][letterCoords[1]] = ' ';
        }
        for (int wordIndex = 0; wordIndex < solutions.length; wordIndex++) {
            if (word.equals(solutions[wordIndex])) {
                solutions[wordIndex] = null;
            }
        }
    }

    private Word matchPlayAndSolutions(Player player) {
        Word word = new Word();
        int[][] startAndEndCoords = player.getCurrentPlay();
        for (int wordIndex = 0; wordIndex < solutions.length; wordIndex++) {
            int[] startCoords = solutions[wordIndex].getStartLetterCoordinates();
            int[] endCoords = solutions[wordIndex].getEndLetterCoordinates();
            if (Arrays.equals(startAndEndCoords[0], startCoords) && Arrays.equals(startAndEndCoords[1], endCoords)) {
                word = solutions[wordIndex];
                break;
            }
        }
        return word;
    }


    public boolean isGameWon(Player player) {
        boolean isGameWon = false;
        if (this.solutions.length == this.successfulPlaysCount) {
            isGameWon = true;
        }
        return isGameWon;
    }

}