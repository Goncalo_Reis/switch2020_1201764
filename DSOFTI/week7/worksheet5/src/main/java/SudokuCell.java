public class SudokuCell {

    private int content;
    private boolean isFixed;
    private int section;

    public int getContent() {
        int content = this.content;
        return content;
    }

    public void setContent(SudokuPlayer player) {
        int content = player.getPlayValue();
        if (!this.isFixed) {
            this.content = content;
        }
    }

    public int getSection() {
        int section = this.section;
        return section;
    }

    public boolean getFixed() {
        return this.isFixed;
    }

    public void enterContent(int content) {
        if (content < 0 || content > 9) {
            throw new IllegalArgumentException("Has to be a value between 0 and 9.");
        }
        this.content = content;
    }

    public void setFixed() {
        boolean isFixed = true;
        if (this.content == 0) {
            isFixed = false;
        }
        this.isFixed = isFixed;
    }

    public int determineSection(int row, int column) {
        int sector = 0;
        if (row >= 0 && row <= 2 && column >= 0 && column <= 2) {
            sector = 1;
        }
        if (row >= 0 && row <= 2 && column >= 3 && column <= 5) {
            sector = 2;
        }
        if (row >= 0 && row <= 2 && column >= 6 && column <= 8) {
            sector = 3;
        }
        if (row >= 3 && row <= 5 && column >= 0 && column <= 2) {
            sector = 4;
        }
        if (row >= 3 && row <= 5 && column >= 3 && column <= 5) {
            sector = 5;
        }
        if (row >= 3 && row <= 5 && column >= 6 && column <= 8) {
            sector = 6;
        }
        if (row >= 6 && row <= 8 && column >= 0 && column <= 2) {
            sector = 7;
        }
        if (row >= 6 && row <= 8 && column >= 3 && column <= 5) {
            sector = 8;
        }
        if (row >= 6 && row <= 8 && column >= 6 && column <= 8) {
            sector = 9;
        }
        this.section = sector;
        return sector;
    }

    private boolean isValidPlay(SudokuGrid mySudokuGrid, SudokuPlayer player) {
        return rowRule(mySudokuGrid, player) && columnRule(mySudokuGrid, player) && sectionRule(mySudokuGrid, player);
    }

    private boolean rowRule(SudokuGrid mySudokuGrid, SudokuPlayer player) {
        boolean rowRule = true;
        int rowToCheck = player.getRowCoord();
        int valueToCheck = player.getPlayValue();
        SudokuCell[][] gameGrid = mySudokuGrid.getGameGrid();
        for (int j = 0; j < gameGrid[0].length && rowRule; j++) {
            if (gameGrid[rowToCheck][j].getContent() == valueToCheck) {
                rowRule = false;
            }
        }
        return rowRule;
    }

    private boolean columnRule(SudokuGrid mySudokuGrid, SudokuPlayer player) {
        boolean columnRule = true;
        int columnToCheck = player.getColumnCoord();
        int valueToCheck = player.getPlayValue();
        SudokuCell[][] gameGrid = mySudokuGrid.getGameGrid();
        for (int i = 0; i < gameGrid.length && columnRule; i++) {
            if (gameGrid[i][columnToCheck].getContent() == valueToCheck) {
                columnRule = false;
            }
        }
        return columnRule;
    }

    private boolean sectionRule(SudokuGrid mySudokuGrid, SudokuPlayer player) {
        boolean sectionRule = true;
        int sectionToCheck = determineSection(player.getRowCoord(), player.getColumnCoord());
        int valueToCheck = player.getPlayValue();
        SudokuCell[][] gameGrid = mySudokuGrid.getGameGrid();
        for (int i = 0; i < gameGrid.length; i++) {
            for (int j = 0; j < gameGrid[i].length; j++) {
                if (gameGrid[i][j].getSection() == sectionToCheck) {
                    if (gameGrid[i][j].getContent() == valueToCheck) {
                        sectionRule = false;
                    }
                }
            }
        }
        return sectionRule;
    }

    private boolean isValidErase(SudokuGrid mySudokuGrid, SudokuPlayer player) {
        boolean isValidErase = true;
        int rowToCheck = player.getRowCoord();
        int columnToCheck = player.getColumnCoord();
        SudokuCell[][] gameGrid = mySudokuGrid.getGameGrid();
        if (gameGrid[rowToCheck][columnToCheck].getFixed()) {
            isValidErase = false;
        }
        return isValidErase;
    }

    public boolean isValidPlayOrErase(SudokuGrid mySudokuGrid, SudokuPlayer player) {
        boolean isValidPlayOrErase;
        if (player.getIsPlayOrErase() == 0) {
            isValidPlayOrErase = isValidPlay(mySudokuGrid, player);
        }
        else {
            isValidPlayOrErase = isValidErase(mySudokuGrid, player);
        }
        return isValidPlayOrErase;
    }


}
