import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BiArrayTest {

    @Test
    public void biArrayEmpty() {
        BiArray myBiArray = new BiArray();
        int[][] expected = {};
        assertArrayEquals(expected, myBiArray.toBiArray());
    }

    @Test
    public void biArraySomeValues() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        int[][] expected = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        assertArrayEquals(expected, myBiArray.toBiArray());
    }

    @Test
    public void toBiArray() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        int[][] expected = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int [][] result = myBiArray.toBiArray();
        assertArrayEquals(expected, result);
    }


    @Test
    void appendAt() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        myBiArray.appendAt(2, 10);
        int[][] expected = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9, 10}};
        assertArrayEquals(expected, myBiArray.toBiArray());
    }

    @Test
    void remove_startRow() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        myBiArray.remove(4);
        int[][] expected = {{1, 2, 3}, {5, 6}, {7, 8, 9}};
        assertArrayEquals(expected, myBiArray.toBiArray());
    }

    @Test
    void remove_endRow() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        myBiArray.remove(9);
        int[][] expected = {{1, 2, 3}, {4, 5, 6}, {7, 8}};
        assertArrayEquals(expected, myBiArray.toBiArray());
    }


    @Test
    void isNull_false() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        assertFalse(myBiArray.isNull());
    }

    @Test
    void isNull_false2() {
        BiArray myBiArray = new BiArray();
        assertFalse(myBiArray.isNull());
    }


    @Test
    void max() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        int expected = 9;
        int result = myBiArray.max();
        assertEquals(expected, result);
    }

    @Test
    void min() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        int expected = 1;
        int result = myBiArray.min();
        assertEquals(expected, result);
    }

    @Test
    void average() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        double expected = 5;
        double result = myBiArray.average();
        assertEquals(expected, result);
    }

    @Test
    void sumsOfEachLine() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        int[] expected = {6, 15, 24};
        int[] result = myBiArray.sumsOfEachLine();
        assertArrayEquals(expected, result);
    }

    @Test
    void sumsOfEachColumn() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        int[] expected = {12, 15, 18};
        int[] result = myBiArray.sumsOfEachColumn();
        assertArrayEquals(expected, result);
    }

    @Test
    void highestLineSumIndex() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        int expected = 2;
        int result = myBiArray.highestLineSumIndex();
        assertEquals(expected, result);
    }

    @Test
    void isSquare_true() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        boolean result = myBiArray.isSquare();
        assertTrue(result);
    }

    @Test
    void isSquare_false() {
        int[][] content = {{1, 2, 3}, {4, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        boolean result = myBiArray.isSquare();
        assertFalse(result);
    }

    @Test
    void isSymmetric_true() {
        int[][] content = {{1, 2, 3}, {2, 5, 6}, {3, 6, 9}};
        BiArray myBiArray = new BiArray(content);
        boolean result = myBiArray.isSymmetric();
        assertTrue(result);
    }

    @Test
    void isSymmetric_false() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        boolean result = myBiArray.isSymmetric();
        assertFalse(result);
    }

    @Test
    void getNullOrMinusOneElementsOfMainDiagonal_noElements() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        int expected = 0;
        int result = myBiArray.getNullOrMinusOneElementsOfMainDiagonal();
        assertEquals(expected, result);
    }

    @Test
    void getNullOrMinusOneElementsOfMainDiagonal_allElements() {
        int[][] content = {{0, 0, -1}, {-1, -1, 0}, {0, -1, 0}};
        BiArray myBiArray = new BiArray(content);
        int expected = 3;
        int result = myBiArray.getNullOrMinusOneElementsOfMainDiagonal();
        assertEquals(expected, result);
    }

    @Test
    void isMainAndSecondaryDiagonalEqual_true() {
        int[][] content = {{1, 2, 1}, {2, 5, 6}, {9, 6, 9}};
        BiArray myBiArray = new BiArray(content);
        boolean result = myBiArray.isMainAndSecondaryDiagonalEqual();
        assertTrue(result);
    }

    @Test
    void isMainAndSecondaryDiagonalEqual_false() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        boolean result = myBiArray.isMainAndSecondaryDiagonalEqual();
        assertFalse(result);
    }

    @Test
    void valuesHigherThanAverageDigits() {
        int[][] content = {{1, 24, 3}, {4, 50, 6}, {7, 88, 9}};
        BiArray myBiArray = new BiArray(content);
        int[] expected = {24, 50, 88};
        int[] result = myBiArray.valuesHigherThanAverageDigits();
        assertArrayEquals(expected, result);
    }

    @Test
    void valuesWithHigherEvenPercentageThanAverageOfDigits() {
        int[][] content = {{1, 22, 44}, {66, 77, 88}, {99, 22, 222}};
        BiArray myBiArray = new BiArray(content);
        int[] expected = {22, 44, 66, 88, 22, 222};
        int[] result = myBiArray.valuesWithHigherEvenPercentageThanAverageOfDigits();
        assertArrayEquals(expected, result);
    }

    @Test
    void invertRowsOfMatrix() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        myBiArray.invertRowsOfMatrix();
        int[][] expected = {{3, 2, 1}, {6, 5, 4}, {9, 8, 7}};
        assertArrayEquals(expected, myBiArray.toBiArray());
    }


    @Test
    void invertColumnsOfMatrix() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        myBiArray.invertColumnsOfMatrix();
        int[][] expected = {{7, 8, 9}, {4, 5, 6}, {1, 2, 3}};
        assertArrayEquals(expected, myBiArray.toBiArray());
    }

    @Test
    void rotateNinetyDegrees() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        myBiArray.rotateNinetyDegrees();
        int[][] expected = {{7, 4, 1}, {8, 5, 2}, {9, 6, 3}};
        assertArrayEquals(expected, myBiArray.toBiArray());
    }

    @Test
    void rotateOneHundredAndEightyDegrees() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        myBiArray.rotateOneHundredAndEightyDegrees();
        int[][] expected = {{9, 8, 7}, {6, 5, 4}, {3, 2, 1}};
        assertArrayEquals(expected, myBiArray.toBiArray());
    }

    @Test
    void rotateMinusNinetyDegrees() {
        int[][] content = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        BiArray myBiArray = new BiArray(content);
        myBiArray.rotateMinusNinetyDegrees();
        int[][] expected = {{3, 6, 9}, {2, 5, 8}, {1, 4, 7}};
        assertArrayEquals(expected, myBiArray.toBiArray());
    }




}