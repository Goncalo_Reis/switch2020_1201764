import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayTest {

    @Test
    public void emptyArray() {
        Array myArray = new Array();
        int[] result = myArray.toArray();
        assertEquals(0, result.length);
    }

    @Test
    public void initializedWithZeroValues() {
        int[] content = {};
        Array myArray = new Array(content);
        int[] result = myArray.toArray();
        assertEquals(0, result.length);
    }

    @Test
    public void initializedWithSeveralValues() {
        int[] content = {1, 2, 3};
        Array myArray = new Array(content);
        int[] result = myArray.toArray();
        assertEquals(3, result.length);
    }

    @Test
    public void add() {
        int[] content = {1, 2, 3};
        int element = 5;
        Array myArray = new Array(content);
        myArray.add(element);
        int[] result = myArray.toArray();
        int[] expected = {1, 2, 3, 5};
        assertArrayEquals(expected, result);
    }

    @Test
    public void removeElement() {
        int[] content = {1, 2, 3};
        int element = 2;
        Array myArray = new Array(content);
        myArray.removeElement(element);
        int[] result = myArray.toArray();
        int[] expected = {1, 3};
        assertArrayEquals(expected, result);
    }

    @Test
    public void removeIndex() {
        int[] content = {1, 2, 3};
        int index = 2;
        Array myArray = new Array(content);
        myArray.removeIndex(index);
        int[] result = myArray.toArray();
        int[] expected = {1, 2};
        assertArrayEquals(expected, result);
    }

    @Test
    public void select() {
        int index = 1;
        int[] content = {1, 2, 3};
        Array myArray = new Array(content);
        int expected = 2;
        int result = myArray.select(index);
        assertEquals(expected, result);
    }

    @Test
    public void length() {
        int[] content = {1, 2, 3, 4};
        Array myArray = new Array(content);
        int expected = 4;
        int result = myArray.length();
        assertEquals(expected, result);
    }

    @Test
    public void max() {
        int[] content = {1, 2, 3, 4};
        Array myArray = new Array(content);
        int expected = 4;
        int result = myArray.max();
        assertEquals(expected, result);
    }

    @Test
    public void min() {
        int[] content = {1, 2, 3, 4};
        Array myArray = new Array(content);
        int expected = 1;
        int result = myArray.min();
        assertEquals(expected, result);
    }

    @Test
    public void average() {
        int[] content = {1, 2, 3, 4};
        Array myArray = new Array(content);
        double expected = 2.5;
        double result = myArray.average();
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void averageOfEven() {
        int[] content = {1, 2, 3, 4};
        Array myArray = new Array(content);
        double expected = 3;
        myArray.selectEven();
        double result = myArray.average();
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void averageOfOdd() {
        int[] content = {1, 2, 3, 4};
        Array myArray = new Array(content);
        double expected = 2;
        myArray.selectOdd();
        double result = myArray.average();
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void averageOfMultiples() {
        int[] content = {1, 2, 3, 4};
        Array myArray = new Array(content);
        double expected = 3;
        myArray.selectMultiples(2);
        double result = myArray.average();
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void sortAscending_keepSame() {
        int[] content = {1, 2, 3, 4};
        Array myArray = new Array(content);
        int[] expected = {1, 2, 3, 4};
        String direction = "ascending";
        myArray.sort(direction);
        int[] result = myArray.toArray();
        assertArrayEquals(expected, result);
    }

    @Test
    public void sortAscending_sortValues() {
        int[] content = {5, 4, 3, 2, 1};
        Array myArray = new Array(content);
        int[] expected = {1, 2, 3, 4, 5};
        String direction = "ascending";
        myArray.sort(direction);
        int[] result = myArray.toArray();
        assertArrayEquals(expected, result);
    }

    @Test
    public void sortDescending_keepSame() {
        int[] content = {4, 3, 2, 1};
        Array myArray = new Array(content);
        int[] expected = {4, 3, 2, 1};
        String direction = "descending";
        myArray.sort(direction);
        int[] result = myArray.toArray();
        assertArrayEquals(expected, result);
    }

    @Test
    public void sortDescending_sortValues() {
        int[] content = {1, 2, 3, 4, 5};
        Array myArray = new Array(content);
        int[] expected = {5, 4, 3, 2, 1};
        String direction = "descending";
        myArray.sort(direction);
        int[] result = myArray.toArray();
        assertArrayEquals(expected, result);
    }

    @Test
    public void isEmpty_true() {
        int[] content = {};
        Array myArray = new Array(content);
        assertTrue(myArray.isSize(0));
    }

    @Test
    public void isEmpty_false() {
        int[] content = {1, 2};
        Array myArray = new Array(content);
        assertFalse(myArray.isSize(0));
    }

    @Test
    public void isOneLength_true() {
        int[] content = {1};
        Array myArray = new Array(content);
        assertTrue(myArray.isSize(1));
    }

    @Test
    public void isOneLength_false() {
        int[] content = {1, 2};
        Array myArray = new Array(content);
        assertFalse(myArray.isSize(1));
    }

    @Test
    public void isEven_true() {
        int[] content = {2, 4};
        Array myArray = new Array(content);
        assertTrue(myArray.isEven());
    }

    @Test
    public void isEven_false() {
        int[] content = {2, 4, 5};
        Array myArray = new Array(content);
        assertFalse(myArray.isEven());
    }

    @Test
    public void isOdd_true() {
        int[] content = {1, 3};
        Array myArray = new Array(content);
        assertTrue(myArray.isOdd());
    }

    @Test
    public void isOdd_false() {
        int[] content = {3, 4, 5};
        Array myArray = new Array(content);
        assertFalse(myArray.isOdd());
    }

    @Test
    public void isRepeated_true() {
        int[] content = {1, 2, 1};
        Array myArray = new Array(content);
        assertTrue(myArray.isRepeated());
    }

    @Test
    public void isRepeated_false() {
        int[] content = {3, 4, 5};
        Array myArray = new Array(content);
        assertFalse(myArray.isRepeated());
    }


    @Test
    public void elementsWhoseDigitCountIsSuperiorThanAverageDigitCount() {
        int[] content = {1, 222, 3, 222, 4, 323};
        Array myArray = new Array(content);
        int[] result = myArray.elementsWhoseDigitCountIsSuperiorThanAverageDigitCount();
        int[] expected = {222, 222, 323};
        assertArrayEquals(expected, result);
    }


    @Test
    void elementsWhichPercentageOfEvenDigitsOfElementIsSuperiorThanAverageOfPercentageOfEvenDigitsOfAllElements() {
        int[] content = {12, 13, 14, 5, 222};
        Array myArray = new Array(content);
        int[] result = myArray.elementsWhichPercentageOfEvenDigitsOfElementIsSuperiorThanAverageOfPercentageOfEvenDigitsOfAllElements();
        int[] expected = {12, 14, 222};
        assertArrayEquals(expected, result);
    }


    @Test
    void elementsComposedByEvenDigits() {
        int[] content = {12, 13, 14, 5, 222};
        Array myArray = new Array(content);
        int[] result = myArray.elementsComposedByEvenDigits();
        int[] expected = {222};
        assertArrayEquals(expected, result);
    }


    @Test
    void elementsThatAreAscendingSequences() {
        int[] content = {12, 13, 14, 5, 222};
        Array myArray = new Array(content);
        int[] result = myArray.elementsThatAreAscendingSequences();
        int[] expected = {12, 13, 14};
        assertArrayEquals(expected, result);
    }


    @Test
    void elementsThatArePalindromes() {
        int[] content = {12, 13, 141, 5, 222};
        Array myArray = new Array(content);
        int[] result = myArray.elementsThatArePalindromes();
        int[] expected = {141, 5, 222};
        assertArrayEquals(expected, result);
    }


    @Test
    void elementsThatAreEqualDigits() {
        int[] content = {12, 13, 141, 5, 222};
        Array myArray = new Array(content);
        int[] result = myArray.elementsThatAreEqualDigits();
        int[] expected = {5, 222};
        assertArrayEquals(expected, result);
    }

    @Test
    void elementsThatAreNotAmstrong() {
        int[] content = {12, 13, 141, 5, 222, 371};
        Array myArray = new Array(content);
        int[] result = myArray.elementsThatAreNotAmstrong();
        int[] expected = {12, 13, 141, 222};
        assertArrayEquals(expected, result);
    }


    @Test
    void elementsThatAreAscendingSequencesOfDeterminedLength() {
        int[] content = {12, 13, 141, 5, 222, 379, 1234};
        Array myArray = new Array(content);
        int length = 2;
        int[] result = myArray.elementsThatAreAscendingSequencesOfDeterminedLength(length);
        int[] expected = {12, 13, 379, 1234};
        assertArrayEquals(expected, result);
    }


    @Test
    void isEqual_true() {
        int[] content = {12, 13, 141, 5, 222, 379, 1234};
        Array myArray = new Array(content);
        int[] otherArray = {12, 13, 141, 5, 222, 379, 1234};
        boolean result = myArray.isEqual(otherArray);
        assertTrue(result);
    }

    @Test
    void isEqual_false() {
        int[] content = {12, 13, 141, 5, 222, 379, 1234};
        Array myArray = new Array(content);
        int[] otherArray = {12, 13, 141, 5, 222, 379};
        boolean result = myArray.isEqual(otherArray);
        assertFalse(result);
    }

}