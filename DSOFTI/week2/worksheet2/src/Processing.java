public class Processing {

    public static double exercise1(int note1, int note2, int note3, int weight1, int weight2, int weight3) {

        if (note1 > 20 || note2 > 20 || note3 > 20 || weight1 > 20 || weight2 > 20 || weight3 > 20) {

            return -1;

        }

        if (weight1 + weight2 + weight3 != 20) {

            return -2;

        }

        double weightedAverage = ((double) note1 * weight1 + note2 * weight2 + note3 * weight3) / (weight1 + weight2 + weight3);

        return weightedAverage;

    }


    public static int exercise2_digit3(int number) {

        if (number < 100 || number > 999) {

            return -1;

        } else {

            int digit3;
            digit3 = number % 10;
            return digit3;

        }

    }

    public static int exercise2_digit2(int number) {

        int digit2;
        digit2 = (number / 10) % 10;
        return digit2;

    }

    public static int exercise2_digit1(int number) {

        int digit1;
        digit1 = (number / 100) % 10;
        return digit1;

    }

    public static int exercise2_oddOrEven(int number) {

        if (number % 2 == 0) {

            return 0;

        } else {

            return 1;

        }

    }


    public static double exercise3_getDistance(double x, double y) {

        double d = Math.sqrt(Math.pow(x * 2 - x * 1, 2) + Math.pow(y * 2 - y * 1, 2));
        return d;

    }


    public static double exercise4_getFunctionValue(double x) {

        if (x < 0) {

            return x;

        } else if (x == 0) {

            return 0;

        }

        return (x * 2) - (2 * x);

    }


    public static double exercise5_getVolume(double area) {

        if (area < 0) {

            //incorrect area
            return -1;

        }

        double edge = Math.sqrt(area / 6);
        double volume = (Math.pow(edge, 3)) * 0.001;
        return volume;

    }

    public static String exercise5_getCubeClassification(double volume) {

        if (volume <= 1) {

            return "Small";

        } else if (volume > 1 && volume <= 2) {

            return "Medium";

        } else {

            return "Big";

        }

    }


    public static int exercise6_getHours(int totalSeconds) {

        int hours = totalSeconds / 3600;
        return hours;

    }

    public static int exercise6_getMinutes(int totalSeconds) {

        int minutes = (totalSeconds % 3600) / 60;
        return minutes;

    }

    public static int exercise6_getSeconds(int totalSeconds) {

        int seconds = ((totalSeconds % 3600) % 60);
        return seconds;

    }

    public static String exercise6_getGreeting(int hours, int minutes, int seconds) {

        if (hours >= 6 && hours < 12) {

            return "Good morning";

        } else if (hours == 12 && seconds == 0) {

            return "Good morning";

        } else if (hours >= 12 && hours < 20) {

            return "Good afternoon";

        } else if (hours == 20 && seconds == 0) {

            return "Good afternoon";

        } else if (hours >= 20 && hours < 24) {

            return "Good night";

        } else if (hours >= 0 && hours < 6) {

            return "Good night";

        }

        return "Error.";

    }


    public static double exercise7_getNeededLitres(double area, double litrePerformance) {

        double neededLitres = area / litrePerformance;
        return neededLitres;

    }

    public static double exercise7_getPaintCost(double neededLitres, double paintCostLitre) {

        double paintCost = neededLitres * paintCostLitre;
        return paintCost;

    }

    public static double exercise7_getPerMeterWage(double dailyWage) {
        final int WORKER_PERFORMANCE_HOUR = 2;

        double hourlyWage = dailyWage / 8;
        double perMeterWage = hourlyWage / WORKER_PERFORMANCE_HOUR;

        return perMeterWage;
    }

    public static double exercise7_getWorkersCost(double area, double perMeterWage) {

        double workersCost;

        if (area > 0 && area < 100) {

            workersCost = perMeterWage * area;
            return workersCost;

        } else if (area >= 100 && area < 300) {

            workersCost = (perMeterWage * 2) * (area / 2);
            return workersCost;

        } else if (area >= 300 && area < 1000) {

            workersCost = (perMeterWage * 3) * (area / 3);
            return workersCost;
        } else if (area >= 1000) {

            workersCost = (perMeterWage * 4) * (area / 4);
            return workersCost;

        }

        //error
        return -1;

    }

    public static double exercise7_getTotalCosts(double paintCost, double workersCost) {

        double totalCosts = paintCost + workersCost;
        return totalCosts;

    }

    public static double exercise7_getDesiredOutput(int desiredOutput, double paintCost, double workersCost, double totalCost) {
        if (desiredOutput == 1) {
            return paintCost;
        } else if (desiredOutput == 2) {
            return workersCost;
        } else if (desiredOutput == 3) {
            return totalCost;
        }
        return -1;
    }


    public static String exercise8_isMultiple(double x, double y) {
        if (x % y == 0) {
            return "x is a multiple of y.";
        } else if (y % x == 0) {
            return "y is a multiple of x.";
        }
        return "x is not a multiple of y.";
    }


    public static int exercise9_digit1(int number) {
        int digit1 = (number / 100) % 10;
        return digit1;
    }

    public static int exercise9_digit2(int number) {
        int digit2 = (number / 10) % 10;
        return digit2;
    }

    public static int exercise9_digit3(int number) {
        int digit3 = number % 10;
        return digit3;
    }

    public static String exercise9_isIncreasing(int digit1, int digit2, int digit3) {
        if (digit1 < digit2 && digit2 < digit3) {
            return "Is increasing.";
        }
        return "It is not increasing.";
    }


    public static double exercise10_itemCost(double price) {
        if (price <= 50) {
            return price * 0.8;
        } else if (price > 50 && price <= 100) {
            return price * 0.7;
        } else if (price > 100 && price <= 200) {
            return price * 0.6;
        }
        return price * 0.4;
    }


    public static String exercise11_classCategorization(double approved, double limit1, double limit2, double limit3, double limit4) {
        if (approved < 0 || approved > 1) {
            return "Invalid value.";
        } else if (approved < limit1) {
            return "Bad class.";
        } else if (approved < limit2) {
            return "Weak class.";
        } else if (approved < limit3) {
            return "Reasonable class.";
        } else if (approved < limit4) {
            return "Good class.";
        }
        return "Excellent class.";
    }


    public static String exercise12_pollutionCategorization(double pollution) {
        if (pollution < 0.3) {
            return "No activities must be stopped.";
        } else if (pollution >= 0.3 && pollution < 0.4) {
            return "1st group industries must stop their activities.";
        } else if (pollution >= 0.4 && pollution < 0.5) {
            return "1st and 2nd group industries must stop their activities.";
        }
        return "1st, 2nd, and 3rd group industries must stop their activities.";
    }


    public static int exercise13_hoursNeeded(double grassArea, int trees, int shrubs) {
        double hoursNeeded = Math.ceil(((300 * grassArea) + (600 * trees) + (400 * shrubs)) / 3600);
        return (int) hoursNeeded;
    }

    public static double exercise13_totalCost(int hoursNeeded, double grassArea, int trees, int shrubs) {
        double totalCost = (hoursNeeded * 10) + (grassArea * 10) + (trees * 20) + (shrubs * 15);
        return totalCost;
    }


    public static double exercise14_dailyAverage(double distance1, double distance2, double distance3, double distance4, double distance5) {
        double dailyAverage = (distance1 + distance2 + distance3 + distance4 + distance5) / 5;
        return dailyAverage;
    }

    public static double exercise14_milesToKm(double distanceInMiles) {
        double distanceInKm = (distanceInMiles * 1609) / 1000;
        return distanceInKm;
    }


    public static String exercise15_triangleSizeClassification(double sideA, double sideB, double sideC) {
        if (sideA + sideB < sideC || sideA + sideC < sideB || sideB + sideC < sideA || sideA <= 0 || sideB <= 0 || sideC <= 0) {
            return "Triangle not possible.";
        }
        if (sideA == sideB && sideA == sideC) {
            return "Equilateral triangle.";
        } else if (sideA != sideB && sideA != sideC && sideB != sideC) {
            return "Scalene triangle.";
        }
        return "Isosceles triangle.";
    }


    public static String exercise16_triangleAngleClassification(double angleA, double angleB, double angleC) {
        if (angleA + angleB + angleC != 180 || angleA < 0 || angleB < 0 || angleC < 0) {
            return "Triangle not possible.";
        }
        if (angleA < 90 && angleB < 90 && angleC < 90) {
            return "Acute triangle.";
        } else if (angleA == 90 || angleB == 90 || angleC == 90) {
            return "Right triangle.";
        }
        return "Obtuse triangle.";
    }


    public static int exercise17_spillHour(int departureMinute, int durationMinute) {
        int arrivalMinute = departureMinute + durationMinute;
        if (arrivalMinute > 59) {
            //spillHour
            return 1;
        }
        //No spillHour
        return 0;
    }

    public static int exercise17_getArrivalMinute(int departureMinute, int durationMinute) {
        int arrivalMinute = departureMinute + durationMinute;
        if (arrivalMinute > 59) {
            arrivalMinute = arrivalMinute - 60;
        }
        return arrivalMinute;
    }

    public static String exercise17_spillDay(int departureHour, int durationHour, int spillHour) {
        int arrivalHour = departureHour + durationHour + spillHour;
        if (arrivalHour > 23) {
            return "Tomorrow";
        }
        return "Today";
    }

    public static int exercise17_getArrivalHour(int departureHour, int durationHour, int spillHour) {
        int arrivalHour = departureHour + durationHour + spillHour;
        if (arrivalHour > 23) {
            arrivalHour = arrivalHour - 24;
        }
        return arrivalHour;
    }


    public static int exercise18_initialTimeToSeconds(int hours, int minutes, int seconds) {
        int initialTimeSeconds = (hours * 3600) + (minutes * 60) + seconds;
        return initialTimeSeconds;
    }

    public static int exercise18_endTimeSeconds(int initialTimeSeconds, int durationSeconds) {
        int endTimeSeconds = initialTimeSeconds + durationSeconds;
        return endTimeSeconds;
    }

    public static int exercise18_endTimeConversionHours(int endTimeSeconds) {
        int finalHours = endTimeSeconds / 3600;
        return finalHours;
    }

    public static int exercise18_endTimeConversionMinutes(int endTimeSeconds) {
        int finalMinutes = (endTimeSeconds % 3600) / 60;
        return finalMinutes;
    }

    public static int exercise18_endTimeConversionSeconds(int endTimeSeconds) {
        int finalSeconds = (endTimeSeconds % 3600) % 60;
        return finalSeconds;
    }

    public static double exercise19_getWeeklyWage(int weekWorkHours) {
        double weeklyWage;
        if (weekWorkHours > 36 && weekWorkHours < 42) {
            weeklyWage = (36 * 7.5) + ((weekWorkHours - 36) * 10);
            return weeklyWage;
        }
        else if (weekWorkHours >= 42) {
            weeklyWage = (36 * 7.5) + (5 * 10) + ((weekWorkHours - 41) * 15);
            return weeklyWage;
        }
        weeklyWage = weekWorkHours * 7.5;
        return  weeklyWage;
    }

    public static int exercise20_getCost(int weekDay, int kitType, int distance) {
        int cost = distance * 2;

        if (weekDay == 1) {
            if (kitType == 1) {
                cost += 30;
                return cost;
            }
            if (kitType == 2) {
                cost += 50;
                return cost;
            }
            if (kitType == 3) {
                cost += 100;
                return cost;
            }
        }
        if (kitType == 1) {
            cost += 40;
            return cost;
        }
        if (kitType == 2) {
            cost += 70;
            return cost;
        }
        cost += 140;
        return cost;
    }
}

