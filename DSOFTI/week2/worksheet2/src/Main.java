import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //exercise1();
        //exercise2();
        //exercise3();
        //exercise4();
        //exercise5();
        //exercise6();
        //exercise7();
        //exercise8();
        //exercise9();
        //exercise10();
        //exercise11();
        //exercise12();
        //exercise13();
        //exercise14();
        //exercise15();
        //exercise16();
        //exercise17();
        //exercise18();
        //exercise19();
        //exercise20();
    }

    public static void exercise1() {

        double weightedAverage;
        int note1, note2, note3, weight1, weight2, weight3;

        Scanner read = new Scanner(System.in);

        System.out.println("Enter the value of note 1.");
        note1 = read.nextInt();
        System.out.println("Enter the value of note 2.");
        note2 = read.nextInt();
        System.out.println("Enter the value of note 3.");
        note3 = read.nextInt();
        System.out.println("Enter the value of weight 1.");
        weight1 = read.nextInt();
        System.out.println("Enter the value of weight 2.");
        weight2 = read.nextInt();
        System.out.println("Enter the value of weight 3.");
        weight3 = read.nextInt();

        weightedAverage = Processing.exercise1(note1, note2, note3, weight1, weight2, weight3);

        System.out.printf("The weighted average is %.2f.\n", weightedAverage);

        if (weightedAverage >= 8) {

            System.out.println("The weighted average value is sufficient for approval.");
        } else if (weightedAverage == -1) {

            System.out.println("All values have to be below or equal to 20.");

        } else if (weightedAverage == -2) {

            System.out.println("The total of all weights has to be equal to 20.");

        } else {

            System.out.println("The weighted average value is not sufficient for approval.");

        }

    }

    public static void exercise2() {

        int number, digit3, digit2, digit1;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter number between 100 and 999.");
        number = read.nextInt();

        digit3 = Processing.exercise2_digit3(number);
        digit2 = Processing.exercise2_digit2(number);
        digit1 = Processing.exercise2_digit1(number);

        if (digit3 == -1) {

            System.out.println("Please enter a value between 100 and 999.");
            System.exit(-1);

        } else {

            System.out.printf("The first digit is %d, the second is %d, and the third is %d.\n", digit1, digit2, digit3);

        }

        if (Processing.exercise2_oddOrEven(number) == 0) {

            System.out.println("The number is even.");

        } else if (Processing.exercise2_oddOrEven(number) == 1) {

            System.out.println("The number is odd.");

        }
    }

    public static void exercise3() {

        double x, y, d;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter the value of x.");
        x = read.nextDouble();
        System.out.println("Enter the value of y.");
        y = read.nextDouble();

        d = Processing.exercise3_getDistance(x, y);

        System.out.printf("The distance is %.2f", d);

    }

    public static void exercise4() {

        double x;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter the value of x.");
        x = read.nextDouble();

        double result = Processing.exercise4_getFunctionValue(x);

        System.out.printf("The result is %.2f", result);

    }

    public static void exercise5() {

        double area, volume;
        String classification;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter value of area.");
        area = read.nextDouble();

        volume = Processing.exercise5_getVolume(area);
        classification = Processing.exercise5_getCubeClassification(volume);

        System.out.printf("The cube's volume is %.2f, and its classification is %s.", volume, classification);

    }

    public static void exercise6() {

        int totalSeconds, hours, minutes, seconds;
        String greeting;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter total seconds.");
        totalSeconds = read.nextInt();

        hours = Processing.exercise6_getHours(totalSeconds);
        minutes = Processing.exercise6_getMinutes(totalSeconds);
        seconds = Processing.exercise6_getSeconds(totalSeconds);
        greeting = Processing.exercise6_getGreeting(hours, minutes, seconds);

        System.out.printf("%d : %d : %d\n", hours, minutes, seconds);
        System.out.println(greeting);

    }

    public static void exercise7() {
        double area, litrePerformance, neededLitres, paintCostLitre, dailyWage, perMeterWage, paintCost, workersCost, totalCost, output;
        int desiredOutput;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter area of building.");
        area = read.nextDouble();
        System.out.println("Enter value of the performance per litre.");
        litrePerformance = read.nextDouble();
        System.out.println("Enter cost of a litre of paint.");
        paintCostLitre = read.nextDouble();
        System.out.println("Enter daily wage of one worker.");
        dailyWage = read.nextDouble();
        System.out.println("Select the desired output: 1 for paint cost, 2 for workers cost, and 3 for total cost.");
        desiredOutput = read.nextInt();

        neededLitres = Processing.exercise7_getNeededLitres(area, litrePerformance);
        paintCost = Processing.exercise7_getPaintCost(neededLitres, paintCostLitre);
        perMeterWage = Processing.exercise7_getPerMeterWage(dailyWage);
        workersCost = Processing.exercise7_getWorkersCost(area, perMeterWage);
        totalCost = Processing.exercise7_getTotalCosts(paintCost, workersCost);
        output = Processing.exercise7_getDesiredOutput(desiredOutput, paintCost, workersCost, totalCost);

        System.out.printf("The requested result is %.2f", output);

    }

    public static void exercise8() {
        double x, y;
        String isMultiple;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter value of x.");
        x = read.nextDouble();
        System.out.println("Enter value of y.");
        y = read.nextDouble();

        isMultiple = Processing.exercise8_isMultiple(x, y);

        System.out.println(isMultiple);
    }

    public static void exercise9() {
        int number, digit1, digit2, digit3;
        String isIncreasing;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter a three-digit number.");
        number = read.nextInt();

        digit1 = Processing.exercise9_digit1(number);
        digit2 = Processing.exercise9_digit2(number);
        digit3 = Processing.exercise9_digit3(number);
        isIncreasing = Processing.exercise9_isIncreasing(digit1, digit2, digit3);

        System.out.println(isIncreasing);

    }

    public static void exercise10() {
        double price, finalPrice;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter the original price.");
        price = read.nextDouble();

        finalPrice = Processing.exercise10_itemCost(price);

        System.out.printf("The final price is %.2f.", finalPrice);
    }

    public static void exercise11() {
        double approved, limit1, limit2, limit3, limit4;
        String categorization;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter value of approved students.");
        approved = read.nextDouble();
        System.out.println("Enter value of first threshold.");
        limit1 = read.nextDouble();
        System.out.println("Enter value of second threshold.");
        limit2 = read.nextDouble();
        System.out.println("Enter value of third threshold.");
        limit3 = read.nextDouble();
        System.out.println("Enter value of forth threshold.");
        limit4 = read.nextDouble();


        categorization = Processing.exercise11_classCategorization(approved, limit1, limit2, limit3, limit4);

        System.out.println(categorization);
    }

    public static void exercise12() {
        double pollution;
        String categorization;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter pollution level.");
        pollution = read.nextDouble();

        categorization = Processing.exercise12_pollutionCategorization(pollution);

        System.out.println(categorization);
    }

    public static void exercise13() {
        double grassArea, totalCost;
        int trees, shrubs, hoursNeeded;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter area to be covered in grass.");
        grassArea = read.nextDouble();
        System.out.println("Enter number of trees.");
        trees = read.nextInt();
        System.out.println("Enter number of shrubs.");
        shrubs = read.nextInt();

        hoursNeeded = Processing.exercise13_hoursNeeded(grassArea, trees, shrubs);
        totalCost = Processing.exercise13_totalCost(hoursNeeded, grassArea, trees, shrubs);

        System.out.printf("The time needed is %d hours and the total cost is %.2f.", hoursNeeded, totalCost);
    }

    public static void exercise14() {
        double distance1, distance2, distance3, distance4, distance5, dailyAverage, dailyAverageInKm;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter distance 1.");
        distance1 = read.nextDouble();
        System.out.println("Enter distance 2.");
        distance2 = read.nextDouble();
        System.out.println("Enter distance 3.");
        distance3 = read.nextDouble();
        System.out.println("Enter distance 4.");
        distance4 = read.nextDouble();
        System.out.println("Enter distance 5.");
        distance5 = read.nextDouble();

        dailyAverage = Processing.exercise14_dailyAverage(distance1, distance2, distance3, distance4, distance5);
        dailyAverageInKm = Processing.exercise14_milesToKm(dailyAverage);

        System.out.printf("The daily average is %.2f km.", dailyAverageInKm);
    }

    public static void exercise15() {
        double sideA, sideB, sideC;
        String triangleClassification;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter value of side a.");
        sideA = read.nextDouble();
        System.out.println("Enter value of side b.");
        sideB = read.nextDouble();
        System.out.println("Enter value of side c.");
        sideC = read.nextDouble();

        triangleClassification = Processing.exercise15_triangleSizeClassification(sideA, sideB, sideC);

        System.out.println(triangleClassification);
    }

    public static void exercise16() {
        double angleA, angleB, angleC;
        String triangleClassification;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter value of angle a.");
        angleA = read.nextDouble();
        System.out.println("Enter value of angle b.");
        angleB = read.nextDouble();
        System.out.println("Enter value of angle c.");
        angleC = read.nextDouble();

        triangleClassification = Processing.exercise16_triangleAngleClassification(angleA, angleB, angleC);

        System.out.println(triangleClassification);
    }

    public static void exercise17() {
        int departureHour, departureMinute, durationHour, durationMinute, spillHour, arrivalHour, arrivalMinute;
        String day;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter hour of departure.");
        departureHour = read.nextInt();
        System.out.println("Enter minute of departure.");
        departureMinute = read.nextInt();
        System.out.println("Enter hour of duration.");
        durationHour = read.nextInt();
        System.out.println("Enter minute of duration.");
        durationMinute = read.nextInt();

        spillHour = Processing.exercise17_spillHour(departureMinute, durationMinute);
        arrivalMinute = Processing.exercise17_getArrivalMinute(departureMinute, durationMinute);
        arrivalHour = Processing.exercise17_getArrivalHour(departureHour, durationHour, spillHour);
        day = Processing.exercise17_spillDay(departureHour, durationHour, spillHour);

        System.out.printf("The train arrives at %d:%d of %s.", arrivalHour, arrivalMinute, day);
    }

    public static void exercise18() {
        int hours, minutes, seconds, durationSeconds, initialTimeSeconds, endTimeSeconds, finalHours, finalMinutes, finalSeconds;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter start hour.");
        hours = read.nextInt();
        System.out.println("Enter start minute.");
        minutes = read.nextInt();
        System.out.println("Enter start second.");
        seconds = read.nextInt();
        System.out.println("Enter duration in seconds.");
        durationSeconds = read.nextInt();

        initialTimeSeconds = Processing.exercise18_initialTimeToSeconds(hours, minutes, seconds);
        endTimeSeconds = Processing.exercise18_endTimeSeconds(initialTimeSeconds, durationSeconds);
        finalHours = Processing.exercise18_endTimeConversionHours(endTimeSeconds);
        finalMinutes = Processing.exercise18_endTimeConversionMinutes(endTimeSeconds);
        finalSeconds = Processing.exercise18_endTimeConversionSeconds(endTimeSeconds);

        System.out.printf("%d:%d:%d", finalHours, finalMinutes, finalSeconds);
    }

    public static void exercise19() {
        int weekWorkHours;
        double hourlyWage, weeklyWage;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter number of hours worked in one week.");
        weekWorkHours = read.nextInt();

        weeklyWage = Processing.exercise19_getWeeklyWage(weekWorkHours);

        System.out.printf("The weekly wage is %.2f.", weeklyWage);

    }

    public static void exercise20() {
        int cost, weekDay, kitType, distance;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter 1 if the reservation is for a work day, and 2 if it is for the weekend.");
        weekDay = read.nextInt();
        System.out.println("Enter 1 for kit A, 2 for kit B, and 3 for kit C.");
        kitType = read.nextInt();
        System.out.println("Enter distance in km.");
        distance = read.nextInt();


        cost = Processing.exercise20_getCost(weekDay, kitType, distance);

        System.out.printf("The cost is %d.", cost);
    }
}
