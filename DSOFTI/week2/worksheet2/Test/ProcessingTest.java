import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProcessingTest {

    @Test
    void exercise1_test1() {

        int note1 = 20;
        int note2 = 20;
        int note3 = 20;
        int weight1 = 7;
        int weight2 = 5;
        int weight3 = 8;
        double expected = 20;

        double result = Processing.exercise1(note1, note2, note3, weight1, weight2, weight3);

        assertEquals(expected, result, 0.0001);

    }
    @Test
    void exercise1_test2() {

        int note1 = 7;
        int note2 = 7;
        int note3 = 7;
        int weight1 = 7;
        int weight2 = 5;
        int weight3 = 8;
        double expected = 7;

        double result = Processing.exercise1(note1, note2, note3, weight1, weight2, weight3);

        assertEquals(expected, result, 0.0001);

    }
    @Test
    void exercise1_test3() {

        int note1 = 8;
        int note2 = 8;
        int note3 = 8;
        int weight1 = 7;
        int weight2 = 5;
        int weight3 = 8;
        double expected = 8;

        double result = Processing.exercise1(note1, note2, note3, weight1, weight2, weight3);

        assertEquals(expected, result, 0.0001);

    }
    @Test
    void exercise1_test4() {

        int note1 = 21;
        int note2 = 8;
        int note3 = 10;
        int weight1 = 7;
        int weight2 = 5;
        int weight3 = 8;
        double expected = -1;

        double result = Processing.exercise1(note1, note2, note3, weight1, weight2, weight3);

        assertEquals(expected, result, 0.0001);

    }
    @Test
    void exercise1_test5() {

        int note1 = 20;
        int note2 = 8;
        int note3 = 10;
        int weight1 = 7;
        int weight2 = 6;
        int weight3 = 8;
        double expected = -2;

        double result = Processing.exercise1(note1, note2, note3, weight1, weight2, weight3);

        assertEquals(expected, result, 0.0001);

    }


    @Test
    void exercise2_digit3_test1() {

        int number = 268;
        int expected = 8;

        int result = Processing.exercise2_digit3(number);

        assertEquals(expected, result);

    }
    @Test
    void exercise2_digit3_test2() {

        int number = 99;
        int expected = -1;

        int result = Processing.exercise2_digit3(number);

        assertEquals(expected, result);

    }
    @Test
    void exercise2_digit3_test3() {

        int number = 1000;
        int expected = -1;

        int result = Processing.exercise2_digit3(number);

        assertEquals(expected, result);

    }
    @Test
    void exercise2_digit2() {

        int number = 234;
        int expected = 3;

        int result = Processing.exercise2_digit2(number);

        assertEquals(expected, result);

    }
    @Test
    void exercise2_digit1() {

        int number = 345;
        int expected = 3;

        int result = Processing.exercise2_digit1(number);

        assertEquals(expected, result);

    }
    @Test
    void exercise2_oddOrEven_test1() {

        int number = 344;
        int expected = 0;

        int result = Processing.exercise2_oddOrEven(number);

        assertEquals(expected, result);

    }
    @Test
    void exercise2_oddOrEven_test2() {

        int number = 677;
        int expected = 1;

        int result = Processing.exercise2_oddOrEven(number);

        assertEquals(expected, result);

    }

    @Test
    void exercise3_getDistance() {

        double x = 10;
        double y = 15.5;
        double expected = 18.4458;

        double result = Processing.exercise3_getDistance(x, y);

        assertEquals(expected, result, 0.0001);

    }

    @Test
    void exercise4_getFunctionValue_test1() {

        double x = -2;
        double expected = -2;

        double result = Processing.exercise4_getFunctionValue(x);

        assertEquals(expected, result, 0.0001);

    }
    @Test
    void exercise4_getFunctionValue_test2() {

        double x = 0;
        double expected = 0;

        double result = Processing.exercise4_getFunctionValue(x);

        assertEquals(expected, result, 0.0001);

    }
    @Test
    void exercise4_getFunctionValue_test3() {

        double x = 13;
        double expected = 0;

        double result = Processing.exercise4_getFunctionValue(x);

        assertEquals(expected, result, 0.0001);

    }

    @Test
    void exercise5_getVolume_test1() {

        double area = -5;
        double expected = -1;

        double result = Processing.exercise5_getVolume(area);

        assertEquals(expected, result, 0.0001);

    }
    @Test
    void exercise5_getVolume_test2() {

        double area = 5;
        double expected = 0.0007607;

        double result = Processing.exercise5_getVolume(area);

        assertEquals(expected, result, 0.0001);

    }
    @Test
    void exercise5_getCubeClassification_test1() {

        double volume = 0.5;
        String expected = "Small";

        String result = Processing.exercise5_getCubeClassification(volume);

        assertEquals(expected, result);

    }
    @Test
    void exercise5_getCubeClassification_test2() {

        double volume = 1.7;
        String expected = "Medium";

        String result = Processing.exercise5_getCubeClassification(volume);

        assertEquals(expected, result);

    }
    @Test
    void exercise5_getCubeClassification_test3() {

        double volume = 2.5;
        String expected = "Big";

        String result = Processing.exercise5_getCubeClassification(volume);

        assertEquals(expected, result);

    }

    @Test
    void exercise6_getHours() {

        int totalSeconds = 3601;
        int expected = 1;

        int result = Processing.exercise6_getHours(totalSeconds);

        assertEquals(expected, result);

    }

    @Test
    void exercise6_getMinutes() {

        int totalSeconds = 125;
        int expected = 2;

        int result = Processing.exercise6_getMinutes(totalSeconds);

        assertEquals(expected, result);

    }

    @Test
    void exercise6_getSeconds() {

        int totalSeconds = 63;
        int expected = 3;

        int result = Processing.exercise6_getSeconds(totalSeconds);

        assertEquals(expected, result);

    }

    @Test
    void exercise6_getGreeting_test1() {

        int hours = 0;
        int minutes = 0;
        int seconds = 0;
        String expected = "Good night";

        String result = Processing.exercise6_getGreeting(hours, minutes, seconds);

        assertEquals(expected, result);

    }

    @Test
    void exercise6_getGreeting_test2() {

        int hours = 6;
        int minutes = 0;
        int seconds = 0;
        String expected = "Good morning";

        String result = Processing.exercise6_getGreeting(hours, minutes, seconds);

        assertEquals(expected, result);

    }

    @Test
    void exercise6_getGreeting_test3() {

        int hours = 12;
        int minutes = 0;
        int seconds = 1;
        String expected = "Good afternoon";

        String result = Processing.exercise6_getGreeting(hours, minutes, seconds);

        assertEquals(expected, result);

    }

    @Test
    void exercise6_getGreeting_test4() {

        int hours = 20;
        int minutes = 0;
        int seconds = 0;
        String expected = "Good afternoon";

        String result = Processing.exercise6_getGreeting(hours, minutes, seconds);

        assertEquals(expected, result);

    }

    @Test
    void exercise7_getNeededLitres() {
        double area = 10;
        double litrePerformance = 5;
        double expected = 2;

        double result = Processing.exercise7_getNeededLitres(area, litrePerformance);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise7_getPaintCost() {
        double neededLitres = 5;
        double paintCostLitre = 2.5;
        double expected = 12.5;

        double result = Processing.exercise7_getPaintCost(neededLitres, paintCostLitre);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise7_getPerMeterWage() {
        double dailyWage = 12;
        double expected = 0.75;

        double result = Processing.exercise7_getPerMeterWage(dailyWage);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise7_getWorkersCost_areaBetween0And100() {
        double area = 99;
        double perMeterWage = 2;
        double expected = 198;

        double result = Processing.exercise7_getWorkersCost(area, perMeterWage);

        assertEquals(expected, result);
    }

    @Test
    void exercise7_getWorkersCost_areaBetween100and300() {
        double area = 100;
        double perMeterWage = 2;
        double expected = 200;

        double result = Processing.exercise7_getWorkersCost(area, perMeterWage);

        assertEquals(expected, result);
    }
    @Test
    void exercise7_getWorkersCost_areaBetween300and1000() {
        double area = 300;
        double perMeterWage = 2;
        double expected = 600;

        double result = Processing.exercise7_getWorkersCost(area, perMeterWage);

        assertEquals(expected, result);
    }

    @Test
    void exercise7_getWorkersCost_areaGreaterThan1000() {
        double area = 1000;
        double perMeterWage = 2;
        double expected = 2000;

        double result = Processing.exercise7_getWorkersCost(area, perMeterWage);

        assertEquals(expected, result);
    }

    @Test
    void exercise7_getTotalCosts() {
        double paintCost = 156.7;
        double workersCost = 765.012;
        double expected = 921.712;

        double result = Processing.exercise7_getTotalCosts(paintCost, workersCost);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise7_getDesiredOutput_paintCostSelection() {
        int desiredOutput = 1;
        double paintCost = 100;
        double workersCost = 500;
        double totalCost = 600;
        double expected = 100;

        double result = Processing.exercise7_getDesiredOutput(desiredOutput, paintCost, workersCost, totalCost);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise7_getDesiredOutput_workersCostSelection() {
        int desiredOutput = 2;
        double paintCost = 100;
        double workersCost = 500;
        double totalCost = 600;
        double expected = 500;

        double result = Processing.exercise7_getDesiredOutput(desiredOutput, paintCost, workersCost, totalCost);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise7_getDesiredOutput_totalCostSelection() {
        int desiredOutput = 3;
        double paintCost = 100;
        double workersCost = 500;
        double totalCost = 600;
        double expected = 600;

        double result = Processing.exercise7_getDesiredOutput(desiredOutput, paintCost, workersCost, totalCost);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise8_isMultiple_xIsMultipleOfy() {
        double x = 20;
        double y = 10;
        String expected = "x is a multiple of y.";

        String result = Processing.exercise8_isMultiple(x, y);

        assertEquals(expected, result);
    }

    @Test
    void exercise8_isMultiple_yIsMultipleOfx() {
        double x = 10;
        double y = 20;
        String expected = "y is a multiple of x.";

        String result = Processing.exercise8_isMultiple(x, y);

        assertEquals(expected, result);

    }

    @Test
    void exercise8_isMultiple_isNotMultiple() {
        double x = 20;
        double y = 7;
        String expected = "x is not a multiple of y.";

        String result = Processing.exercise8_isMultiple(x, y);

        assertEquals(expected, result);

    }

    @Test
    void exercise9_digit1() {
        int number = 764;
        int expected = 7;

        int result = Processing.exercise9_digit1(number);

        assertEquals(expected, result);
    }

    @Test
    void exercise9_digit2() {
        int number = 764;
        int expected = 6;

        int result = Processing.exercise9_digit2(number);

        assertEquals(expected, result);
    }

    @Test
    void exercise9_digit3() {
        int number = 764;
        int expected = 4;

        int result = Processing.exercise9_digit3(number);

        assertEquals(expected, result);
    }

    @Test
    void exercise9_isIncreasing_true() {
        int digit1 = 1;
        int digit2 = 2;
        int digit3 = 3;
        String expected = "Is increasing.";

        String result = Processing.exercise9_isIncreasing(digit1, digit2, digit3);

        assertEquals(expected, result);
    }
    @Test
    void exercise9_isIncreasing_false() {
        int digit1 = 1;
        int digit2 = 2;
        int digit3 = 0;
        String expected = "It is not increasing.";

        String result = Processing.exercise9_isIncreasing(digit1, digit2, digit3);

        assertEquals(expected, result);
    }

    @Test
    void exercise10_itemCost_belowOrEqual50() {
        double price = 50;
        double expected = 40;

        double result = Processing.exercise10_itemCost(price);

        assertEquals(expected, result);
    }

    @Test
    void exercise10_itemCost_between50And100() {
        double price = 100;
        double expected = 70;

        double result = Processing.exercise10_itemCost(price);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise10_itemCost_between100And200() {
        double price = 200;
        double expected = 120;

        double result = Processing.exercise10_itemCost(price);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise10_itemCost_above200() {
        double price = 300;
        double expected = 120;

        double result = Processing.exercise10_itemCost(price);

        assertEquals(expected, result);

    }

    @Test
    void exercise11_classCategorization_invalidValue() {
        double approved = -1;
        double limit1 = 0.2;
        double limit2 = 0.5;
        double limit3 = 0.7;
        double limit4 = 0.9;
        String expected = "Invalid value.";

        String result = Processing.exercise11_classCategorization(approved, limit1, limit2, limit3, limit4);

        assertEquals(expected, result);
    }
    @Test
    void exercise11_classCategorization_lowerThanLimit1() {
        double approved = 0.09;
        double limit1 = 0.1;
        double limit2 = 0.5;
        double limit3 = 0.6;
        double limit4 = 0.95;
        String expected = "Bad class.";

        String result = Processing.exercise11_classCategorization(approved, limit1, limit2, limit3, limit4);

        assertEquals(expected, result);
    }
    @Test
    void exercise11_classCategorization_lowerThanLimit2() {
        double approved = 0.20;
        double limit1 = 0.2;
        double limit2 = 0.3;
        double limit3 = 0.7;
        double limit4 = 0.9;
        String expected = "Weak class.";

        String result = Processing.exercise11_classCategorization(approved, limit1, limit2, limit3, limit4);

        assertEquals(expected, result);
    }
    @Test
    void exercise11_classCategorization_lowerThanLimit3() {
        double approved = 0.60;
        double limit1 = 0.2;
        double limit2 = 0.6;
        double limit3 = 0.7;
        double limit4 = 0.9;
        String expected = "Reasonable class.";

        String result = Processing.exercise11_classCategorization(approved, limit1, limit2, limit3, limit4);

        assertEquals(expected, result);
    }
    @Test
    void exercise11_classCategorization_lowerThanLimit4() {
        double approved = 0.80;
        double limit1 = 0.2;
        double limit2 = 0.5;
        double limit3 = 0.8;
        double limit4 = 0.9;
        String expected = "Good class.";

        String result = Processing.exercise11_classCategorization(approved, limit1, limit2, limit3, limit4);

        assertEquals(expected, result);
    }
    @Test
    void exercise11_classCategorization_equalOrHigherThanLimit4() {
        double approved = 0.95;
        double limit1 = 0.2;
        double limit2 = 0.5;
        double limit3 = 0.7;
        double limit4 = 0.95;
        String expected = "Excellent class.";

        String result = Processing.exercise11_classCategorization(approved, limit1, limit2, limit3, limit4);

        assertEquals(expected, result);
    }

    @Test
    void exercise12_pollutionCategorization_lessThan30() {
        double pollution = 0.2;
        String expected = "No activities must be stopped.";

        String result = Processing.exercise12_pollutionCategorization(pollution);

        assertEquals(expected, result);
    }
    @Test
    void exercise12_pollutionCategorization_lessThan40() {
        double pollution = 0.3;
        String expected = "1st group industries must stop their activities.";

        String result = Processing.exercise12_pollutionCategorization(pollution);

        assertEquals(expected, result);
    }
    @Test
    void exercise12_pollutionCategorization_lessThan50() {
        double pollution = 0.4;
        String expected = "1st and 2nd group industries must stop their activities.";

        String result = Processing.exercise12_pollutionCategorization(pollution);

        assertEquals(expected, result);
    }
    @Test
    void exercise12_pollutionCategorization_equalOrMoreThan50() {
        double pollution = 0.5;
        String expected = "1st, 2nd, and 3rd group industries must stop their activities.";

        String result = Processing.exercise12_pollutionCategorization(pollution);

        assertEquals(expected, result);
    }

    @Test
    void exercise13_hoursNeeded() {
        double grassArea = 10.5;
        int trees = 5;
        int shrubs = 10;
        int expected = 3;

        int result = Processing.exercise13_hoursNeeded(grassArea, trees, shrubs);

        assertEquals(expected, result);
    }

    @Test
    void exercise13_totalCost() {
        int hoursNeeded = 3;
        double grassArea = 10.5;
        int trees = 5;
        int shrubs = 10;
        double expected = 385;

        double result = Processing.exercise13_totalCost(hoursNeeded, grassArea, trees, shrubs);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise14_dailyAverage() {
        double distance1 = 10;
        double distance2 = 10;
        double distance3 = 10;
        double distance4 = 10;
        double distance5 = 10;
        double expected = 10;

        double result = Processing.exercise14_dailyAverage(distance1, distance2, distance3, distance4, distance5);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise14_milesToKm() {
        double dailyAverage = 10;
        double expected = 16.09;

        double result = Processing.exercise14_milesToKm(dailyAverage);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise16_triangleClassification_isTrianglePossible() {
        double angleA = 90;
        double angleB = 45;
        double angleC = 46;
        String expected = "Triangle not possible.";

        String result = Processing.exercise16_triangleAngleClassification(angleA, angleB, angleC);

        assertEquals(expected, result);
    }
    @Test
    void exercise16_triangleClassification_negativeAngles() {
        double angleA = 90;
        double angleB = -45;
        double angleC = 135;
        String expected = "Triangle not possible.";

        String result = Processing.exercise16_triangleAngleClassification(angleA, angleB, angleC);

        assertEquals(expected, result);
    }

    @Test
    void exercise16_triangleClassification_acuteTriangle() {
        double angleA = 60;
        double angleB = 60;
        double angleC = 60;
        String expected = "Acute triangle.";

        String result = Processing.exercise16_triangleAngleClassification(angleA, angleB, angleC);

        assertEquals(expected, result);
    }

    @Test
    void exercise16_triangleClassification_rightTriangle() {
        double angleA = 90;
        double angleB = 45;
        double angleC = 45;
        String expected = "Right triangle.";

        String result = Processing.exercise16_triangleAngleClassification(angleA, angleB, angleC);

        assertEquals(expected, result);
    }
    @Test
    void exercise16_triangleClassification_obtuseTriangle() {
        double angleA = 120;
        double angleB = 30;
        double angleC = 30;
        String expected = "Obtuse triangle.";

        String result = Processing.exercise16_triangleAngleClassification(angleA, angleB, angleC);

        assertEquals(expected, result);
    }

    @Test
    void exercise15_triangleSizeClassification_negativeSizes() {
        double sideA = 10;
        double sideB = -1;
        double sideC = 10;
        String expected = "Triangle not possible.";

        String result = Processing.exercise15_triangleSizeClassification(sideA, sideB, sideC);

        assertEquals(expected, result);
    }

    @Test
    void exercise15_triangleSizeClassification_isTrianglePossible() {
        double sideA = 10;
        double sideB = 10;
        double sideC = 21;
        String expected = "Triangle not possible.";

        String result = Processing.exercise15_triangleSizeClassification(sideA, sideB, sideC);

        assertEquals(expected, result);
    }

    @Test
    void exercise15_triangleSizeClassification_equilateral() {
        double sideA = 10;
        double sideB = 10;
        double sideC = 10;
        String expected = "Equilateral triangle.";

        String result = Processing.exercise15_triangleSizeClassification(sideA, sideB, sideC);

        assertEquals(expected, result);
    }

    @Test
    void exercise15_triangleSizeClassification_scalene() {
        double sideA = 10;
        double sideB = 7;
        double sideC = 5;
        String expected = "Scalene triangle.";

        String result = Processing.exercise15_triangleSizeClassification(sideA, sideB, sideC);

        assertEquals(expected, result);
    }

    @Test
    void exercise15_triangleSizeClassification_isosceles() {
        double sideA = 10;
        double sideB = 10;
        double sideC = 5;
        String expected = "Isosceles triangle.";

        String result = Processing.exercise15_triangleSizeClassification(sideA, sideB, sideC);

        assertEquals(expected, result);
    }

    @Test
    void exercise17_spillHour_lessOrEqualTo59() {
        int departureMinute = 20;
        int durationMinute = 39;
        int expected = 0;

        int result = Processing.exercise17_spillHour(departureMinute, durationMinute);

        assertEquals(expected, result);
    }

    @Test
    void exercise17_spillHour_moreThan59() {
        int departureMinute = 20;
        int durationMinute = 40;
        int expected = 1;

        int result = Processing.exercise17_spillHour(departureMinute, durationMinute);

        assertEquals(expected, result);
    }

    @Test
    void exercise17_getArrivalMinute_lessOrEqualTo59() {
        int departureMinute = 20;
        int durationMinute = 39;
        int expected = 0;

        int result = Processing.exercise17_spillHour(departureMinute, durationMinute);

        assertEquals(expected, result);
    }

    @Test
    void exercise17_getArrivalMinute_moreThan59() {
        int departureMinute = 20;
        int durationMinute = 40;
        int expected = 1;

        int result = Processing.exercise17_spillHour(departureMinute, durationMinute);

        assertEquals(expected, result);
    }

    @Test
    void exercise17_spillDay_today() {
        int departureHour = 20;
        int durationHour = 2;
        int spillHour = 1;
        String expected = "Today";

        String result = Processing.exercise17_spillDay(departureHour, durationHour, spillHour);

        assertEquals(expected, result);
    }

    @Test
    void exercise17_spillDay_tomorrow() {
        int departureHour = 20;
        int durationHour = 3;
        int spillHour = 1;
        String expected = "Tomorrow";

        String result = Processing.exercise17_spillDay(departureHour, durationHour, spillHour);

        assertEquals(expected, result);
    }

    @Test
    void exercise17_getArrivalHour_lessOrEqualTo23() {
        int departureHour = 20;
        int durationHour = 2;
        int spillHour = 1;
        String expected = "Today";

        String result = Processing.exercise17_spillDay(departureHour, durationHour, spillHour);

        assertEquals(expected, result);
    }

    @Test
    void exercise17_getArrivalHour_moreThan23() {
        int departureHour = 20;
        int durationHour = 3;
        int spillHour = 1;
        String expected = "Tomorrow";

        String result = Processing.exercise17_spillDay(departureHour, durationHour, spillHour);

        assertEquals(expected, result);
    }

    @Test
    void exercise18_initialTimeToSeconds() {
        int hours = 15;
        int minutes = 37;
        int seconds = 32;
        int expected = 56252;

        int result = Processing.exercise18_initialTimeToSeconds(hours, minutes, seconds);

        assertEquals(expected, result);
    }

    @Test
    void exercise18_endTimeSeconds() {
        int initialTimeSeconds = 78253;
        int durationSeconds = 8264;
        int expected = 86517;

        int result = Processing.exercise18_endTimeSeconds(initialTimeSeconds, durationSeconds);

        assertEquals(expected, result);
    }

    @Test
    void exercise18_endTimeConversionHours() {
        int endTimeSeconds = 37495;
        int expected = 10;

        int result = Processing.exercise18_endTimeConversionHours(endTimeSeconds);

        assertEquals(expected, result);
    }

    @Test
    void exercise18_endTimeConversionMinutes() {
        int endTimeSeconds = 37495;
        int expected = 24;

        int result = Processing.exercise18_endTimeConversionMinutes(endTimeSeconds);

        assertEquals(expected, result);
    }

    @Test
    void exercise18_endTimeConversionSeconds() {
        int endTimeSeconds = 37495;
        int expected = 55;

        int result = Processing.exercise18_endTimeConversionSeconds(endTimeSeconds);

        assertEquals(expected, result);
    }

    @Test
    void exercise19_getWeeklyWage_lessOrEqualTo36() {
        int workWeekHours = 36;
        double expected = 270;

        double result = Processing.exercise19_getWeeklyWage(workWeekHours);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise19_getWeeklyWage_lessThan42() {
        int workWeekHours = 41;
        double expected = 320;

        double result = Processing.exercise19_getWeeklyWage(workWeekHours);

        assertEquals(expected, result, 0.0001);

    }

    @Test
    void exercise19_getWeeklyWage_moreOrEqualTo42() {
        int workWeekHours = 42;
        double expected = 335;

        double result = Processing.exercise19_getWeeklyWage(workWeekHours);

        assertEquals(expected, result, 0.0001);

    }

    @Test
    void exercise20_getCost_workDayKitA() {
        int weekDay = 1;
        int kitType = 1;
        int distance = 15;
        int expected = 60;

        int result = Processing.exercise20_getCost(weekDay, kitType, distance);

        assertEquals(expected, result);
    }
    @Test
    void exercise20_getCost_workDayKitB() {
        int weekDay = 1;
        int kitType = 2;
        int distance = 15;
        int expected = 80;

        int result = Processing.exercise20_getCost(weekDay, kitType, distance);

        assertEquals(expected, result);
    }
    @Test
    void exercise20_getCost_workDayKitC() {
        int weekDay = 1;
        int kitType = 3;
        int distance = 15;
        int expected = 130;

        int result = Processing.exercise20_getCost(weekDay, kitType, distance);

        assertEquals(expected, result);
    }
    @Test
    void exercise20_getCost_weekendKitA() {
        int weekDay = 2;
        int kitType = 1;
        int distance = 15;
        int expected = 70;

        int result = Processing.exercise20_getCost(weekDay, kitType, distance);

        assertEquals(expected, result);
    }
    @Test
    void exercise20_getCost_weekendKitB() {
        int weekDay = 2;
        int kitType = 2;
        int distance = 15;
        int expected = 100;

        int result = Processing.exercise20_getCost(weekDay, kitType, distance);

        assertEquals(expected, result);
    }
    @Test
    void exercise20_getCost_weekendKitC() {
        int weekDay = 2;
        int kitType = 3;
        int distance = 15;
        int expected = 170;

        int result = Processing.exercise20_getCost(weekDay, kitType, distance);

        assertEquals(expected, result);
    }

}
