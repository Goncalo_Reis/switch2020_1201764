package com.company;

import org.jetbrains.annotations.Contract;

import java.util.Scanner;

public class Processing {

    public static int exercise1_getLoopResult(int num) {
        int res = 1;
        if (num <= 0) {
            return -1;  //error code
        }
        for (int x = num; x >= 1; x--) {
            res *= x;
        }
        return res;
    }


    public static double exercise2_percentagePositiveGrades(int N, int[] grades) {
        int positiveGradesCount = 0;
        int positiveGradesPercentage;

        for (int i : grades) {  //count number of positive grades
            if (i >= 50) {
                positiveGradesCount++;
            }
        }
        if (positiveGradesCount == 0) {
            positiveGradesPercentage = 0;
            return positiveGradesPercentage;
        }

        positiveGradesPercentage = (positiveGradesCount * 100) / N;
        return positiveGradesPercentage;
    }

    public static double exercise2_averageNegativeGrades(int[] grades) {
        int negativeGradesCount = 0;
        int loopCount = 0;
        double negativeGradesAverage;

        for (int i : grades) {  // count number of negative grades
            if (i < 50) {
                negativeGradesCount++;
            }
        }
        if (negativeGradesCount == 0) {
            negativeGradesAverage = -1;  //error code
            return negativeGradesAverage;
        }
        int[] negativeGrades = new int[negativeGradesCount];

        for (int i = 0; i < negativeGradesCount; i++) {
            for (int j = loopCount; j < grades.length; j++) {
                loopCount += 1;
                if (grades[j] < 50) {
                    negativeGrades[i] = grades[j];
                    break;
                }
            }
        }

        int sumNegativeGrades = 0;  // sum all negative grades values
        for (int i = 0; i < negativeGrades.length; i++) {
            sumNegativeGrades += negativeGrades[i];
        }
        negativeGradesAverage = (double) sumNegativeGrades / negativeGradesCount;
        return negativeGradesAverage;
    }


    public static double exercise3_percentageEvenNumbers(int[] numbers) {
        int evenNumbersCount = 0;
        double percentageEvenNumbers;
        int negativeNumber = 0;
        if (numbers.length == 1) {
            negativeNumber += 1;
        }
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] <= 0) {
                negativeNumber += i;
                break;
            }
            else if (numbers[i] % 2 == 0) {
                evenNumbersCount += 1;
            }
        }
        if (evenNumbersCount == 0) {
            percentageEvenNumbers = 0;
            return percentageEvenNumbers;
        }
        percentageEvenNumbers = (evenNumbersCount * 100) / negativeNumber;
        return percentageEvenNumbers;
    }

    public static double exercise3_averageOddNumbers(int[] numbers) {
        int oddNumbersCount = 0;
        int sumOddNumbers = 0;
        double averageOddNumbers;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] <= 0) {
                break;
            }
            else if (numbers[i] % 2 != 0) {
                oddNumbersCount += 1;
                sumOddNumbers += numbers[i];
            }
        }
        if (oddNumbersCount == 0) {
            averageOddNumbers = -1;    // error code
            return averageOddNumbers;
        }

        averageOddNumbers = sumOddNumbers / oddNumbersCount;
        return averageOddNumbers;
    }


    public static int exercise4_multiplesOf3(int[] numbers) {
        int multiplesOf3Count = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 3 == 0) {
                multiplesOf3Count += 1;
            }
        }
        return multiplesOf3Count;
    }

    public static int exercise4_multiplesOfNumber(int number, int[] numbers) {
        int multiplesOfNumberCount = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % number == 0) {
                multiplesOfNumberCount += 1;
            }
        }
        return multiplesOfNumberCount;
    }

    public static int exercise4_multiplesOf3And5(int[] numbers) {
        int multiplesOf3And5Count = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 3 == 0 && numbers[i] % 5 == 0) {
                multiplesOf3And5Count += 1;
            }
        }
        return multiplesOf3And5Count;

    }

    public static int exercise4_multiplesOfTwoNumbers(int number1, int number2, int[] numbers) {
        int multiplesOfTwoNumbersCount = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % number1 == 0 && numbers[i] % number2 == 0) {
                multiplesOfTwoNumbersCount += 1;
            }
        }
        return multiplesOfTwoNumbersCount;
    }

    public static int exercise4_multiplesOfTwoNumbersSum(int number1, int number2, int[] numbers) {
        int multiplesOfTwoNumbersSum = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % number1 == 0 && numbers[i] % number2 == 0) {
                multiplesOfTwoNumbersSum += numbers[i];
            }
        }
        return multiplesOfTwoNumbersSum;
    }


    public static int exercise5_evenNumbersSum(int[] numbers) {
        int evenNumbersSum = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 == 0) {
                evenNumbersSum += numbers[i];
            }
        }
        return evenNumbersSum;
    }

    public static int exercise5_evenNumbersCount(int[] numbers) {
        int evenNumbersCount = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 == 0) {
                evenNumbersCount += 1;
            }
        }
        return evenNumbersCount;
    }

    public static int exercise5_oddNumbersSum(int[] numbers) {
        int oddNumbersSum = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 != 0) {
                oddNumbersSum += numbers[i];
            }
        }
        return oddNumbersSum;
    }

    public static int exercise5_oddNumbersCount(int[] numbers) {
        int oddNumbersCount = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 != 0) {
                oddNumbersCount += 1;
            }
        }
        return oddNumbersCount;
    }

    public static int exercise5_multiplesOfNumberSum(int number, int[] numbers) {
        int multiplesOfNumberSum = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % number == 0) {
                multiplesOfNumberSum += numbers[i];
            }
        }
        return multiplesOfNumberSum;
    }

    public static int exercise5_multiplesOfNumberProduct(int number, int[] numbers) {
        int multiplesOfNumberProduct = 0;
        int iterationCount = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % number == 0) {
                if (iterationCount == 0) {
                    multiplesOfNumberProduct += numbers[i];
                    iterationCount += 1;
                }
                else {
                    multiplesOfNumberProduct *= numbers[i];
                }
            }
        }
        return multiplesOfNumberProduct;
    }

    public static int exercise5_multiplesOfNumberAverage(int number, int[] numbers) {
        int multiplesOfNumberCount = 0;
        int multiplesOfNumberSum = 0;
        int multiplesOfNumberAverage = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % number == 0) {
                    multiplesOfNumberSum += numbers[i];
                    multiplesOfNumberCount += 1;
            }
        }
        multiplesOfNumberAverage = multiplesOfNumberSum / multiplesOfNumberCount;
        return multiplesOfNumberAverage;
    }

    public static int exercise5_multiplesOfTwoNumbersAverage(int number1, int number2, int[] numbers) {
        int multiplesOfNumberCount = 0;
        int multiplesOfNumberSum = 0;
        int multiplesOfNumberAverage = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % number1 == 0 && numbers[i] % number2 == 0) {
                multiplesOfNumberSum += numbers[i];
                multiplesOfNumberCount += 1;
            }
        }
        multiplesOfNumberAverage = multiplesOfNumberSum / multiplesOfNumberCount;
        return multiplesOfNumberAverage;
    }


    public static int exercise6_getNumberOfDigits(int number) {
        int countNumberDigits = (int) Math.log10(number) + 1;
        return countNumberDigits;
    }

    public static int exercise6_getNumberOfEvenDigits(int number) {
        int countNumberEvenDigits = 0;
        String numberConvertedToString = Integer.toString(number);
        for (int i = 0; i < numberConvertedToString.length(); i++) {
            if ( (int) numberConvertedToString.charAt(i) % 2 == 0) {
                countNumberEvenDigits ++;
            }
        }
        return countNumberEvenDigits;
    }

    public static int exercise6_getNumberOfOddDigits(int number) {
        int countNumberOddDigits = 0;
        String numberConvertedToString = Integer.toString(number);
        for (int i = 0; i < numberConvertedToString.length(); i++) {
            if ( (int) numberConvertedToString.charAt(i) % 2 != 0) {
                countNumberOddDigits ++;
            }
        }
        return countNumberOddDigits;
    }

    public static int exercise6_sumDigits(int number) {
        int sumDigits = 0;
        String numberConvertedToString = Integer.toString(number);
        for (int i = 0; i < numberConvertedToString.length(); i++) {
            sumDigits += numberConvertedToString.charAt(i) - 48;  //unicode value to number value
        }
        return sumDigits;
    }

    public static int exercise6_sumEvenDigits(int number) {
        int sumEvenDigits = 0;
        String numberConvertedToString = Integer.toString(number);
        for (int i = 0; i < numberConvertedToString.length(); i++) {
            if (numberConvertedToString.charAt(i) % 2 == 0) {
                sumEvenDigits += numberConvertedToString.charAt(i) - 48;
            }
        }
        return sumEvenDigits;
    }

    public static int exercise6_sumOddDigits(int number) {
        int sumOddDigits = 0;
        String numberConvertedToString = Integer.toString(number);
        for (int i = 0; i < numberConvertedToString.length(); i++) {
            if (numberConvertedToString.charAt(i) % 2 != 0) {
                sumOddDigits += numberConvertedToString.charAt(i) - 48;
            }
        }
        return sumOddDigits;
    }

    public static double exercise6_averageDigits(int number) {
        int sumDigits = 0;
        String numberConvertedToString = Integer.toString(number);
        int countDigits = numberConvertedToString.length();
        for (int i = 0; i < numberConvertedToString.length(); i++) {
                sumDigits += numberConvertedToString.charAt(i) - 48;
        }
        double averageDigits = (double) sumDigits / countDigits;
        return averageDigits;
    }

    public static double exercise6_averageEvenDigits(int number) {
        int sumEvenDigits = 0;
        String numberConvertedToString = Integer.toString(number);
        int countEvenDigits = 0;
        for (int i = 0; i < numberConvertedToString.length(); i++) {
            if (numberConvertedToString.charAt(i) % 2 == 0) {
                sumEvenDigits += numberConvertedToString.charAt(i) - 48;
                countEvenDigits ++;
            }
        }
        double averageEvenDigits = (double) sumEvenDigits / countEvenDigits;
        return averageEvenDigits;
    }

    public static double exercise6_averageOddDigits(int number) {
        int sumOddDigits = 0;
        String numberConvertedToString = Integer.toString(number);
        int countOddDigits = 0;
        for (int i = 0; i < numberConvertedToString.length(); i++) {
            if (numberConvertedToString.charAt(i) % 2 != 0) {
                sumOddDigits += numberConvertedToString.charAt(i) - 48;
                countOddDigits ++;
            }
        }
        double averageOddDigits = (double) sumOddDigits / countOddDigits;
        return averageOddDigits;
    }

    public static int exercise6_numberBackwards(int number) {
        String numberBackwardsString = "";
        String numberConvertedToString = Integer.toString(number);
        for (int i = 0; i < numberConvertedToString.length(); i++) {
            numberBackwardsString += numberConvertedToString.charAt(numberConvertedToString.length() - 1 - i);
        }
        int numberBackwards = Integer.parseInt(numberBackwardsString);
        return numberBackwards;
    }


    public static boolean exercise7_isPalindrome(int number) {
        String numberAsString = Integer.toString(number);
        if (numberAsString.length() == 1) {
            return false;
        }
        int equalDigits = 0;
        for (int i = 0; i < numberAsString.length() / 2; i++) {
            if (numberAsString.charAt(i) == numberAsString.charAt(numberAsString.length() - 1 - i)) {
                equalDigits ++;
            }
        }
        if (equalDigits == numberAsString.length() / 2) {
            return true;
        }
        return false;
    }

    public static boolean exercise7_isAmstrong(int number) {
        int countDigit = Integer.toString(number).length();
        double [] digits = new double[countDigit];

        for (int i = 0; i < countDigit; i++) {
            digits[i] = ((number % Math.pow(10, 1 + i) - number % Math.pow(10, i)) / Math.pow(10, i));
        }

        int sumDigitCubes = 0;
        for (int i = 0; i < digits.length; i++) {
            sumDigitCubes += Math.pow(digits[i], 3);
        }

        if (number == sumDigitCubes) {
            return true;
        }

        return false;
    }

    public static int exercise7_getFirstPalindrome(int inferiorLimit, int superiorLimit) {
        for (int i = inferiorLimit; i <= superiorLimit; i++) {
            if (exercise7_isPalindrome(i)) {
                int firstPalindrome = i;
                return firstPalindrome;
            }
        }
        return -1;    // error code
    }

    public static int exercise7_getBiggerPalindrome(int inferiorLimit, int superiorLimit) {
        int biggerPalindrome = 0;
        for (int i = inferiorLimit; i <= superiorLimit; i++) {
            if (exercise7_isPalindrome(i)) {
                biggerPalindrome = i;
            }
        }
        if (biggerPalindrome == 0) {
            return -1;    // error code
        }
        return biggerPalindrome;
    }

    public static int exercise7_countPalindromes(int inferiorLimit, int superiorLimit) {
        int countPalindromes = 0;
        for (int i = inferiorLimit; i <= superiorLimit; i++) {
            if (exercise7_isPalindrome(i)) {
                countPalindromes += 1;
            }
        }
        return countPalindromes;
    }

    public static int exercise7_getFirstAmstrong(int inferiorLimit, int superiorLimit) {
        for (int i = inferiorLimit; i <= superiorLimit; i++) {
            if (exercise7_isAmstrong(i)) {
                int firstAmstrong = i;
                return firstAmstrong;
            }
        }
        return -1;    // error code
    }

    public static int exercise7_countAmstrong(int inferiorLimit, int superiorLimit) {
        int countAmstrong = 0;
        for (int i = inferiorLimit; i <= superiorLimit; i++) {
            if (exercise7_isAmstrong(i)) {
                countAmstrong += 1;
            }
        }
        return countAmstrong;
    }


    public static boolean exercise8_compareNumbers(int number, int sum) {
        if (sum > number) {
            return true;
        }
        return false;
    }


    public static double exercise9_employeeWage(int extraHours, int baseWage) {
        double employeeMonthlyWage = baseWage + ((baseWage * 0.02) * extraHours);
        return employeeMonthlyWage;
    }


    public static boolean exercise10_compareNumbers(int number, int product) {
        if (product > number) {
            return true;
        }
        return false;
    }


    public static String exercise11_getCombinations(int N) {
        int inferiorLimit = 0;
        int superiorLimit = 10;
        int countCombinations = 0;

        for (int i = inferiorLimit; i <= superiorLimit; i++) {
            for (int j = inferiorLimit; j <= superiorLimit; j++) {
                if (i + j == N) {
                    System.out.printf("%d + %d, ", i, j);
                    countCombinations ++;
                }
            }
        }

        return countCombinations + " combinations.\n";
    }


    public static double exercise12_positiveRoot(double a, double b, double c) {
        double result = (-b + (Math.sqrt(Math.pow(b, 2) - 4 * a * c))) / (2 * a);
        return result;
    }

    public static double exercise12_negativeRoot(double a, double b, double c) {
        double result = (-b - (Math.sqrt(Math.pow(b, 2) - 4 * a * c))) / (2 * a);
        return result;
    }


    public static String exercise13_classifyProduct(int code) {
        if (code == 1) {
            return "Non-perishable food.\n";
        }
        else if (code > 1 && code < 5) {
            return "Perishable food.\n";
        }
        else if (code > 4 && code < 7) {
            return "Clothing.\n";
        }
        else if (code == 7) {
            return "Personal hygiene.\n";
        }
        else if (code > 7 && code < 16) {
            return "Cleaning and domestic tools.\n";
        }
        return "Invalid code.\n";
    }


    public static double exercise14_moneyExchange(double value, String currency) {

        switch (currency) {
            case "D":
                return value * 1.534;
            case "L":
                return value * 0.774;
            case "I":
                return value * 161.480;
            case "C":
                return value * 9.593;
        }

        return value * 1.601;
    }


    public static String exercise15_getQualitativeGrades(int grade) {
        if (grade >= 0 && grade <= 4) {
            return "Bad.\n";
        }
        else if (grade >= 5 && grade <= 9) {
            return "Mediocre.\n";
        }
        else if (grade >= 10 && grade <= 13) {
            return "Sufficient.\n";
        }
        else if (grade >= 14 && grade <= 17) {
            return "Good.\n";
        }
        else if (grade >= 18 && grade <= 20) {
            return "Very good.\n";
        }
        return "Invalid.\n";
    }


    public static double exercise16_netWage(double grossWage) {
        double netWage;
        final double TAX_TIER_1 = 0.90;
        final double TAX_TIER_2 = 0.85;
        final double TAX_TIER_3 = 0.80;

        if (grossWage <= 500) {
            netWage = grossWage * TAX_TIER_1;
        }

        else if (grossWage > 500 && grossWage <= 1000) {
            netWage = (500 * TAX_TIER_1) + ( (grossWage - 500) * TAX_TIER_2);
        }

        else {
            netWage = (500 * TAX_TIER_1) + (500 * TAX_TIER_2) + (grossWage - 1000) * TAX_TIER_3;
        }

        return netWage;
    }


    public static String exercise17_classifyDogRace(float dogWeight) {

        String dogRace = "";

        if (dogWeight <= 10) {
            dogRace = "small";
        }
        else if (dogWeight > 10 && dogWeight <= 25) {
            dogRace = "medium";
        }
        else if (dogWeight > 25 && dogWeight <= 45) {
            dogRace = "big";
        }
        else if (dogWeight > 45) {
            dogRace = "giant";
        }

        return dogRace;
    }

    public static boolean exercise17_IsAmountFoodAdequate(float foodEaten, String dogRace) {

        final int ADEQUATE_FOOD_AMOUNT_SMALL_DOG = 100;
        final int ADEQUATE_FOOD_AMOUNT_MEDIUM_DOG = 250;
        final int ADEQUATE_FOOD_AMOUNT_BIG_DOG = 300;
        final int ADEQUATE_FOOD_AMOUNT_GIANT_DOG = 500;
        boolean isAmountFoodAdequate = true;

        if ("small".equals(dogRace)) {
            if (foodEaten != ADEQUATE_FOOD_AMOUNT_SMALL_DOG) {
                isAmountFoodAdequate = false;
            }
        } else if ("medium".equals(dogRace)) {
            if (foodEaten != ADEQUATE_FOOD_AMOUNT_MEDIUM_DOG) {
                isAmountFoodAdequate = false;
            }
        } else if ("big".equals(dogRace)) {
            if (foodEaten != ADEQUATE_FOOD_AMOUNT_BIG_DOG) {
                isAmountFoodAdequate = false;
            }
        } else if ("giant".equals(dogRace)) {
            if (foodEaten != ADEQUATE_FOOD_AMOUNT_GIANT_DOG) {
                isAmountFoodAdequate = false;
            }
        }

        return isAmountFoodAdequate;
    }


    public static String exercise18_get8DigitNumber(long idNumber) {
        String idNumberString = Long.toString(idNumber);
        String string8DigitNumber = "";

        for (int i = 0; i < 8; i++) {
            string8DigitNumber += idNumberString.charAt(i);
        }

        return string8DigitNumber;
    }

    public static int exercise18_weightedSum(String string8DigitNumber) {
        int weightedSum = 0;
        for (int i = 0; i < string8DigitNumber.length(); i++) {
            weightedSum += (string8DigitNumber.charAt(i) - 48) * (string8DigitNumber.length() - i);
        }
        return weightedSum;
    }

    public static boolean exercise18_isMultiple11(int weightedSum) {
        boolean isMultiple11 = true;

        if (weightedSum % 11 != 0) {
            isMultiple11 = false;
        }

        return isMultiple11;
    }


    public static String exercise19_getEvenDigits(String sequence) {
        String evenDigits = "";

        for (int i = 0; i < sequence.length(); i++) {
            if ((sequence.charAt(i) - 48) % 2 == 0) {
                evenDigits += sequence.charAt(i);
            }
        }

        return evenDigits;
    }

    public static String exercise19_getOddDigits(String sequence) {
        String oddDigits = "";

        for (int i = 0; i < sequence.length(); i++) {
            if ((sequence.charAt(i) - 48) % 2 != 0) {
                oddDigits += sequence.charAt(i);
            }
        }

        return oddDigits;
    }


    public static int exercise20_getSum(int number) {
        int sum = 0;

        for (int i = 1; i < number; i++) {
            if (number % i == 0) {
                sum += i;
            }
        }

        return sum;
    }

    public static String exercise20_categorizeNumber(int number, int sum) {
        String category = "";
        if (number == sum) {
            category = "Perfect";
        }
        else if (number < sum) {
            category = "Abundant";
        }
        else {
            category = "Reduced";
        }
        return category;
    }
}


