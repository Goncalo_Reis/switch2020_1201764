package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //exercise1();
        //exercise2();
        //exercise3();
        //exercise4();
        //exercise5();
        //exercise6();
        //exercise7();
        //exercise8();
        //exercise9();
        //exercise10();
        //exercise11();
        //exercise12();
        //exercise13();
        //exercise14();
        //exercise15();
        //exercise16();
        //exercise17();
        //exercise18();
        //exercise19();
        exercise20();
    }

    public static void exercise1() {
        int num, res;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter num.");
        num = read.nextInt();

        res = Processing.exercise1_getLoopResult(num);

        if (res == -1) {
            System.out.println("You must enter a positive number.");
        } else {
            System.out.printf("Result is %d.", res);
        }
    }

    public static void exercise2() {
        int N;
        double positiveGradesPercentage, negativeGradesAverage;
        int[] grades;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter number of students.");
        N = read.nextInt();
        if (N <= 0) {
            System.out.println("You must enter a positive number.");
        } else {
            grades = new int[N];
            for (int i = 0; i < N; i++) {
                System.out.printf("Enter grade of student %d.\n", i + 1);
                grades[i] = read.nextInt();
            }

            positiveGradesPercentage = Processing.exercise2_percentagePositiveGrades(N, grades);
            negativeGradesAverage = Processing.exercise2_averageNegativeGrades(grades);

            if (negativeGradesAverage == -1) {
                System.out.println("The percentage of positive grades is 100%.");
            } else {
                System.out.printf("The percentage of positive grades is %.2f, and the average of the negative ones is %.2f.", positiveGradesPercentage, negativeGradesAverage);
            }
        }
    }

    public static void exercise3() {
        int N;
        int[] numbers = new int[0];
        double percentageEvenNumbers, averageOddNumbers;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter quantity of numbers.");
        N = read.nextInt();
        if (N <= 0) {
            System.out.println("You must enter a positive number.");
        } else {
            numbers = new int[N];
            for (int i = 0; i < N; i++) {
                System.out.printf("Enter number %d.\n", i + 1);
                numbers[i] = read.nextInt();
            }

            percentageEvenNumbers = Processing.exercise3_percentageEvenNumbers(numbers);
            averageOddNumbers = Processing.exercise3_averageOddNumbers(numbers);
            if (averageOddNumbers == -1) {
                System.out.printf("The percentage of even numbers is %.2f.", percentageEvenNumbers);
            } else {
                System.out.printf("The percentage of even numbers is %.2f, and the average of the odd ones is %.2f.", percentageEvenNumbers, averageOddNumbers);
            }
        }
    }

    public static void exercise4() {
        int N, number, number1, number2;
        int[] numbers;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter quantity of numbers.");
        N = read.nextInt();

        if (N <= 0) {
            System.out.println("You must enter a positive number.");
        }
        else {
            numbers = new int[N];
            for (int i = 0; i < N; i++) {
                System.out.printf("Enter number %d.\n", i + 1);
                numbers[i] = read.nextInt();
            }

            //System.out.print(Processing.exercise4_multiplesOf3(numbers));

            /*
            System.out.println("Enter number to check multiples.");
            number = read.nextInt();
            System.out.print(Processing.exercise4_multiplesOfNumber(number, numbers));
            */

                //System.out.print(Processing.exercise4_multiplesOf3And5(numbers));

            /*
            System.out.println("Enter first number to check multiples.");
            number1 = read.nextInt();
            System.out.println("Enter second number to check multiples.");
            number2 = read.nextInt();
            System.out.print(Processing.exercise4_multiplesOfTwoNumbers(number1, number2, numbers));
            */

            /*
            System.out.println("Enter first number to check multiples.");
            number1 = read.nextInt();
            System.out.println("Enter second number to check multiples.");
            number2 = read.nextInt();
            System.out.print(Processing.exercise4_multiplesOfTwoNumbersSum(number1, number2, numbers));
            */
            }
        }

    public static void exercise5() {
        int N, number, number1, number2;
        int[] numbers;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter quantity of numbers.");
        N = read.nextInt();

        if (N <= 0) {
            System.out.println("You must enter a positive number.");
        }
        else {
            numbers = new int[N];
            for (int i = 0; i < N; i++) {
                System.out.printf("Enter number %d.\n", i + 1);
                numbers[i] = read.nextInt();
            }

            //System.out.print(Processing.exercise5_evenNumbersSum(numbers));


            //System.out.print(Processing.exercise5_evenNumbersCount(numbers));


            //System.out.print(Processing.exercise5_oddNumbersSum(numbers));

            //System.out.print(Processing.exercise5_oddNumbersCount(numbers));

            /*
            System.out.println("Enter number to check multiples.");
            number = read.nextInt();
            System.out.print(Processing.exercise5_multiplesOfNumberSum(number, numbers));
             */

            /*
            System.out.println("Enter number to check multiples.");
            number = read.nextInt();
            System.out.print(Processing.exercise5_multiplesOfNumberProduct(number, numbers));
             */

            /*
            System.out.println("Enter number to check multiples.");
            number = read.nextInt();
            System.out.print(Processing.exercise5_multiplesOfNumberAverage(number, numbers));
            */

            /*
            System.out.println("Enter first number to check multiples.");
            number1 = read.nextInt();
            System.out.println("Enter second number to check multiples.");
            number2 = read.nextInt();
            System.out.print(Processing.exercise5_multiplesOfTwoNumbersAverage(number1, number2, numbers));
            */
        }
    }

    public static void exercise6() {
        int number;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter number.");
        number = read.nextInt();

        //System.out.print(Processing.exercise6_getNumberOfDigits(number));

        //System.out.print(Processing.exercise6_getNumberOfEvenDigits(number));

        //System.out.print(Processing.exercise6_getNumberOfOddDigits(number));

        //System.out.print(Processing.exercise6_sumDigits(number));

        //System.out.print(Processing.exercise6_sumEvenDigits(number));

        //System.out.print(Processing.exercise6_sumOddDigits(number));

        //System.out.print(Processing.exercise6_averageDigits(number));

        //System.out.print(Processing.exercise6_averageEvenDigits(number));

        //System.out.print(Processing.exercise6_averageOddDigits(number));

        //System.out.print(Processing.exercise6_numberBackwards(number));
    }

    public static void exercise7() {
        int number, inferiorLimit, superiorLimit;

        Scanner read = new Scanner(System.in);

        /*
        System.out.println("Enter number.");
        number = read.nextInt();
        System.out.print(Processing.exercise7_isPalindrome(number));
         */

        /*
        System.out.println("Enter number.");
        number = read.nextInt();
        System.out.print(Processing.exercise7_isAmstrong(number));
         */

        /*
        System.out.println("Enter number.");
        number = read.nextInt();
        System.out.print(Processing.exercise7_isAmstrong(number));
         */

        /*
        System.out.println("Enter inferior limit.");
        inferiorLimit = read.nextInt();
        System.out.println("Enter superior limit.");
        superiorLimit = read.nextInt();
        System.out.print(Processing.exercise7_getFirstPalindrome(inferiorLimit, superiorLimit));
         */

        /*
        System.out.println("Enter inferior limit.");
        inferiorLimit = read.nextInt();
        System.out.println("Enter superior limit.");
        superiorLimit = read.nextInt();
        System.out.print(Processing.exercise7_getBiggerPalindrome(inferiorLimit, superiorLimit));
         */

        /*
        System.out.println("Enter inferior limit.");
        inferiorLimit = read.nextInt();
        System.out.println("Enter superior limit.");
        superiorLimit = read.nextInt();
        System.out.print(Processing.exercise7_countPalindromes(inferiorLimit, superiorLimit));
         */

        /*
        System.out.println("Enter inferior limit.");
        inferiorLimit = read.nextInt();
        System.out.println("Enter superior limit.");
        superiorLimit = read.nextInt();
        System.out.print(Processing.exercise7_getFirstAmstrong(inferiorLimit, superiorLimit));
         */

        System.out.println("Enter inferior limit.");
        inferiorLimit = read.nextInt();
        System.out.println("Enter superior limit.");
        superiorLimit = read.nextInt();
        System.out.print(Processing.exercise7_countAmstrong(inferiorLimit, superiorLimit));

    }

    public static void exercise8() {
        int number, input;
        int smallestNumber = 0;
        int sum = 0;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter a number.");
        number = read.nextInt();

        do {
            System.out.println("Enter numbers until the first number is reached.");
            input = read.nextInt();
            if (input < smallestNumber || sum == 0) {
                smallestNumber = input;
            }
            sum += input;
        } while (Processing.exercise8_compareNumbers(number, sum) == false);

        System.out.printf("The smallest number is %d.", smallestNumber);
    }

    public static void exercise9() {
        int extraHours = 0;
        int baseWage;
        int sumEmployeesWage = 0;
        int numberEmployees = 0;
        double employeeWage = 0;
        double averageEmployeesWage = 0;

        Scanner read = new Scanner(System.in);

        while (true) {
            System.out.println("Enter number of extra hours.");
            extraHours = read.nextInt();
            if (extraHours == -1) {
                break;
            }
            System.out.println("Enter value of base wage.");
            baseWage = read.nextInt();
            employeeWage = Processing.exercise9_employeeWage(extraHours, baseWage);
            System.out.printf("This employee's wage is %.2f.\n", employeeWage);
            sumEmployeesWage += employeeWage;
            numberEmployees += 1;
        }

        averageEmployeesWage = sumEmployeesWage / numberEmployees;

        System.out.printf("The average of all wages is %.2f.", averageEmployeesWage);
    }

    public static void exercise10() {
        int number, input;
        int highestNumber = 0;
        int product = 1;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter a number.");
        number = read.nextInt();

        do {
            System.out.println("Enter numbers until the first number is reached.");
            input = read.nextInt();
            if (input > highestNumber) {
                highestNumber = input;
            }
            product *= input;
        } while (Processing.exercise10_compareNumbers(number, product) == false);

        System.out.printf("The highest number is %d.", highestNumber);
    }

    public static void exercise11() {
        int N;
        String combinations;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter N.");
        N = read.nextInt();

        System.out.print(Processing.exercise11_getCombinations(N));
    }

    public static void exercise12() {
        int N;
        double a, b, c, root1, root2;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter number of equations to solve.");
        N = read.nextInt();

        int i = 0;
        while (i < N) {
            System.out.println("Enter a value.");
            a = read.nextInt();
            System.out.println("Enter b value.");
            b = read.nextInt();
            System.out.println("Enter c value.");
            c = read.nextInt();
            root1 = Processing.exercise12_positiveRoot(a, b, c);
            root2 = Processing.exercise12_negativeRoot(a, b, c);
            System.out.printf("The roots are %.2f and %.2f.\n", root1, root2);
            i++;
        }

    }

    public static void exercise13() {
        int code;
        String classification;

        Scanner read = new Scanner(System.in);

        do {
            System.out.println("Enter code number.");
            code = read.nextInt();
            System.out.print(Processing.exercise13_classifyProduct(code));
        } while (code != 0);

    }

    public static void exercise14() {
        int value = 0;
        String currency;

        Scanner read = new Scanner(System.in);
        do {
            System.out.println("Enter currency to exchange to.");
            currency = read.next();
            System.out.println("Enter value to exchange.");
            value = read.nextInt();
            System.out.print(Processing.exercise14_moneyExchange(value, currency) + "\n");

        } while(value > 0);
    }

    public static void exercise15() {
        int grade = 0;

        Scanner read = new Scanner(System.in);

        do {
            System.out.println("Enter grade.");
            grade = read.nextInt();
            System.out.print(Processing.exercise15_getQualitativeGrades(grade));
        } while (grade >= 0);
    }

    public static void exercise16() {

        double grossWage, netWage;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter gross wage.");
        grossWage = read.nextDouble();

        netWage = Processing.exercise16_netWage(grossWage);

        System.out.print("The net wage is " + netWage);
    }

    public static void exercise17() {

        float dogWeight = 0;
        float foodEaten;
        String dogRace;
        boolean isAmountFoodAdequate;

        Scanner read = new Scanner(System.in);
        do {
            System.out.println("Enter dog weight.");
            dogWeight = read.nextFloat();
            if (dogWeight > 0) {
                System.out.println("Enter amount of food eaten by the dog.");
                foodEaten = read.nextFloat();


                dogRace = Processing.exercise17_classifyDogRace(dogWeight);
                isAmountFoodAdequate = Processing.exercise17_IsAmountFoodAdequate(foodEaten, dogRace);

                if (isAmountFoodAdequate) {
                    System.out.println("The amount of food is adequate.");
                } else {
                    System.out.println("The amount of food is not adequate.");
                }
            }
        } while(dogWeight > 0);
    }

    public static void exercise18() {
        long idNumber;
        int weightedSum;
        String string8DigitNumber;
        boolean isMultiple11;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter ID Number.");
        idNumber = read.nextLong();

        string8DigitNumber = Processing.exercise18_get8DigitNumber(idNumber);
        weightedSum = Processing.exercise18_weightedSum(string8DigitNumber);
        isMultiple11 = Processing.exercise18_isMultiple11(weightedSum);

        if (isMultiple11) {
            System.out.println("The ID number is correct.");
        }
        else {
            System.out.println("The ID number is not correct.");
        }
    }

    public static void exercise19() {
        String sequence, evenDigits, oddDigits;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter sequence.");
        sequence = read.next();

        evenDigits = Processing.exercise19_getEvenDigits(sequence);
        oddDigits = Processing.exercise19_getOddDigits(sequence);

        System.out.print(evenDigits + oddDigits);
    }

    public static void exercise20() {
        int number, sum;
        String category;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter number.");
        number = read.nextInt();

        sum = Processing.exercise20_getSum(number);
        category = Processing.exercise20_categorizeNumber(number, sum);

        System.out.println(number + " is " + category + ".");
    }
}
