package com.company;

import org.junit.jupiter.api.Test;

import static java.lang.Double.NaN;
import static java.lang.Double.doubleToLongBits;
import static org.junit.jupiter.api.Assertions.*;

class ProcessingTest {

    @Test
    void exercise1_getLoopResult_1() {
        int num = 1;
        int expected = 1;

        int result = Processing.exercise1_getLoopResult(num);

        assertEquals(expected, result);
    }

    @Test
    void exercise1_getLoopResult_greaterThan1() {
        int num = 5;
        int expected = 120;

        int result = Processing.exercise1_getLoopResult(num);

        assertEquals(expected, result);
    }

    @Test
    void exercise1_getLoopResult_lessThan1() {
        int num = 0;
        int expected = -1;

        int result = Processing.exercise1_getLoopResult(num);

        assertEquals(expected, result);
    }

    @Test
    void exercise2_percentagePositiveGrades_0PositiveGrades() {
        int N = 2;
        int[] grades = {0, 40};
        double expected = 0;

        double result = Processing.exercise2_percentagePositiveGrades(N, grades);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise2_percentagePositiveGrades_moreThan0PositiveGrades() {
        int N = 3;
        int[] grades = {40, 50, 60};
        double expected = 66;

        double result = Processing.exercise2_percentagePositiveGrades(N, grades);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise2_averageNegativeGrades_0NegativeGrades() {
        int[] grades = {50, 100};
        double expected = -1;

        double result = Processing.exercise2_averageNegativeGrades(grades);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise2_averageNegativeGrades_moreThan0NegativeGrades() {
        int[] grades = {0, 40, 50, 90};
        double expected = 20;

        double result = Processing.exercise2_averageNegativeGrades(grades);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise3_percentageEvenNumbers() {
        int[] numbers = {5, 3, 2, 8, -5, 2};
        double expected = 50;

        double result = Processing.exercise3_percentageEvenNumbers(numbers);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise3_averageOddNumbers() {
        int[] numbers = {5, 3, 2, 8, -5, 2};
        double expected = 4;

        double result = Processing.exercise3_averageOddNumbers(numbers);

        assertEquals(expected, result, 0.0001);

    }

    @Test
    void exercise4_multiplesOf3() {
        int[] numbers = {1, 3, 6, 8, 9, 10};
        int expected = 3;

        int result = Processing.exercise4_multiplesOf3(numbers);

        assertEquals(expected, result);
    }

    @Test
    void exercise4_multiplesOfNumber() {
        int number = 6;
        int[] numbers = {1, 3, 6, 8, 9, 12};
        int expected = 2;

        int result = Processing.exercise4_multiplesOfNumber(number, numbers);

        assertEquals(expected, result);
    }

    @Test
    void exercise4_multiplesOf3And5() {
        int[] numbers = {1, 3, 6, 8, 9, 10, 30};
        int expected = 1;

        int result = Processing.exercise4_multiplesOf3And5(numbers);

        assertEquals(expected, result);
    }

    @Test
    void exercise4_multiplesOfTwoNumbers() {
        int number1 = 6;
        int number2 = 10;
        int[] numbers = {1, 3, 6, 12, 60, 100, 120};
        int expected = 2;

        int result = Processing.exercise4_multiplesOfTwoNumbers(number1, number2, numbers);

        assertEquals(expected, result);
    }

    @Test
    void exercise4_multiplesOfTwoNumbersSum() {
        int number1 = 6;
        int number2 = 10;
        int[] numbers = {1, 3, 6, 12, 60, 100, 120};
        int expected = 180;

        int result = Processing.exercise4_multiplesOfTwoNumbersSum(number1, number2, numbers);

        assertEquals(expected, result);
    }

    @Test
    void exercise5_evenNumbersSum() {
        int[] numbers = {1, 2, 3, 4, 5};
        int expected = 6;

        int result = Processing.exercise5_evenNumbersSum(numbers);

        assertEquals(expected, result);
    }

    @Test
    void exercise5_evenNumbersCount() {
        int[] numbers = {1, 2, 3, 4, 5};
        int expected = 2;

        int result = Processing.exercise5_evenNumbersCount(numbers);

        assertEquals(expected, result);

    }

    @Test
    void exercise5_oddNumbersSum() {
        int[] numbers = {1, 2, 3, 4, 5};
        int expected = 9;

        int result = Processing.exercise5_oddNumbersSum(numbers);

        assertEquals(expected, result);

    }

    @Test
    void exercise5_oddNumbersCount() {
        int[] numbers = {1, 2, 3, 4, 5};
        int expected = 3;

        int result = Processing.exercise5_oddNumbersCount(numbers);

        assertEquals(expected, result);
    }

    @Test
    void exercise5_multiplesOfNumberSum() {
        int number = 2;
        int[] numbers = {1, 2, 3, 4, 5};
        int expected = 6;

        int result = Processing.exercise5_multiplesOfNumberSum(number, numbers);

        assertEquals(expected, result);
    }

    @Test
    void exercise5_multiplesOfNumberProduct() {
        int number = 2;
        int[] numbers = {1, 2, 3, 4, 5};
        int expected = 8;

        int result = Processing.exercise5_multiplesOfNumberProduct(number, numbers);

        assertEquals(expected, result);
    }

    @Test
    void exercise5_multiplesOfNumberAverage() {
        int number = 2;
        int[] numbers = {1, 2, 3, 4, 5};
        int expected = 3;

        int result = Processing.exercise5_multiplesOfNumberAverage(number, numbers);

        assertEquals(expected, result);
    }

    @Test
    void exercise5_multiplesOfTwoNumbersAverage() {
        int number1 = 2;
        int number2 = 4;
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int expected = 6;

        int result = Processing.exercise5_multiplesOfTwoNumbersAverage(number1, number2, numbers);

        assertEquals(expected, result);
    }

    @Test
    void exercise6_getNumberOfDigits() {
        int number = 12345;
        int expected = 5;

        int result = Processing.exercise6_getNumberOfDigits(number);

        assertEquals(expected, result);
    }

    @Test
    void exercise6_getNumberOfEvenDigits() {
        int number = 12345;
        int expected = 2;

        int result = Processing.exercise6_getNumberOfEvenDigits(number);

        assertEquals(expected, result);
    }

    @Test
    void exercise6_getNumberOfOddDigits() {
        int number = 12345;
        int expected = 3;

        int result = Processing.exercise6_getNumberOfOddDigits(number);

        assertEquals(expected, result);

    }

    @Test
    void exercise6_sumDigits() {
        int number = 12345;
        int expected = 15;

        int result = Processing.exercise6_sumDigits(number);

        assertEquals(expected, result);
    }

    @Test
    void exercise6_sumEvenDigits() {
        int number = 12345;
        int expected = 6;

        int result = Processing.exercise6_sumEvenDigits(number);

        assertEquals(expected, result);
    }

    @Test
    void exercise6_sumOddDigits() {
        int number = 12345;
        int expected = 9;

        int result = Processing.exercise6_sumOddDigits(number);

        assertEquals(expected, result);
    }

    @Test
    void exercise6_averageDigits() {
        int number = 12345;
        double expected = 3;

        double result = Processing.exercise6_averageDigits(number);

        assertEquals(expected, result);
    }

    @Test
    void exercise6_averageEvenDigits() {
        int number = 123456;
        double expected = 4;

        double result = Processing.exercise6_averageEvenDigits(number);

        assertEquals(expected, result);
    }

    @Test
    void exercise6_averageOddDigits() {
        int number = 123456789;
        double expected = 5;

        double result = Processing.exercise6_averageOddDigits(number);

        assertEquals(expected, result);
    }

    @Test
    void exercise6_numberBackwards() {
        int number = 123456789;
        int expected = 987654321;

        int result = Processing.exercise6_numberBackwards(number);

        assertEquals(expected, result);
    }

    @Test
    void exercise7_isPalindrome() {
        int number = 101;
        boolean expected = true;

        boolean result = Processing.exercise7_isPalindrome(number);

        assertEquals(expected, result);
    }

    @Test
    void exercise7_isAmstrong() {
        int number = 407;
        boolean expected = true;

        boolean result = Processing.exercise7_isAmstrong(number);

        assertEquals(expected, result);
    }

    @Test
    void exercise7_getFirstPalindrome() {
        int inferiorLimit = 1;
        int superiorLimit = 102;
        int expected = 11;

        int result = Processing.exercise7_getFirstPalindrome(inferiorLimit, superiorLimit);

        assertEquals(expected, result);
    }

    @Test
    void exercise7_getBiggerPalindrome() {
        int inferiorLimit = 1;
        int superiorLimit = 102;
        int expected = 101;

        int result = Processing.exercise7_getBiggerPalindrome(inferiorLimit, superiorLimit);

        assertEquals(expected, result);
    }

    @Test
    void exercise7_countPalindromes() {
        int inferiorLimit = 1;
        int superiorLimit = 102;
        int expected = 10;

        int result = Processing.exercise7_countPalindromes(inferiorLimit, superiorLimit);

        assertEquals(expected, result);

    }

    @Test
    void exercise7_getFirstAmstrong() {
        int inferiorLimit = 1;
        int superiorLimit = 102;
        int expected = 1;

        int result = Processing.exercise7_getFirstAmstrong(inferiorLimit, superiorLimit);

        assertEquals(expected, result);
    }

    @Test
    void exercise7_countAmstrong() {
        int inferiorLimit = 0;
        int superiorLimit = 500;
        int expected = 6;

        int result = Processing.exercise7_countAmstrong(inferiorLimit, superiorLimit);

        assertEquals(expected, result);
    }

    @Test
    void exercise8_compareNumbers() {
        int number = 100;
        int sum = 101;
        boolean expected = true;

        boolean result = Processing.exercise8_compareNumbers(number, sum);

        assertEquals(expected, result);
    }

    @Test
    void exercise9_employeeWage() {
        int extraHours = 10;
        int baseWage = 1000;
        double expected = 1200;

        double result = Processing.exercise9_employeeWage(extraHours, baseWage);

        assertEquals(expected, result);
    }

    @Test
    void exercise10_compareNumbers() {
        int number = 5;
        int product = 6;
        boolean expected = true;

        boolean result = Processing.exercise10_compareNumbers(number, product);

        assertEquals(expected, result);
    }

    @Test
    void exercise11_getCombinations() {
        int N = 15;
        String expected = "6 combinations.\n";

        String result = Processing.exercise11_getCombinations(N);

        assertEquals(expected, result);
    }

    @Test
    void exercise12_positiveRoot() {
        double a = 1;
        double b = 2;
        double c = 3;

        double expected = NaN;

        double result = Processing.exercise12_positiveRoot(a, b, c);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise12_negativeRoot() {
        double a = 1;
        double b = 100;
        double c = 3;

        double expected = -99.9699;

        double result = Processing.exercise12_negativeRoot(a, b, c);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise13_classifyProduct_equal1() {
        int code = 1;
        String expected = "Non-perishable food.\n";

        String result = Processing.exercise13_classifyProduct(code);

        assertEquals(expected, result);
    }

    @Test
    void exercise13_classifyProduct_2To4() {
        int code = 4;
        String expected = "Perishable food.\n";

        String result = Processing.exercise13_classifyProduct(code);

        assertEquals(expected, result);
    }

    @Test
    void exercise13_classifyProduct_5To6() {
        int code = 6;
        String expected = "Clothing.\n";

        String result = Processing.exercise13_classifyProduct(code);

        assertEquals(expected, result);
    }

    @Test
    void exercise13_classifyProduct_7() {
        int code = 7;
        String expected = "Personal hygiene.\n";

        String result = Processing.exercise13_classifyProduct(code);

        assertEquals(expected, result);
    }

    @Test
    void exercise13_classifyProduct_8To15() {
        int code = 15;
        String expected = "Cleaning and domestic tools.\n";

        String result = Processing.exercise13_classifyProduct(code);

        assertEquals(expected, result);
    }

    @Test
    void exercise13_classifyProduct_other() {
        int code = 20;
        String expected = "Invalid code.\n";

        String result = Processing.exercise13_classifyProduct(code);

        assertEquals(expected, result);
    }

    @Test
    void exercise14_moneyExchange() {
        double value = 100;
        String currency = "D";
        double expected = 153.4;

        double result = Processing.exercise14_moneyExchange(value, currency);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise15_getQualitativeGrades() {
        int grade = 10;
        String expected = "Sufficient.\n";

        String result = Processing.exercise15_getQualitativeGrades(grade);

        assertEquals(expected, result);
    }

    @Test
    void exercise16_netWage_belowOrEqualTo500() {

        double grossWage = 500;
        double expected = 450;

        double result = Processing.exercise16_netWage(grossWage);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise16_netWage_between501And1000() {

        double grossWage = 1000;
        double expected = 875;

        double result = Processing.exercise16_netWage(grossWage);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise16_netWage() {

        double grossWage = 2000;
        double expected = 1675;

        double result = Processing.exercise16_netWage(grossWage);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise17_classifyDogRace_smallRace() {

        float dogWeight = 10;
        String expected = "small";

        String result = Processing.exercise17_classifyDogRace(dogWeight);

        assertEquals(expected, result);
    }

    @Test
    void exercise17_classifyDogRace_mediumRace() {

        float dogWeight = 25;
        String expected = "medium";

        String result = Processing.exercise17_classifyDogRace(dogWeight);

        assertEquals(expected, result);
    }

    @Test
    void exercise17_classifyDogRace_bigRace() {

        float dogWeight = 45;
        String expected = "big";

        String result = Processing.exercise17_classifyDogRace(dogWeight);

        assertEquals(expected, result);
    }

    @Test
    void exercise17_classifyDogRace_giantRace() {

        float dogWeight = 46;
        String expected = "giant";

        String result = Processing.exercise17_classifyDogRace(dogWeight);

        assertEquals(expected, result);
    }

    @Test
    void exercise17_IsAmountFoodAdequate_yes() {

        float foodEaten = 100;
        String dogRace = "small";

        boolean result = Processing.exercise17_IsAmountFoodAdequate(foodEaten, dogRace);

        assertEquals(true, result);
    }

    @Test
    void exercise17_IsAmountFoodAdequate_no() {

        float foodEaten = 600;
        String dogRace = "giant";

        boolean result = Processing.exercise17_IsAmountFoodAdequate(foodEaten, dogRace);

        assertEquals(false, result);
    }

    @Test
    void exercise18_get8DigitNumber() {

        long idNumber = 123456789;
        String expected = "12345678";

        String result = Processing.exercise18_get8DigitNumber(idNumber);

        assertEquals(expected, result);
    }

    @Test
    void exercise18_weightedSum() {

        String string8DigitNumber = "11111111";
        int expected = 36;

        int result = Processing.exercise18_weightedSum(string8DigitNumber);

        assertEquals(expected, result);
    }

    @Test
    void exercise18_isMultiple11() {

        int weightedSum = 22;

        boolean result = Processing.exercise18_isMultiple11(weightedSum);

        assertEquals(true, result);
    }

    @Test
    void exercise19_getEvenDigits() {
        String sequence = "123456789";
        String expected = "2468";

        String result = Processing.exercise19_getEvenDigits(sequence);

        assertEquals(expected, result);
    }

    @Test
    void exercise19_getOddDigits() {
        String sequence = "123456789";
        String expected = "13579";

        String result = Processing.exercise19_getOddDigits(sequence);

        assertEquals(expected, result);
    }

    @Test
    void exercise20_getSum() {
        int number = 6;
        int expected = 6;

        int result = Processing.exercise20_getSum(number);

        assertEquals(expected, result);
    }

    @Test
    void exercise20_categorizeNumber_perfect() {
        int number = 6;
        int sum = 6;
        String expected = "Perfect";

        String result = Processing.exercise20_categorizeNumber(number, sum);

        assertEquals(expected, result);
    }

    @Test
    void exercise20_categorizeNumber_abundant() {
        int number = 12;
        int sum = 16;
        String expected = "Abundant";

        String result = Processing.exercise20_categorizeNumber(number, sum);

        assertEquals(expected, result);
    }
    @Test
    void exercise20_categorizeNumber_reduced() {
        int number = 9;
        int sum = 4;
        String expected = "Reduced";

        String result = Processing.exercise20_categorizeNumber(number, sum);

        assertEquals(expected, result);
    }

}