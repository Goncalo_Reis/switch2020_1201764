package com.company;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


class ProcessingTest {

    @Test
    void exercise3() {

        double radius = 10;
        double height = 10;
        double expected = 3141592.6535;

        double result = Processing.exercise3(radius, height);

        assertEquals(expected, result, 0.0001);

    }

    @Test
    void exercise4() {

        double time = 10;
        double expected = 3.4;

        double result = Processing.exercise4(time);

        assertEquals(expected, result, 0.0001);

    }

    @Test
    void exercise5() {

        double time = 2;
        double initialSpeed = 0;
        double expected = 19.6;

        double result = Processing.exercise5(time, initialSpeed);

        assertEquals(expected, result, 0.0001);

    }

    @Test
    void exercise6() {

        double buildingShadow = 40;
        double personShadow = 4;
        double personHeight = 2;
        double expected = 20;

        double result = Processing.exercise6(buildingShadow, personShadow, personHeight);

        assertEquals(expected, result, 0.0001);

    }

    @Test
    void exercise7() {

        int person1Hours = 4;
        int person1Minutes = 2;
        int person1Seconds = 10;
        int person2Hours = 1;
        int person2Minutes = 5;
        int person2Seconds = 0;
        double expected = 11.3255;

        double result = Processing.exercise7(person1Hours, person1Minutes, person1Seconds, person2Hours,person2Minutes, person2Seconds);

        assertEquals(expected, result, 0.0001);

    }

    @Test
    void exercise8() {

        double cableLength1 = 40;
        double cableLength2 = 60;
        double angle = 60;
        double expected = 52.9150;

        double result = Processing.exercise8(cableLength1, cableLength2, angle);

        assertEquals(expected, result, 0.0001);

    }

    @Test
    void exercise9() {

        double A = 10;
        double B = 25;
        double x = 3.4;
        double expected = 238;

        double result = Processing.exercise9(A, B, x);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void exercise10() {

        double c1 = 18;
        double c2 = 6.5;
        double expected = 19.1376;

        double result = Processing.exercise10(c1, c2);

        assertEquals(expected, result, 0.0001);

    }

    @Test
    void exercise11() {

        double x = 10.7;
        double expected =  83.39;

        double result = Processing.exercise11(x);

        assertEquals(expected, result, 0.0001);

    }

    @Test
    void exercise12() {

        double celsius = 18.5;
        double expected =  65.3;

        double result = Processing.exercise12(celsius);

        assertEquals(expected, result, 0.0001);

    }

    @Test
    void exercise13() {

        int H = 1;
        int M = 34;
        int expected = 94;

        int result = Processing.exercise13(H, M);

        assertEquals(expected, result);

    }
}