package com.company;


public class Processing {

    public static double exercise3(double radius, double height) {

        double volume = (Math.PI * radius * radius * height) * 1000;

        return volume;

    }

    public static double exercise4(double time) {

        double distance = 340 * time / 1000;

        return distance;

    }

    public static double exercise5(double time, double initialSpeed) {

        final double GRAVITY = 9.8;

        double height = initialSpeed * time + (GRAVITY * time * time / 2);

        return height;

    }

    public static double exercise6(double buildingShadow, double personShadow, double personHeight) {

        double buildingHeight =  buildingShadow * personHeight / personShadow;

        return buildingHeight;

    }

    public static double exercise7(int person1Hours, int person1Minutes, int person1Seconds, int person2Hours, int person2Minutes, int person2Seconds) {

        final double DISTANCE = 42195.00;

        double speed = DISTANCE / (person1Hours * 3600 + person1Minutes * 60 + person1Seconds);

        double distance = speed * (person2Hours * 3600 + person2Minutes * 60 + person2Seconds) / 1000;

        return distance;

    }

    public static double exercise8(double cableLength1, double cableLength2, double angle) {

        double distance = Math.sqrt(cableLength1 * cableLength1 + cableLength2 * cableLength2 - 2 * cableLength1 * cableLength2 * Math.cos(Math.toRadians(angle)));

        return distance;

    }

    public static double exercise9(double A, double B, double x) {

        double perimeter = 2 * x * A + 2 * x * B;

        return perimeter;

    }

    public static double exercise10(double c1, double c2) {

        double h = Math.sqrt(Math.pow(c1, 2) + Math.pow(c2, 2));

        return h;

    }

    public static double exercise11(double x) {

        double result = Math.pow(x, 2) - 3 * x + 1;

        return result;

    }

    public static double exercise12(double celsius) {

        double fahrenheit = 1.8 * celsius + 32;

        return fahrenheit;

    }

    public static int exercise13(int H, int M) {

        int totalMinutes = H * 60 + M;

        return totalMinutes;

    }

}
