package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //exercise3();
        //exercise4();
        //exercise5();
        //exercise6();
        //exercise7();
        //exercise8();
        //exercise9();
        //exercise10();
        //exercise11();
        //exercise12();
        //exercise13();

    }

    public static void exercise3() {

        Scanner read = new Scanner(System.in);

        double radius, height, volume;

        System.out.println("Enter radius in meters.");
        radius = read.nextDouble();
        System.out.println("Enter height in meters.");
        height = read.nextDouble();

        volume = Processing.exercise3(radius, height);

        System.out.printf("The volume is %.2f l.", volume);
    }

    public static void exercise4() {

        Scanner read = new Scanner(System.in);

        double time, distance;

        System.out.println("Enter time in seconds.");
        time = read.nextDouble();

        distance = Processing.exercise4(time);

        System.out.printf("The distance is %.2f m.", distance);

    }

    public static void exercise5() {

        Scanner read = new Scanner(System.in);

        double time, initialSpeed, height;

        System.out.println("Enter time in seconds.");
        time = read.nextDouble();
        System.out.println("Enter initial speed in m/s.");
        initialSpeed = read.nextDouble();

        height = Processing.exercise5(time, initialSpeed);

        System.out.printf("The height is %.2f m.", height);

    }

    public static void exercise6() {

        Scanner read = new Scanner(System.in);

        double buildingShadow, personShadow, personHeight, buildingHeight;

        System.out.println("Enter length of building shadow in meters.");
        buildingShadow = read.nextDouble();
        System.out.println("Enter length of person shadow in meters.");
        personShadow = read.nextDouble();
        System.out.println("Enter height of person in meters.");
        personHeight = read.nextDouble();

        buildingHeight = Processing.exercise6(buildingShadow, personShadow, personHeight);

        System.out.printf("The building's height is %.2f m.", buildingHeight);

    }

    public static void exercise7() {

        Scanner read = new Scanner(System.in);

        int person1Hours, person1Minutes, person1Seconds, person2Hours, person2Minutes, person2Seconds;
        double distance;

        System.out.println("Enter number of hours person 1 completed.");
        person1Hours = read.nextInt();
        System.out.println("Enter number of minutes person 1 completed.");
        person1Minutes = read.nextInt();
        System.out.println("Enter number of seconds person 1 completed.");
        person1Seconds = read.nextInt();
        System.out.println("Enter number of hours person 2 completed.");
        person2Hours = read.nextInt();
        System.out.println("Enter number of minutes person 2 completed.");
        person2Minutes = read.nextInt();
        System.out.println("Enter number of seconds person 2 completed.");
        person2Seconds = read.nextInt();


        distance = Processing.exercise7(person1Hours, person1Minutes, person1Seconds, person2Hours, person2Minutes, person2Seconds);

        System.out.printf("The distance is %.2f m.", distance);

    }

    public static void exercise8() {

        Scanner read = new Scanner(System.in);

        double cableLength1, cableLength2, angle, distance;

        System.out.println("Enter length of cable 1 in meters.");
        cableLength1 = read.nextDouble();
        System.out.println("Enter length of cable 2 in meters.");
        cableLength2 = read.nextDouble();
        System.out.println("Enter value of angle in degrees.");
        angle = read.nextDouble();

        distance = Processing.exercise8(cableLength1, cableLength2, angle);

        System.out.printf("The distance is %.2f m.", distance);

    }

    public static void exercise9() {

        Scanner read = new Scanner(System.in);

        double A, B, x, perimeter;

        System.out.println("Enter A.");
        A = read.nextDouble();
        System.out.println("Enter B.");
        B = read.nextDouble();
        System.out.println("Enter x.");
        x = read.nextDouble();

        perimeter = Processing.exercise9(A, B, x);

        System.out.printf("The perimeter is %.2f.", perimeter);

    }

    public static void exercise10() {

        Scanner read = new Scanner(System.in);

        double c1, c2, h;

        System.out.println("Enter c1.");
        c1 = read.nextDouble();
        System.out.println("Enter c2.");
        c2 = read.nextDouble();

        h = Processing.exercise10(c1, c2);

        System.out.printf("The hypotenuse is %.2f.", h);

    }

    public static void exercise11() {

        Scanner read = new Scanner(System.in);

        double x, result;

        System.out.println("Enter x.");
        x = read.nextDouble();

        result = Processing.exercise11(x);

        System.out.printf("The result is %.2f.", result);

    }

    public static void exercise12() {

        Scanner read = new Scanner(System.in);

        double celsius, fahrenheit;

        System.out.println("Enter value of temperature in Celsius.");
        celsius = read.nextDouble();

        fahrenheit = Processing.exercise12(celsius);

        System.out.printf("The value of the temperature in fahrenheit is %.2f.", fahrenheit);

    }

    public static void exercise13() {

        Scanner read = new Scanner(System.in);

        int H, M, totalMinutes;

        System.out.println("Enter value of H.");
        H = read.nextInt();
        System.out.println("Enter value of M.");
        M = read.nextInt();


        totalMinutes = Processing.exercise13(H, M);

        System.out.printf("The total value in minutes is %d.", totalMinutes);

    }


}
