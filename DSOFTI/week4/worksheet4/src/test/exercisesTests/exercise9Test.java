import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise9Test {

    @Test
    void isNumberPalindrome_oneDigit() {

        int number = 1;

        boolean result = exercise9.isNumberPalindrome(number);

        assertEquals(true, result);
    }

    @Test
    void isNumberPalindrome_yes() {

        int number = 54045;

        boolean result = exercise9.isNumberPalindrome(number);

        assertEquals(true, result);
    }

    @Test
    void isNumberPalindrome_no() {

        int number = 12345321;

        boolean result = exercise9.isNumberPalindrome(number);

        assertEquals(false, result);
    }

}