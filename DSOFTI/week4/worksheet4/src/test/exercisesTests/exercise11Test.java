import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise11Test {

    @Test
    void getScalarProduct() {

        int[] array1 = {1, 3, 5, 7, 9};
        int[] array2 = {2, 4, 6, 8, 10};
        int expected = 190;

        int result = exercise11.getScalarProduct(array1, array2);

        assertEquals(expected, result);
    }
}