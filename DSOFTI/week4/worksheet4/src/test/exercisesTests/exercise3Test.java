import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise3Test {

    @Test
    void sumArrayElements_equal0() {
        int[] array = {0, 0, 0};
        int expected = 0;

        int result = exercise3.sumArrayElements(array);

        assertEquals(expected, result);
    }

    @Test
    void sumArrayElements_positiveNumber() {
        int[] array = {8, 3, 1};
        int expected = 12;

        int result = exercise3.sumArrayElements(array);

        assertEquals(expected, result);
    }

}