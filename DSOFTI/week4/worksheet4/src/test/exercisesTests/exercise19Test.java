import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise19Test {

    @Test
    void play_emptySpace() {
        int[][] sudoku = {  {0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[][] filledSpacesMask = {{0, 0, 0, 1, 1, 0, 1, 0, 1},
                                    {1, 1, 0, 0, 1, 0, 0, 1, 0},
                                    {1, 1, 0, 0, 0, 1, 1, 0, 0},
                                    {1, 1, 0, 1, 0, 0, 0, 1, 0},
                                    {0, 0, 1, 1, 0, 1, 1, 0, 0},
                                    {0, 1, 0, 0, 0, 1, 0, 1, 1},
                                    {0, 0, 1, 1, 0, 0, 0, 1, 1},
                                    {0, 1, 0, 0, 1, 0, 0, 1, 1},
                                    {1, 0, 1, 0, 1, 1, 0, 0, 0}};
        int[] position = {0, 0};
        int number = 3;
        int[][] expected = {{3, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}};
        int[][] result = exercise19.play(sudoku, filledSpacesMask, position, number);
        assertArrayEquals(expected, result);
    }

    @Test
    void play_fullSpace() {
        int[][] sudoku = {  {0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[][] filledSpacesMask = {{0, 0, 0, 1, 1, 0, 1, 0, 1},
                                    {1, 1, 0, 0, 1, 0, 0, 1, 0},
                                    {1, 1, 0, 0, 0, 1, 1, 0, 0},
                                    {1, 1, 0, 1, 0, 0, 0, 1, 0},
                                    {0, 0, 1, 1, 0, 1, 1, 0, 0},
                                    {0, 1, 0, 0, 0, 1, 0, 1, 1},
                                    {0, 0, 1, 1, 0, 0, 0, 1, 1},
                                    {0, 1, 0, 0, 1, 0, 0, 1, 1},
                                    {1, 0, 1, 0, 1, 1, 0, 0, 0}};
        int[] position = {1, 0};
        int number = 3;
        int[][] expected = {{0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}};
        int[][] result = exercise19.play(sudoku, filledSpacesMask, position, number);
        assertArrayEquals(expected, result);
    }

    @Test
    void filledSpacesMask() {
        int[][] sudoku = {  {0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[][] expected = {{0, 0, 0, 1, 1, 0, 1, 0, 1},
                            {1, 1, 0, 0, 1, 0, 0, 1, 0},
                            {1, 1, 0, 0, 0, 1, 1, 0, 0},
                            {1, 1, 0, 1, 0, 0, 0, 1, 0},
                            {0, 0, 1, 1, 0, 1, 1, 0, 0},
                            {0, 1, 0, 0, 0, 1, 0, 1, 1},
                            {0, 0, 1, 1, 0, 0, 0, 1, 1},
                            {0, 1, 0, 0, 1, 0, 0, 1, 1},
                            {1, 0, 1, 0, 1, 1, 0, 0, 0}};
        int[][] result = exercise19.filledSpacesMask(sudoku);
        assertArrayEquals(expected, result);
    }

    @Test
    void erasePlay_erasableSpace() {
        int[][] sudoku = {  {3, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[][] playerActionsMask= {{1, 0, 0, 0, 0, 0, 0, 0, 0},
                                    {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                    {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                    {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                    {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                    {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                    {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                    {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                    {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        int[] position = {0, 0};
        int[][] expected = {{0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}};

        int[][] result = exercise19.erasePlay(sudoku, playerActionsMask, position);
        assertArrayEquals(expected, result);
    }

    @Test
    void erasePlay_nonErasableSpace() {
        int[][] sudoku = {  {3, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[][] playerActionsMask= {{1, 0, 0, 0, 0, 0, 0, 0, 0},
                                    {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                    {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                    {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                    {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                    {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                    {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                    {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                    {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        int[] position = {1, 0};
        int[][] expected = {{3, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}};

        int[][] result = exercise19.erasePlay(sudoku, playerActionsMask, position);
        assertArrayEquals(expected, result);
    }

    @Test
    void playerActionsMask() {
        int[][] sudoku = {  {3, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[][] presetNumbersMask ={{0, 0, 0, 1, 1, 0, 1, 0, 1},
                                    {1, 1, 0, 0, 1, 0, 0, 1, 0},
                                    {1, 1, 0, 0, 0, 1, 1, 0, 0},
                                    {1, 1, 0, 1, 0, 0, 0, 1, 0},
                                    {0, 0, 1, 1, 0, 1, 1, 0, 0},
                                    {0, 1, 0, 0, 0, 1, 0, 1, 1},
                                    {0, 0, 1, 1, 0, 0, 0, 1, 1},
                                    {0, 1, 0, 0, 1, 0, 0, 1, 1},
                                    {1, 0, 1, 0, 1, 1, 0, 0, 0}  };
        int[][] expected = {{1, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0}};

        int[][] result = exercise19.playerActionsMask(sudoku, presetNumbersMask);
        assertArrayEquals(expected, result);
    }

    @Test
    void validPlay_true() {
        int[][] sudoku = {  {0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[][] SECTION_LIMITS = {  {0, 2, 0, 2},
                                    {0, 2, 3, 5},
                                    {0, 2, 6, 8},
                                    {3, 5, 0, 2},
                                    {3, 5, 3, 5},
                                    {3, 5, 6, 8},
                                    {6, 8, 0, 2},
                                    {6, 8, 3, 5},
                                    {6, 8, 6, 8}  };
        int[] position = {0, 0};
        int number = 3;
        boolean result = exercise19.validPlay(sudoku, SECTION_LIMITS, position, number);
        assertTrue(result);
    }

    @Test
    void validPlay_falseColumn() {
        int[][] sudoku = {  {0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[][] SECTION_LIMITS = {  {0, 2, 0, 2},
                                    {0, 2, 3, 5},
                                    {0, 2, 6, 8},
                                    {3, 5, 0, 2},
                                    {3, 5, 3, 5},
                                    {3, 5, 6, 8},
                                    {6, 8, 0, 2},
                                    {6, 8, 3, 5},
                                    {6, 8, 6, 8}  };
        int[] position = {0, 0};
        int number = 6;
        boolean result = exercise19.validPlay(sudoku, SECTION_LIMITS, position, number);
        assertFalse(result);
    }

    @Test
    void validPlay_falseRow() {
        int[][] sudoku = {  {0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[][] SECTION_LIMITS = {  {0, 2, 0, 2},
                                    {0, 2, 3, 5},
                                    {0, 2, 6, 8},
                                    {3, 5, 0, 2},
                                    {3, 5, 3, 5},
                                    {3, 5, 6, 8},
                                    {6, 8, 0, 2},
                                    {6, 8, 3, 5},
                                    {6, 8, 6, 8}  };
        int[] position = {0, 0};
        int number = 2;
        boolean result = exercise19.validPlay(sudoku, SECTION_LIMITS, position, number);
        assertFalse(result);
    }

    @Test
    void validPlay_falseSection() {
        int[][] sudoku = {  {0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[][] SECTION_LIMITS = {  {0, 2, 0, 2},
                                    {0, 2, 3, 5},
                                    {0, 2, 6, 8},
                                    {3, 5, 0, 2},
                                    {3, 5, 3, 5},
                                    {3, 5, 6, 8},
                                    {6, 8, 0, 2},
                                    {6, 8, 3, 5},
                                    {6, 8, 6, 8}  };
        int[] position = {0, 0};
        int number = 9;
        boolean result = exercise19.validPlay(sudoku, SECTION_LIMITS, position, number);
        assertFalse(result);
    }

    @Test
    void rowRule_true() {
        int[][] sudoku = {  {0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[] position = {0, 0};
        int number = 8;
        boolean result = exercise19.rowRule(sudoku, position, number);
        assertTrue(result);
    }


    @Test
    void rowRule_false() {
        int[][] sudoku = {  {0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[] position = {0, 0};
        int number = 2;
        boolean result = exercise19.rowRule(sudoku, position, number);
        assertFalse(result);
    }

    @Test
    void columnRule_true() {
        int[][] sudoku = {  {0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[] position = {4, 0};
        int number = 4;
        boolean result = exercise19.columnRule(sudoku, position, number);
        assertTrue(result);
    }

    @Test
    void columnRule_false() {
        int[][] sudoku = {  {0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[] position = {0, 0};
        int number = 8;
        boolean result = exercise19.columnRule(sudoku, position, number);
        assertFalse(result);
    }

    @Test
    void searchNumber_true() {
        int[] array = {5, 2, 8, 3, 0};
        int number = 8;
        boolean result = exercise19.numberInArray(array, number);
        assertTrue(result);
    }

    @Test
    void searchNumber_false() {
        int[] array = {5, 2, 8, 3, 0};
        int number = 7;
        boolean result = exercise19.numberInArray(array, number);
        assertFalse(result);
    }

    @Test
    void sectionRule_true() {
        int[][] section = { {0, 0, 3},
                            {8, 2, 5},
                            {0, 1, 4} };
        int number = 6;
        boolean result = exercise19.sectionRule(section, number);
        assertTrue(result);
    }

    @Test
    void sectionRule_false() {
        int[][] section = { {0, 0, 3},
                            {8, 2, 5},
                            {0, 1, 4} };
        int number = 4;
        boolean result = exercise19.sectionRule(section, number);
        assertFalse(result);
    }


    @Test
    void isolateSectionContents_firstSection() {
        int[][] sudoku = {  {0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[][] SECTION_LIMITS = {  {0, 2, 0, 2},
                                    {0, 2, 3, 5},
                                    {0, 2, 6, 8},
                                    {3, 5, 0, 2},
                                    {3, 5, 3, 5},
                                    {3, 5, 6, 8},
                                    {6, 8, 0, 2},
                                    {6, 8, 3, 5},
                                    {6, 8, 6, 8}  };
        int sectionMatched = 1;
        int[][] expected = {{0, 0, 0},
                            {6, 8, 0},
                            {1, 9, 0}};
        int[][] result = exercise19.isolateSectionContents(sudoku, SECTION_LIMITS, sectionMatched);
        assertArrayEquals(expected, result);
    }

    @Test
    void isolateSectionContents_lastSection() {
        int[][] sudoku = {  {0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[][] SECTION_LIMITS = {  {0, 2, 0, 2},
                                    {0, 2, 3, 5},
                                    {0, 2, 6, 8},
                                    {3, 5, 0, 2},
                                    {3, 5, 3, 5},
                                    {3, 5, 6, 8},
                                    {6, 8, 0, 2},
                                    {6, 8, 3, 5},
                                    {6, 8, 6, 8}  };
        int sectionMatched = 9;
        int[][] expected = {{0, 7, 4},
                            {0, 3, 6},
                            {0, 0, 0}};
        int[][] result = exercise19.isolateSectionContents(sudoku, SECTION_LIMITS, sectionMatched);
        assertArrayEquals(expected, result);
    }

    @Test
    void getSectionNumber_firstSection() {
        int[][] SECTION_LIMITS = {  {0, 2, 0, 2},
                                    {0, 2, 3, 5},
                                    {0, 2, 6, 8},
                                    {3, 5, 0, 2},
                                    {3, 5, 3, 5},
                                    {3, 5, 6, 8},
                                    {6, 8, 0, 2},
                                    {6, 8, 3, 5},
                                    {6, 8, 6, 8}  };
        int[] position = {0, 0};
        int expected = 1;
        int result = exercise19.getSectionNumber(SECTION_LIMITS, position);
        assertEquals(expected, result);
    }

    @Test
    void getSectionNumber_lastSection() {
        int[][] SECTION_LIMITS = {  {0, 2, 0, 2},
                                    {0, 2, 3, 5},
                                    {0, 2, 6, 8},
                                    {3, 5, 0, 2},
                                    {3, 5, 3, 5},
                                    {3, 5, 6, 8},
                                    {6, 8, 0, 2},
                                    {6, 8, 3, 5},
                                    {6, 8, 6, 8}  };
        int[] position = {8, 8};
        int expected = 9;
        int result = exercise19.getSectionNumber(SECTION_LIMITS, position);
        assertEquals(expected, result);
    }


    @Test
    void withinSectionLimits_true() {
        int[] upDownLeftRightLimits = {0, 2, 0, 2};
        int[] position = {2, 2};
        boolean result = exercise19.withinSectionLimits(upDownLeftRightLimits, position);
        assertTrue(result);
    }

    @Test
    void withinSectionLimits_false() {
        int[] upDownLeftRightLimits = {0, 2, 0, 2};
        int[] position = {2, 3};
        boolean result = exercise19.withinSectionLimits(upDownLeftRightLimits, position);
        assertFalse(result);
    }

    @Test
    void finishedGame_finished() {
        int[][] sudoku = {  {1, 1, 1, 2, 6, 1, 7, 1, 1},
                            {6, 8, 1, 1, 7, 1, 1, 9, 1},
                            {1, 9, 1, 1, 1, 4, 5, 1, 1},
                            {8, 2, 1, 1, 1, 1, 1, 4, 1},
                            {1, 1, 4, 6, 1, 2, 9, 1, 1},
                            {1, 5, 1, 1, 1, 3, 1, 2, 8},
                            {1, 1, 9, 3, 1, 1, 1, 7, 4},
                            {1, 4, 1, 1, 5, 1, 1, 3, 6},
                            {7, 1, 3, 1, 1, 8, 1, 1, 1}  };
        boolean result = exercise19.finishedGame(sudoku);
        assertTrue(result);
    }

    @Test
    void finishedGame_unfinished() {
        int[][] sudoku = {  {1, 1, 1, 2, 6, 1, 7, 1, 1},
                            {6, 8, 1, 1, 7, 1, 1, 9, 1},
                            {1, 9, 1, 1, 1, 4, 5, 1, 1},
                            {8, 2, 1, 1, 1, 1, 1, 4, 1},
                            {1, 1, 4, 6, 1, 2, 9, 1, 1},
                            {1, 5, 1, 1, 1, 3, 1, 2, 8},
                            {1, 1, 9, 3, 1, 1, 1, 7, 4},
                            {1, 4, 1, 1, 5, 1, 1, 3, 6},
                            {7, 1, 3, 1, 1, 8, 1, 1, 0}  };
        boolean result = exercise19.finishedGame(sudoku);
        assertFalse(result);
    }


    @Test
    void emptySpacesMask() {
        int[][] sudoku = {  {0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int[][] expected = {{1, 1, 1, 0, 0, 1, 0, 1, 0},
                            {0, 0, 1, 1, 0, 1, 1, 0, 1},
                            {0, 0, 1, 1, 1, 0, 0, 1, 1},
                            {0, 0, 1, 0, 1, 1, 1, 0, 1},
                            {1, 1, 0, 0, 1, 0, 0, 1, 1},
                            {1, 0, 1, 1, 1, 0, 1, 0, 0},
                            {1, 1, 0, 0, 1, 1, 1, 0, 0},
                            {1, 0, 1, 1, 0, 1, 1, 0, 0},
                            {0, 1, 0, 1, 0, 0, 1, 1, 1}};
        int[][] result = exercise19.emptySpacesMask(sudoku);
        assertArrayEquals(expected, result);
    }

    @Test
    void specificNumberMask() {
        int[][] sudoku = {  {0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        int number = 1;
        int[][] expected = {{0, 0, 0, 0, 0, 0, 0, 0, 1},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {1, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 1, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 1, 0, 0, 0, 0}};
        int[][] result = exercise19.specificNumberMask(sudoku, number);
        assertArrayEquals(expected, result);
    }
}