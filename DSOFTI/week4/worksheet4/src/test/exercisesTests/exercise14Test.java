import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise14Test {

    @Test
    void is2DArrayRectangular_true() {

        int[][] matrix = {{1},{2}};

        boolean result = exercise14.isMatrixRectangular(matrix);

        assertEquals(true, result);
    }

    @Test
    void is2DArrayRectangular_false_square() {

        int[][] matrix = {{1, 2, 3},{1, 2, 3}, {1, 2, 3}};

        boolean result = exercise14.isMatrixRectangular(matrix);

        assertEquals(false, result);
    }

    @Test
    void is2DArrayRectangular_false_irregular() {

        int[][] matrix = {{1, 2, 3}, {1, 2, 3}, {1, 2, 3, 4}};

        boolean result = exercise14.isMatrixRectangular(matrix);

        assertEquals(false, result);
    }


}