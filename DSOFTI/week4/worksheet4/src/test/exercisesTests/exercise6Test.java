import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise6Test {

    @Test
    void getTruncatedArray_noElements() {
        int[] array = {1, 20, 8, 5, 0};
        int elementsCount = 0;
        int[] expected = {};

        int[] result = exercise6.getTruncatedArray(array, elementsCount);

        assertArrayEquals(expected, result);
    }

    @Test
    void getTruncatedArray_someElements() {
        int[] array = {1, 20, 8, 5, 0};
        int elementsCount = 2;
        int[] expected = {1, 20};

        int[] result = exercise6.getTruncatedArray(array, elementsCount);

        assertArrayEquals(expected, result);
    }

    @Test
    void getTruncatedArray_allElements() {
        int[] array = {1, 20, 8, 5, 0};
        int elementsCount = 5;
        int[] expected = {1, 20, 8, 5, 0};

        int[] result = exercise6.getTruncatedArray(array, elementsCount);

        assertArrayEquals(expected, result);
    }


}