import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise8Test {

    @Test
    void countMultiplesOfNumbersInArray() {

        int lowerLimit = 1;
        int upperLimit = 20;
        int[] numbers = {2, 3};
        int expected = 3;

        int result = exercise8.countMultiplesOfNumbersInArray(lowerLimit, upperLimit, numbers);

        assertEquals(expected, result);
    }

    @Test
    void arrayOfMultiplesOfNumbersInArray() {

        int lowerLimit = 1;
        int upperLimit = 20;
        int[] numbers = {2, 3};
        int[] expected = {6, 12, 18};

        int[] result = exercise8.arrayOfMultiplesOfNumbersInArray(lowerLimit, upperLimit, numbers);

        assertArrayEquals(expected, result);
    }
}