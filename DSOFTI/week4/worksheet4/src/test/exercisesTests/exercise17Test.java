import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise17Test {

    @Test
    void product_constantXMatrix() {

        int[][] matrix = {{1, 2, 3}, {4, 5, 6}};
        int constant = 10;
        int[][] expected = {{10, 20 ,30}, {40, 50, 60}};

        int[][] result = exercise17.product(matrix, constant);

        assertArrayEquals(expected, result);
    }

    @Test
    void sum() {

        int[][] matrix1 = {{1, 2, 3}, {4, 5, 6}};
        int[][] matrix2 = {{4, 5, 6}, {1, 2, 3}};
        int[][] expected = {{5, 7, 9}, {5, 7, 9}};

        int[][] result = exercise17.sum(matrix1, matrix2);

        assertArrayEquals(expected, result);
    }

    @Test
    void Product_MatrixXMatrix() {

        int[][] matrix1 = {{1, 2, 3}, {4, 5, 6}};
        int[][] matrix2 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] expected = {{30, 36, 42}, {66, 81, 96}};

        int[][] result = exercise17.product(matrix1, matrix2);

        assertArrayEquals(expected, result);
    }
}