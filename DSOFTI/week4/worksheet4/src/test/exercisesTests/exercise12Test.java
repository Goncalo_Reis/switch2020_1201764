import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise12Test {

    @Test
    void compareMatrixLinesAndColumns_notEqual() {

        int[][] matrix = {{1, 2, 3}, {1, 2}, {1, 2, 3}};
        int expected = -1;

        int result = exercise12.countLineElements(matrix);

        assertEquals(expected, result);
    }

    @Test
    void compareMatrixLinesAndColumns_Equal() {

        int[][] matrix = {{1, 2, 3}, {1, 2, 3}, {1, 2, 3}};
        int expected = 3;

        int result = exercise12.countLineElements(matrix);

        assertEquals(expected, result);
    }
}