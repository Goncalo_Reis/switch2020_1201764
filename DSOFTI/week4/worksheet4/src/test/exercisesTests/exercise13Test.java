import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise13Test {

    @Test
    void isMatrixSquare_true() {

        int[][] matrix = {{1, 2}, {1, 2}};

        boolean result = exercise13.isMatrixSquare(matrix);

        assertEquals(true, result);
    }

    @Test
    void isMatrixSquare_false_tallRectangle() {

        int[][] matrix = {{1, 2}, {1, 2}, {1, 2}};

        boolean result = exercise13.isMatrixSquare(matrix);

        assertEquals(false, result);
    }

    @Test
    void isMatrixSquare_false_wideRectangle() {

        int[][] matrix = {{1, 2, 3}, {1, 2, 3}};

        boolean result = exercise13.isMatrixSquare(matrix);

        assertEquals(false, result);
    }

    @Test
    void isMatrixSquare_false_irregular() {

        int[][] matrix = {{1, 2, 3}, {1, 2, 3}, {1, 2, 3, 4}};

        boolean result = exercise13.isMatrixSquare(matrix);

        assertEquals(false, result);
    }

}