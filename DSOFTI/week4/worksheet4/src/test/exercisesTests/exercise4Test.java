import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise4Test {

    @Test
    void countEvenElements() {
        int[] array = {2, 4, 7, 8};
        int expected = 3;

        int result = exercise4.countEvenElements(array);

        assertEquals(expected, result);
    }

    @Test
    void getEvenArray_element0() {
        int[] array = {0, 3};
        int[] expected = {0};

        int[] result = exercise4.getEvenArray(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void getEvenArray_allEven() {
        int[] array = {2, 4, 6, 8};
        int[] expected = {2, 4, 6, 8};

        int[] result = exercise4.getEvenArray(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void getEvenArray_allOdd() {
        int[] array = {1, 3, 5, 7, 9};
        int[] expected = {};

        int[] result = exercise4.getEvenArray(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void countOddElements() {
        int[] array = {2, 4, 7, 8};
        int expected = 1;

        int result = exercise4.countOddElements(array);

        assertEquals(expected, result);
    }

    @Test
    void getOddArray_element0() {
        int[] array = {0, 3};
        int[] expected = {3};

        int[] result = exercise4.getOddArray(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void getOddArray_allEven() {
        int[] array = {2, 4, 6, 8};
        int[] expected = {};

        int[] result = exercise4.getOddArray(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void getOddArray_allOdd() {
        int[] array = {1, 3, 5, 7, 9};
        int[] expected = {1, 3, 5, 7, 9};

        int[] result = exercise4.getOddArray(array);

        assertArrayEquals(expected, result);
    }


}