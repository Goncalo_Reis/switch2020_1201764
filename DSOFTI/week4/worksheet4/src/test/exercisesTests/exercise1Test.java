import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise1Test {

    @Test
    void countDigits_oneDigit() {
        long number = 9;
        int expected = 1;

        int result = exercise1.countDigits(number);

        assertEquals(expected, result);
    }

    @Test
    void countDigits_multipleDigits() {
        long number = 8462961286L;
        int expected = 10;

        int result = exercise1.countDigits(number);

        assertEquals(expected, result);
    }

}