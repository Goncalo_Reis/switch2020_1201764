import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import static org.junit.jupiter.api.Assertions.*;

class exercise15Test {

    @Test
    void getLowestElement() {

        int[][] matrix = {{2, 1}, {4, 3}};
        int expected = 1;

        int result = exercise15.getLowestElement(matrix);

        assertEquals(expected, result);
    }

    @Test
    void getHighestElement() {

        int[][] matrix = {{2, 1}, {4, 3}};
        int expected = 4;

        int result = exercise15.getHighestElement(matrix);

        assertEquals(expected, result);
    }

    @Test
    void getAverage() {

        int[][] matrix = {{2, 1}, {4, 3}};
        float expected = 2.5F;

        float result = exercise15.getAverage(matrix);

        assertEquals(expected, result);
    }

    @Test
    void getProduct() {

        int[][] matrix = {{2, 1}, {4, 3}};
        int expected = 24;

        int result = exercise15.getProduct(matrix);

        assertEquals(expected, result);
    }


    @Test
    void repeatedElementsCount() {

        int[][] matrix = { {1, 2, 2}, {2, 3, 9}, {3, 5, 1} };
        int expected = 4;

        int result = exercise15.repeatedElementsCount(matrix);

        assertEquals(expected, result);
    }

    @Test
    void repeatedElementsIndexes() {

        int[][] matrix = { {1, 2, 2}, {2, 3, 9}, {3, 5, 1} };
        int[][] expected = { {2, 0, 1, 2}, {2, 2, 0, 0} };

        int[][] result = exercise15.repeatedElementsIndexes(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void excludeRepeatedElements() {

        int[][] matrix = { {1, 2, 2}, {2, 3, 9}, {3, 5, 1} };
        int[] expected = {1, 2, 3, 9, 5};

        int[] result = exercise15.excludeRepeatedElements(matrix);

        assertArrayEquals(expected, result);
    }


    @Test
    void countPrimeNumbers() {

        int[][] matrix = { {1, 2, 2}, {2, 3, 9}, {3, 5, 1} };
        int expected = 6;

        int result = exercise15.countPrimeNumbers(matrix);

        assertEquals(expected, result);
    }

    @Test
    void getPrimeNumbers() {

        int[][] matrix = { {1, 2, 2}, {2, 3, 9}, {3, 5, 1} };
        int[] expected = {2, 2, 2, 3, 3, 5};

        int[] result = exercise15.getPrimeNumbers(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void countMainDiagonal() {

        int[][] matrix = { {1, 2, 2}, {2, 3, 9}, {3, 5, 1} };
        int expected = 3;

        int result = exercise15.countMainDiagonal(matrix);

        assertEquals(expected, result);
    }

    @Test
    void getMainDiagonal() {

        int[][] matrix = { {1, 2, 2}, {2, 3, 9}, {3, 5, 1} };
        int[] expected = {1, 3, 1};

        int[] result = exercise15.getMainDiagonal(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void countSecondaryDiagonal() {

        int[][] matrix = { {1, 2, 2}, {2, 3, 9}, {3, 5, 1} };
        int expected = 3;

        int result = exercise15.countSecondaryDiagonal(matrix);

        assertEquals(expected, result);
    }

    @Test
    void getSecondaryDiagonal() {

        int[][] matrix = { {1, 2, 2}, {2, 3, 9}, {3, 5, 1} };
        int[] expected = {2, 3, 3};

        int[] result = exercise15.getSecondaryDiagonal(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void isIdentityMatrix_false() {

        int[][] matrix = { {1, 2, 2}, {2, 3, 9}, {3, 5, 1} };

        boolean result = exercise15.isIdentityMatrix(matrix);

        assertFalse(result);
    }

    @Test
    void isIdentityMatrix_true() {

        int[][] matrix = { {1, 0, 0}, {0, 1, 0}, {0, 0, 1} };

        boolean result = exercise15.isIdentityMatrix(matrix);

        assertTrue(result);
    }

    @Test
    void reducedMatrix_2x2() {

        int[][] matrix = {{1, 2}, {3, 4}};
        int line = 0;
        int column = 0;
        int[][] expected = {{4}};

        int[][] result = exercise15.reducedMatrix(matrix, line, column);

        assertArrayEquals(expected, result);
    }

    @Test
    void reducedMatrix_3x3() {

        int[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int line = 0;
        int column = 2;
        int[][] expected = {{4, 5}, {7, 8}};

        int[][] result = exercise15.reducedMatrix(matrix, line, column);

        assertArrayEquals(expected, result);
    }

    @Test
    void reducedMatrix_5x5() {

        int[][] matrix = {{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}, {11, 12, 13, 14, 15}, {16, 17, 18, 19, 20}, {21, 22, 23, 24, 25}};
        int line = 4;
        int column = 4;
        int[][] expected = {{1, 2, 3, 4}, {6, 7, 8, 9}, {11, 12, 13, 14}, {16, 17, 18, 19}};

        int[][] result = exercise15.reducedMatrix(matrix, line, column);

        assertArrayEquals(expected, result);
    }


    @Test
    void determinant_2x2() {

        int[][] matrix = {{1, 2}, {3, 4}};
        int expected = -2;

        int result = exercise15.determinant(matrix);

        assertEquals(expected, result);
    }

    @Test
    void determinant_3x3() {

        int[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int expected = 0;

        int result = exercise15.determinant(matrix);

        assertEquals(expected, result);
    }

    @Test
    void determinant_5x5() {

        int[][] matrix = {{0, 23, 5, 6, 37}, {12, -54, 7, 1, -8}, {-43, 5, 10, 123, -78}, {12, 34, 90, -4, 0}, {-9, 3, 44, 80, -1}};
        int expected = -86194782;

        int result = exercise15.determinant(matrix);

        assertEquals(expected, result);
    }

    @Test
    void minorsMatrix_2x2() {

        int[][] matrix = {{1, 2}, {3, 4}};
        int[][] expected = {{4, 3}, {2, 1}};

        int[][] result = exercise15.minorsMatrix(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void minorsMatrix_3x3() {

        int[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] expected = {{-3, -6, -3}, {-6, -12, -6}, {-3, -6, -3}};

        int[][] result = exercise15.minorsMatrix(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void minorsMatrix_5x5() {

        int[][] matrix = {{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}, {11, 12, 13, 14, 15}, {16, 17, 18, 19, 20}, {21, 22, 23, 24, 25}};
        int[][] expected = {{0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}};

        int[][] result = exercise15.minorsMatrix(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void cofactorsMatrix_2x2() {

        int[][] matrix = {{1, 2}, {3, 4}};
        int[][] expected = {{4, -3}, {-2, 1}};

        int[][] result = exercise15.cofactorsMatrix(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void cofactorsMatrix_3x3() {

        int[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] expected = {{-3, 6, -3}, {6, -12, 6}, {-3, 6, -3}};

        int[][] result = exercise15.cofactorsMatrix(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void cofactorsMatrix_5x5() {

        int[][] matrix = {{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}, {11, 12, 13, 14, 15}, {16, 17, 18, 19, 20}, {21, 22, 23, 24, 25}};
        int[][] expected = {{0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}};

        int[][] result = exercise15.cofactorsMatrix(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void transposeMatrix_2x2() {

        int[][] matrix = {{1, 2}, {3, 4}};
        int[][] expected = {{1, 3}, {2, 4}};

        int[][] result = exercise15.transposeMatrix(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void transposeMatrix_3x3() {

        int[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] expected = {{1, 4, 7}, {2, 5, 8}, {3, 6, 9}};

        int[][] result = exercise15.transposeMatrix(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void transposeMatrix_5x5() {

        int[][] matrix = {{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}, {11, 12, 13, 14, 15}, {16, 17, 18, 19, 20}, {21, 22, 23, 24, 25}};
        int[][] expected = {{1, 6, 11, 16, 21}, {2, 7, 12, 17, 22}, {3, 8, 13, 18, 23}, {4, 9, 14, 19, 24}, {5, 10, 15, 20, 25}};

        int[][] result = exercise15.transposeMatrix(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void adjointMatrix_2x2() {

        int[][] matrix = {{1, 2}, {3, 4}};
        int[][] expected = {{4, -2}, {-3, 1}};

        int[][] result = exercise15.adjointMatrix(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void adjointMatrix_3x3() {

        int[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] expected = {{-3, 6, -3}, {6, -12, 6}, {-3, 6, -3}};

        int[][] result = exercise15.adjointMatrix(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void adjointMatrix_5x5() {

        int[][] matrix = {{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}, {11, 12, 13, 14, 15}, {16, 17, 18, 19, 20}, {21, 22, 23, 24, 25}};
        int[][] expected = {{0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}};

        int[][] result = exercise15.adjointMatrix(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void inverseMatrix_2x2() {

        int[][] matrix = {{1, 2}, {3, 4}};
        float[][] expected = {{-2, 1}, {1.5F, -0.5F}};

        float[][] result = exercise15.inverseMatrix(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void inverseMatrix_3x3() {

        int[][] matrix = {{1, 4, 5}, {7, 8, 2}, {6, 6, 0}};
        float[][] expected = {{-2, 5, -5.3333335F}, {2, -5, 5.5F}, {-1, 3, -3.3333335F}};

        float[][] result = exercise15.inverseMatrix(matrix);

        assertArrayEquals(expected, result);
    }

    @Test
    void inverseMatrix_5x5() {

        int[][] matrix = {{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}, {11, 12, 13, 14, 15}, {16, 17, 18, 19, 20}, {21, 22, 23, 24, 25}};
        float[][] expected = {{}};

        float[][] result = exercise15.inverseMatrix(matrix);

        try {
            assertArrayEquals(expected, result);
        } catch (AssertionFailedError error) {
            System.out.println("Singular Matrix.");
        }
    }
}