import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise7Test {

    @Test
    void countMultiplesOfNumber() {
        int lowerLimit = 0;
        int upperLimit = 18;
        int number = 3;
        int expected = 7;

        int result = exercise7.countMultiplesOfNumber(lowerLimit, upperLimit, number);

        assertEquals(expected, result);
    }

    @Test
    void arrayMultiplesOfNumber() {

        int lowerLimit = 2;
        int upperLimit = 10;
        int number = 2;
        int[] expected = {2, 4, 6, 8, 10};

        int[] result = exercise7.arrayMultiplesOfNumber(lowerLimit, upperLimit, number);

        assertArrayEquals(expected, result);
    }


}