import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise5Test {

    @Test
    void arrayEvenDigits_noEvenDigits() {
        long number = 13579;
        int[] expected = {};

        int[] result = exercise5.arrayEvenDigits(number);

        assertArrayEquals(expected, result);
    }

    @Test
    void arrayEvenDigits_someEvenDigits() {
        long number = 1235679;
        int[] expected = {2, 6};

        int[] result = exercise5.arrayEvenDigits(number);

        assertArrayEquals(expected, result);
    }

    @Test
    void arrayEvenDigits_allEvenDigits() {
        long number = 2468;
        int[] expected = {2, 4, 6, 8};

        int[] result = exercise5.arrayEvenDigits(number);

        assertArrayEquals(expected, result);
    }

    @Test
    void sumEvenDigits_noEvenDigits() {
        long number = 13579;
        int expected = 0;

        int result = exercise5.sumEvenDigits(number);

        assertEquals(expected, result);
    }

    @Test
    void sumEvenDigits_someEvenDigits() {
        long number = 183256;
        int expected = 16;

        int result = exercise5.sumEvenDigits(number);

        assertEquals(expected, result);
    }

    @Test
    void sumEvenDigits_allEvenDigits() {
        long number = 2468;
        int expected = 20;

        int result = exercise5.sumEvenDigits(number);

        assertEquals(expected, result);
    }
}
