import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise2Test {

    @Test
    void getDigitsIntoArray_singleDigit() {
        long number = 7;
        int[] expected = {7};

        int[] result = exercise2.getDigitsIntoArray(number);

        assertArrayEquals(expected, result);
    }

    @Test
    void getDigitsIntoArray_multipleDigits() {
        long number = 7735928640L;
        int[] expected = {7, 7, 3, 5, 9, 2, 8, 6, 4, 0};

        int[] result = exercise2.getDigitsIntoArray(number);

        assertArrayEquals(expected, result);
    }

}