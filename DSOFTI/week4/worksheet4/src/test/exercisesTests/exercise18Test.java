import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise18Test {

    @Test
    void mask() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'}  };
        char character = 'M';
        int[][] expected = { {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                             {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                             {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                             {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                             {0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
                             {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                             {0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                             {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                             {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                             {0, 0, 0, 0, 1, 0, 0, 1, 0, 0} };

        int[][] result = exercise18.mask(matrix, character);

        assertArrayEquals(expected, result);

    }

    @Test
    void isWordInMatrix_diagonalRightDown() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        String word = "PRESIDENT";

        boolean result = exercise18.isWordInMatrix(matrix, word);

        assertTrue(result);
    }

    @Test
    void isWordInMatrix_diagonalRightUp() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        String word = "TUNA";

        boolean result = exercise18.isWordInMatrix(matrix, word);

        assertTrue(result);
    }

    @Test
    void isWordInMatrix_right() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        String word = "OFFICE";

        boolean result = exercise18.isWordInMatrix(matrix, word);

        assertTrue(result);
    }

    @Test
    void isWordInMatrix_down() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        String word = "SPACE";

        boolean result = exercise18.isWordInMatrix(matrix, word);

        assertTrue(result);
    }

    @Test
    void isWordInMatrix_up() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        String word = "COMPUTER";

        boolean result = exercise18.isWordInMatrix(matrix, word);

        assertTrue(result);
    }

    @Test
    void isWordInMatrix_left() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        String word = "SHIRT";

        boolean result = exercise18.isWordInMatrix(matrix, word);

        assertTrue(result);
    }

    @Test
    void isWordInMatrix_diagonalLeftDown() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        String word = "RAT";

        boolean result = exercise18.isWordInMatrix(matrix, word);

        assertTrue(result);
    }

    @Test
    void isWordInMatrix_diagonalLeftUp() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        String word = "WATER";

        boolean result = exercise18.isWordInMatrix(matrix, word);

        assertTrue(result);
    }

    @Test
    void isWordInMatrix_false() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        String word = "GIRAFFE";

        boolean result = exercise18.isWordInMatrix(matrix, word);

        assertFalse(result);
    }

    @Test
    void isValidCell_firstElement() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        int row = 0;
        int column = 0;

        boolean result = exercise18.isValidCell(matrix, row, column);

        assertTrue(result);
    }

    @Test
    void isValidCell_lastElement() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        int row = 9;
        int column = 9;

        boolean result = exercise18.isValidCell(matrix, row, column);

        assertTrue(result);
    }

    @Test
    void isValidCell_false_rowLessThan0() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        int row = -1;
        int column = 0;

        boolean result = exercise18.isValidCell(matrix, row, column);

        assertFalse(result);
    }

    @Test
    void isValidCell_false_rowMoreThanMatrixLength() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        int row = 10;
        int column = 0;

        boolean result = exercise18.isValidCell(matrix, row, column);

        assertFalse(result);
    }

    @Test
    void isValidCell_false_columnLessThan0() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        int row = 0;
        int column = -1;

        boolean result = exercise18.isValidCell(matrix, row, column);

        assertFalse(result);
    }

    @Test
    void isValidCell_false_columnMoreThanMatrixLength() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        int row = 0;
        int column = 10;

        boolean result = exercise18.isValidCell(matrix, row, column);

        assertFalse(result);
    }

    @Test
    void isMatchingLetter_true() {

        char matrixLetter = 'S';
        char wordLetter = 'S';
        int i = 1;
        int j = 0;

        boolean result = exercise18.isMatchingLetter(matrixLetter, wordLetter, i, j);

        assertTrue(result);
    }

    @Test
    void isMatchingLetter_false() {

        char matrixLetter = 'S';
        char wordLetter = 'O';
        int i = 1;
        int j = 0;

        boolean result = exercise18.isMatchingLetter(matrixLetter, wordLetter, i, j);

        assertFalse(result);
    }

    @Test
    void findWordDirection_diagonalLeftUp() {

        int[] letter1Coord = {1, 1};
        int[] letter2Coord = {0, 0};
        String expected = "diagonal-left-up";

        String result = exercise18.findWordDirection(letter1Coord, letter2Coord);

        assertEquals(expected, result);
    }

    @Test
    void findWordDirection_up() {

        int[] letter1Coord = {1, 1};
        int[] letter2Coord = {0, 1};
        String expected = "up";

        String result = exercise18.findWordDirection(letter1Coord, letter2Coord);

        assertEquals(expected, result);
    }

    @Test
    void findWordDirection_diagonalRightUp() {

        int[] letter1Coord = {1, 1};
        int[] letter2Coord = {0, 2};
        String expected = "diagonal-right-up";

        String result = exercise18.findWordDirection(letter1Coord, letter2Coord);

        assertEquals(expected, result);
    }

    @Test
    void findWordDirection_left() {

        int[] letter1Coord = {1, 1};
        int[] letter2Coord = {1, 0};
        String expected = "left";

        String result = exercise18.findWordDirection(letter1Coord, letter2Coord);

        assertEquals(expected, result);
    }

    @Test
    void findWordDirection_right() {

        int[] letter1Coord = {1, 1};
        int[] letter2Coord = {1, 2};
        String expected = "right";

        String result = exercise18.findWordDirection(letter1Coord, letter2Coord);

        assertEquals(expected, result);
    }

    @Test
    void findWordDirection_diagonalLeftDown() {

        int[] letter1Coord = {1, 1};
        int[] letter2Coord = {2, 0};
        String expected = "diagonal-left-down";

        String result = exercise18.findWordDirection(letter1Coord, letter2Coord);

        assertEquals(expected, result);
    }

    @Test
    void findWordDirection_down() {

        int[] letter1Coord = {1, 1};
        int[] letter2Coord = {2, 1};
        String expected = "down";

        String result = exercise18.findWordDirection(letter1Coord, letter2Coord);

        assertEquals(expected, result);
    }

    @Test
    void findWordDirection_diagonalRightDown() {

        int[] letter1Coord = {1, 1};
        int[] letter2Coord = {2, 2};
        String expected = "diagonal-right-down";

        String result = exercise18.findWordDirection(letter1Coord, letter2Coord);

        assertEquals(expected, result);
    }

    @Test
    void getPathOfPotentialWord() {

        String word = "CAT";
        int[] letter1Coord = {0, 0};
        String direction = "diagonal-right-down";
        int[][] expected = {{0, 0}, {1, 1}, {2, 2}};

        int[][] result = exercise18.getPathOfPotentialWord(word, letter1Coord, direction);

        assertArrayEquals(expected, result);
    }

    @Test
    void isWordInMatrix_coordinates_true() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        int[] letter1Coord = {1, 0};
        int[] finalLetterCoord = {9, 8};
        String word = "PRESIDENT";

        boolean result = exercise18.isWordInMatrix(matrix, letter1Coord, finalLetterCoord, word);

        assertTrue(result);
    }

    @Test
    void isWordInMatrix_coordinates_false() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        int[] letter1Coord = {1, 0};
        int[] finalLetterCoord = {9, 8};
        String word = "GUITAR";

        boolean result = exercise18.isWordInMatrix(matrix, letter1Coord, finalLetterCoord, word);

        assertFalse(result);
    }

    @Test
    void Mask_word() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        int[][] path = {{2, 7}, {2, 6}, {2, 5}, {2, 4}, {2, 3}};
        int[][] expected = {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 1, 1, 1, 1, 1, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};

        int[][] result = exercise18.mask(matrix, path);

        assertArrayEquals(expected, result);
    }

    @Test
    void wordInMatrixCoord() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        String word = "CAT";
        int[][] expected = {{9, 3}, {8, 3}, {7, 3}};

        int[][] result = exercise18.wordInMatrixCoord(matrix, word);

        assertArrayEquals(expected, result);
    }

    @Test
    void lettersInCommon() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        String word1 = "PRESIDENT";
        String word2 = "OFFICE";

        boolean result = exercise18.lettersInCommon(matrix, word1, word2);

        assertTrue(result);
    }

    @Test
    void lettersInCommon_true() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        String word1 = "PRESIDENT";
        String word2 = "OFFICE";

        boolean result = exercise18.lettersInCommon(matrix, word1, word2);

        assertTrue(result);
    }

    @Test
    void lettersInCommon_false() {

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        String word1 = "PRESIDENT";
        String word2 = "CAT";

        boolean result = exercise18.lettersInCommon(matrix, word1, word2);

        assertFalse(result);
    }



}