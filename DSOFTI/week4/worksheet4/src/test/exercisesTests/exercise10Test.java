import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class exercise10Test {

    @Test
    void getLowestElement() {

        int[] array = {5, 10, 15, 3, 9, 0, 12};
        int expected = 0;

        int result = exercise10.getLowestElement(array);

        assertEquals(expected, result);
    }

    @Test
    void getHighestElement() {

        int[] array = {5, 10, 15, 3, 9, 0, 12};
        int expected = 15;

        int result = exercise10.getHighestElement(array);

        assertEquals(expected, result);
    }

    @Test
    void getAverage() {

        int[] array = {5, 10, 15, 3, 9, 0, 12};
        float expected = 7.7142F;

        float result = exercise10.getAverage(array);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void getProduct_with0() {

        int[] array = {5, 10, 15, 3, 9, 0, 12};
        int expected = 0;

        int result = exercise10.getProduct(array);

        assertEquals(expected, result);
    }

    @Test
    void getProduct_without0() {

        int[] array = {5, 10, 15, 3, 9, 1, 12};
        int expected = 243000;

        int result = exercise10.getProduct(array);

        assertEquals(expected, result);
    }

    @Test
    void repeatedElementsCount_noRepeats() {
        int[] array = {1, 2, 3, 4, 5};
        int expected = 0;

        int result = exercise10.repeatedElementsCount(array);

        assertEquals(expected, result);
    }

    @Test
    void repeatedElementsCount_consecutiveRepeats() {
        int[] array = {1, 2, 2, 2, 5};
        int expected = 2;

        int result = exercise10.repeatedElementsCount(array);

        assertEquals(expected, result);
    }

    @Test
    void repeatedElementsCount_allRepeats() {
        int[] array = {1, 1, 1, 1, 1};
        int expected = 5;

        int result = exercise10.repeatedElementsCount(array);

        assertEquals(expected, result);
    }

    @Test
    void repeatedElementsIndex_noRepeats() {
        int[] array = {1, 2, 3, 4, 5};
        int[] expected = {};

        int[] result = exercise10.repeatedElementsIndex(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void repeatedElementsIndex_consecutiveRepeats() {
        int[] array = {1, 2, 2, 3, 2};
        int[] expected = {2, 4};

        int[] result = exercise10.repeatedElementsIndex(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void repeatedElementsIndex_allRepeats() {
        int[] array = {1, 1, 1, 1, 1};
        int[] expected = {1, 2, 3, 4, 0};

        int[] result = exercise10.repeatedElementsIndex(array);

        assertArrayEquals(expected, result);
    }


    @Test
    void excludeRepeatedElementsFromArray_noRepeats() {
        int[] array = {1, 2, 3, 4, 5};
        int[] expected = {1, 2, 3, 4, 5};

        int[] result = exercise10.excludeRepeatedElementsFromArray(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void excludeRepeatedElementsFromArray_consecutiveRepeats() {
        int[] array = {1, 2, 2, 2, 5};
        int[] expected = {1, 2, 5};

        int[] result = exercise10.excludeRepeatedElementsFromArray(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void excludeRepeatedElementsFromArray_allRepeats() {
        int[] array = {1, 1, 1, 1, 1};
        int[] expected = {};

        int[] result = exercise10.excludeRepeatedElementsFromArray(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void invert_differentElements() {
        int[] array = {1, 2, 3, 4, 5};
        int[] expected = {5, 4, 3, 2, 1};

        int[] result = exercise10.invert(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void invert_consecutiveEqualElements() {
        int[] array = {1, 2, 2, 2, 5};
        int[] expected = {5, 2, 2, 2, 1};

        int[] result = exercise10.invert(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void invert_allEqualElements() {
        int[] array = {1, 1, 1, 1, 1};
        int[] expected = {1, 1, 1, 1, 1};

        int[] result = exercise10.invert(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void countPrimeNumbers_noPrimeNumbers() {

        int[] array = {1, 4, 6};
        int expected = 0;

        int result = exercise10.countPrimeNumbers(array);

        assertEquals(expected, result);
    }

    @Test
    void countPrimeNumbers_allPrimeNumbers() {

        int[] array = {2, 3, 5, 7};
        int expected = 4;

        int result = exercise10.countPrimeNumbers(array);

        assertEquals(expected, result);
    }

    @Test
    void getPrimeNumbers_noPrimeNumbers() {

        int[] array = {1, 4, 6};
        int[] expected = {};

        int[] result = exercise10.getPrimeNumbers(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void getPrimeNumbers_allPrimeNumbers() {

        int[] array = {2, 3, 5, 7};
        int[] expected = {2, 3, 5, 7};

        int[] result = exercise10.getPrimeNumbers(array);

        assertArrayEquals(expected, result);

    }

}