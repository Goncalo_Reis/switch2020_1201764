import java.util.Arrays;

public class exercise10 {

    public static void main(String[] args) {

        int[] array, noRepeatedElements, invertedArray, primeNumbers;
        int lowestElement, highestElement, product;
        float average;

        array = exercise3.readArrayFromUser();

        lowestElement = getLowestElement(array);
        highestElement = getHighestElement(array);
        average = getAverage(array);
        product = getProduct(array);
        noRepeatedElements = excludeRepeatedElementsFromArray(array);
        invertedArray = invert(array);
        primeNumbers = getPrimeNumbers(array);

        System.out.println("The lowest element is " + lowestElement + ".");
        System.out.println("The highest element is " + highestElement + ".");
        System.out.println("The average is " + average + ".");
        System.out.println("The product is " + product + ".");
        System.out.println("The set of elements without repetitions is " + Arrays.toString(noRepeatedElements) + ".");
        System.out.println("The set of elements inverted is " + Arrays.toString(invertedArray) + ".");
        System.out.println("The set of prime elements is " + Arrays.toString(primeNumbers) + ".");
    }

    /**
     * A method to return the lowest-value element of an array.
     * @param array An array of type integer.
     * @return The lowest-value element of the array.
     */
    public static int getLowestElement(int[] array) {

        int lowestElement = array[0];

        for (int i : array) {
            if (lowestElement > i) {
                lowestElement = i;
            }
        }

        return lowestElement;
    }

    /**
     * A method to return the highest-value element of an array.
     * @param array An array of type integer.
     * @return The highest-value element of the array.
     */
    public static int getHighestElement(int[] array) {

        int highestElement = array[0];

        for (int i : array) {
            if (highestElement < i) {
                highestElement = i;
            }
        }

        return highestElement;
    }

    /**
     * A method to return the average of the elements in an array.
     * @param array An array of type integer.
     * @return The average of the array elements.
     */
    public static float getAverage(int[] array) {

        float average;
        float sumArrayElements = 0;

        for (int i : array) {
            sumArrayElements += i;
        }
        average = sumArrayElements / array.length;

        return average;
    }

    /**
     * A method to return the product of all the elements in an array.
     * @param array An array of type integer.
     * @return The product of the elements of the array.
     */
    public static int getProduct(int[] array) {

        int product = array[0];

        for (int i = 1; i < array.length; i++) {
            product *= array[i];
        }

        return product;
    }

    /**
     * A method that returns the count of the repeated elements in an array.
     * @param array An array of type integer.
     * @return The count of the repeated elements.
     */
    public static int repeatedElementsCount(int[] array) {

        int repeatedElementsCount = 0;

        for (int i = 0; i < array.length - 1; i++) {
            int eventCount = 0;
            for (int j = 0; eventCount < 1 && j + i + 1 < array.length; j++) {
                if (array[i] == array[j + i + 1]) {
                    repeatedElementsCount++;
                    eventCount++;
                }
            }
        }
        if (repeatedElementsCount == array.length - 1) {
            repeatedElementsCount += 1;
        }

        return repeatedElementsCount;
    }

    /**
     * A method that return an array of the indexes of repeated elements in an array.
     * @param array An array of type integer.
     * @return An array with the indexes of the repeated elements.
     */
    public static int[] repeatedElementsIndex(int[] array) {

        int repeatedElementsCount = repeatedElementsCount(array);
        int[] repeatedElementsIndex = new int[repeatedElementsCount];

        int indexCount = 0;
        for (int i = 0; i < array.length - 1; i++) {
            int eventCount = 0;
            for (int j = 0; eventCount < 1 && j + i + 1 < array.length; j++) {
                if (array[i] == array[j + i + 1]) {
                    repeatedElementsIndex[indexCount] = j + i + 1;
                    indexCount++;
                    eventCount++;
                }
            }
        }
        if (repeatedElementsCount == array.length) {
            repeatedElementsIndex[indexCount] = 0;
        }

        return repeatedElementsIndex;
    }

    /**
     * A method that extracts the elements of an array without value repetitions.
     * @param array An array of type integer.
     * @return A copy of the original array without repeated elements.
     */
    public static int[] excludeRepeatedElementsFromArray(int[] array) {

        int repeatedElementsCount = repeatedElementsCount(array);
        int[] repeatedElementsIndex = repeatedElementsIndex(array);
        int[] noRepeatedElements = new int[array.length - repeatedElementsCount];

        int indexCount = 0;
        for (int i = 0; i < array.length; i++) {
            int eventCount = 0;
            for (int index : repeatedElementsIndex) {
                if (i != index) {
                    eventCount++;
                }
            }
            if (eventCount == repeatedElementsIndex.length) {
                noRepeatedElements[indexCount] = array[i];
                indexCount++;
            }
        }

        return noRepeatedElements;
    }

    /**
     * A method to invert the positions of the elements in an array.
     * @param array An array of type integer.
     * @return The array with the elements in inverted positions.
     */
    public static int[] invert(int[] array) {

        int[] invertedArray = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            invertedArray[i] = array[array.length - 1 - i];
        }

        return invertedArray;
    }

    /**
     * A method to count prime numbers in an array.
     * @param array An array of type Integer.
     * @return The count of the prime numbers in the array.
     */
    public static int countPrimeNumbers(int[] array) {
        final int LOWER_LIMIT = 2;
        int primeNumbersCount = 0;

        for (int element : array) {
            int combinationElementsNumber = element - 1;
            int combinations = 0;
            int z = 0;
            do {
                combinations += combinationElementsNumber - z;
                z++;
            } while (combinationElementsNumber - z > 0);
            int eventCount = 0;
            for (int i = LOWER_LIMIT; i <= element; i++) {
                for (int j = 0; j + i <= element; j++) {
                    if (i * (j + i) != element) {
                        eventCount++;
                    }
                }
            }
            if (combinations == eventCount && element != 1) {
                primeNumbersCount++;
            }
        }

        return primeNumbersCount;
    }

    /**
     * A method to extract the prime numbers in an array.
     * @param array An array of type Integer.
     * @return The array with only the prime numbers.
     */
    public static int[] getPrimeNumbers(int[] array) {

        final int LOWER_LIMIT = 2;
        int primeNumbersCount = countPrimeNumbers(array);
        int[] primeNumbers = new int[primeNumbersCount];

        int indexCount = 0;
        for (int element : array) {
            int combinationElementsNumber = element - 1;
            int combinations = 0;
            int z = 0;
            do {
                combinations += combinationElementsNumber - z;
                z++;
            } while (combinationElementsNumber - z > 0);
            int eventCount = 0;
            for (int i = LOWER_LIMIT; i <= element; i++) {
                for (int j = 0; j + i <= element; j++) {
                    if (i * (j + i) != element) {
                        eventCount++;
                    }
                }
            }
            if (combinations == eventCount && element != 1) {
                primeNumbers[indexCount] = element;
                indexCount++;
            }
        }
        return primeNumbers;
    }
}
