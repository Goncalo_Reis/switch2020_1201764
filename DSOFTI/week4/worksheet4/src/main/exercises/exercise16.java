import java.util.Scanner;

public class exercise16 {

    public static void main(String[] args) {

        int[][] matrix;
        int rows, columns, determinant;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter number of rows");
        rows = read.nextInt();
        System.out.println("Enter number of columns.");
        columns = read.nextInt();
        matrix = exercise12.readMatrix(rows, columns);

        determinant = exercise15.determinant(matrix);

        System.out.println("The determinant's value is " + determinant + ".");
    }


}
