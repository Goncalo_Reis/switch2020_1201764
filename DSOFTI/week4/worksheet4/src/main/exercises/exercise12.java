import java.util.Scanner;

public class exercise12 {

    public static void main(String[] args) {

        int[][] matrix;
        int lines, columns;
        int numberColumns;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter number of lines.");
        lines = read.nextInt();
        System.out.println("Enter number of columns.");
        columns = read.nextInt();
        matrix = readMatrix(lines, columns);

        numberColumns = countLineElements(matrix);

        if (numberColumns == -1) {
            System.out.println("The number of columns is NOT the same.");
        }
        else {
            System.out.println("The number of columns is " + numberColumns + ".");
        }
    }

    /**
     * An auxiliary method tho read a matrix entered in the terminal
     * @param lines Number of rows in the matrix.
     * @param columns Number of columns in the matrix.
     * @return The matrix entered by the user.
     */
    public static int[][] readMatrix(int lines, int columns) {

        int[][] matrix = new int[lines][columns];

        Scanner read = new Scanner(System.in);
        int lineCount = 0;
        while (lineCount < lines) {
            int columnCount = 0;
            while (columnCount < columns) {
                System.out.println("Enter element " + (columnCount + 1) + " of line " + (lineCount + 1) + ".");
                matrix[lineCount][columnCount] = read.nextInt();
                columnCount++;
            }
            lineCount++;
        }

        return matrix;
    }

    /**
     * A method to count the number of elements in each row of a matrix.
     * @param matrix A matrix of type Integer.
     * @return Returns the number of elements in each row. If those differ, returns -1.
     */
    public static int countLineElements(int[][] matrix) {

        int[] columnsCount = new int[matrix.length];
        int numberColumns = -1;

        int index = 0;
        for (int[] i : matrix) {
            columnsCount[index] = i.length;
            index++;
        }
        int eventCount = 0;
        for (int i = 0; i < columnsCount.length - 1; i++) {
            if (columnsCount[i] == columnsCount[i + 1]) {
                eventCount++;
            }
        }
        if (eventCount == columnsCount.length - 1) {
            numberColumns = columnsCount[0];
        }

        return numberColumns;
    }
}
