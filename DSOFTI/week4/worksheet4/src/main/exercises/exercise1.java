import java.util.Scanner;

public class exercise1 {


    public static void main(String[] args) {

        long number;
        int digitCount;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter a positive number.");
        number = read.nextLong();

        digitCount = countDigits(number);

        System.out.print(digitCount);
    }

    /**
     * A method to count the number of digits of a given integer.
     * @param number Number of type long.
     * @return Digit of given number count.
     */
    public static int countDigits(long number) {

        int digitCount = (int) Math.log10(number) + 1;
        return digitCount;
    }
}
