import java.util.Arrays;
import java.util.Scanner;

public class exercise2 {

    public static void main(String[] args) {

        long number;
        int[] digits;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter number.");
        number = read.nextLong();

        digits = exercise2.getDigitsIntoArray(number);

        System.out.println(Arrays.toString(digits));
    }

    /**
     * A method to transfer the individual digits of a given number into an array, 1 digit per element.
     * @param number A number of type long.
     * @return An array the digits of the number as its elements.
     */
    public static int[] getDigitsIntoArray(long number) {

        int digitCount = exercise1.countDigits(number);
        int[] digits = new int[digitCount];

        for (int i = 0; i < digitCount; i++) {
            digits[(digitCount - 1) - i] = (int) ( (number / Math.pow(10, i)) % 10 );
        }

        return digits;
    }
}
