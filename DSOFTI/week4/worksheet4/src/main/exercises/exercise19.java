import java.util.Scanner;

public class exercise19 {

    public static void main(String[] args) {

        final int[][] SECTION_LIMITS = {{0, 2, 0, 2},
                                        {0, 2, 3, 5},
                                        {0, 2, 6, 8},
                                        {3, 5, 0, 2},
                                        {3, 5, 3, 5},
                                        {3, 5, 6, 8},
                                        {6, 8, 0, 2},
                                        {6, 8, 3, 5},
                                        {6, 8, 6, 8}};
        int[][] sudoku = {  {0, 0, 0, 2, 6, 0, 7, 0, 1},
                            {6, 8, 0, 0, 7, 0, 0, 9, 0},
                            {1, 9, 0, 0, 0, 4, 5, 0, 0},
                            {8, 2, 0, 1, 0, 0, 0, 4, 0},
                            {0, 0, 4, 6, 0, 2, 9, 0, 0},
                            {0, 5, 0, 0, 0, 3, 0, 2, 8},
                            {0, 0, 9, 3, 0, 0, 0, 7, 4},
                            {0, 4, 0, 0, 5, 0, 0, 3, 6},
                            {7, 0, 3, 0, 1, 8, 0, 0, 0}  };
        final int NUMBER_POSITION = 2;
        int[] position = new int[NUMBER_POSITION];
        int[][] presetNumbersMask = filledSpacesMask(sudoku);
        newTurn(SECTION_LIMITS, sudoku, position, presetNumbersMask);
        printSudoku(sudoku);
        System.out.println("Congratulations! You finished the game!");
    }

    /**
     * A method to write a number to an empty space of the sudoku matrix.
     * @param sudoku A sudoku matrix.
     * @param filledSpacesMask A mask that signals the filled spaces of a matrix.
     * @param position An array that stores the positioning of the intended play.
     * @param number The number to write on an empty space.
     * @return The updated sudoku matrix.
     */
    public static int[][] play(int[][] sudoku, int[][] filledSpacesMask, int[] position, int number) {
        final int EMPTY_SPACE = 0;
        int[][] updatedSudoku = new int[sudoku.length][sudoku[0].length];
        for (int row = 0; row < sudoku.length; row++) {
            for (int column = 0; column < sudoku[0].length; column++) {
                updatedSudoku[row][column] = sudoku[row][column];
            }
        }
        if (filledSpacesMask[position[0]][position[1]] == EMPTY_SPACE) {
            updatedSudoku[position[0]][position[1]] = number;
        }
        return updatedSudoku;
    }

    /**
     * A method which creates a mask that signals the filled spaces of the sudoku matrix.
     * @param sudoku A sudoku matrix.
     * @return A mask of the filled spaces in the sudoku matrix.
     */
    public static int[][] filledSpacesMask(int[][] sudoku) {
        final int EMPTY_SPACE = 0;
        int[][] filledSpacesMask = new int[sudoku.length][sudoku[0].length];
        for (int row = 0; row < sudoku.length; row++) {
            for (int column = 0; column < sudoku[0].length; column++) {
                if (sudoku[row][column] == EMPTY_SPACE) {
                    filledSpacesMask[row][column] = 0;
                }
                else {
                    filledSpacesMask[row][column] = 1;
                }
            }
        }
        return filledSpacesMask;
    }

    /**
     * A method to erase a number in a filled space of the sudoku matrix.
     * @param sudoku A sudoku matrix.
     * @param playerActionsMask A mask of the numbers entered by the player in the sudoku matrix.
     * @param position An array that stores the positioning of the intended play.
     * @return The updated sudoku matrix.
     */
    public static int[][] erasePlay(int[][] sudoku, int[][] playerActionsMask, int[] position) {
        final int PLAYER_FILLED_SPACE = 1;
        final int EMPTY_SPACE = 0;
        int[][] updatedSudoku = new int[sudoku.length][sudoku[0].length];
        for (int row = 0; row < sudoku.length; row++) {
            for (int column = 0; column < sudoku[0].length; column++) {
                updatedSudoku[row][column] = sudoku[row][column];
            }
        }

        if (playerActionsMask[position[0]][position[1]] == PLAYER_FILLED_SPACE) {
            updatedSudoku[position[0]][position[1]] = EMPTY_SPACE;
        }
        return updatedSudoku;
    }

    /**
     * A method which creates a mask of the numbers entered by the player in the sudoku matrix.
     * @param sudoku A sudoku Matrix.
     * @param presetNumbersMask A mask of the initial numbers present in the sudoku matrix.
     * @return A mask of the spaces of the sudoku matrix filled by the player.
     */
    public static int[][] playerActionsMask (int[][] sudoku, int[][] presetNumbersMask) {
        int[][] playerActionsMask = new int[sudoku.length][sudoku[0].length];
        int[][] existentNumbersMask = filledSpacesMask(sudoku);
        for (int row = 0; row < presetNumbersMask.length; row++) {
            for (int column = 0; column < presetNumbersMask[0].length; column++) {
                if (presetNumbersMask[row][column] == existentNumbersMask[row][column]) {
                    playerActionsMask[row][column] = 0;
                }
                else {
                    playerActionsMask[row][column] = 1;
                }
            }
        }
        return playerActionsMask;
    }

    /**
     * A method that checks whether a play is valid.
     * @param sudoku A sudoku matrix.
     * @param SECTION_LIMITS A matrix of the limits of the 9 sections of the sudoku matrix.
     * @param position An array that stores the positioning of the intended play.
     * @param number The number to write on an empty space.
     * @return Returns true if the play doesn't break any rules.
     */
    public static boolean validPlay(int[][] sudoku, int[][] SECTION_LIMITS, int[] position, int number) {
        boolean isValidPlay = false;
        int sectionMatched = getSectionNumber(SECTION_LIMITS, position);
        int[][] section = isolateSectionContents(sudoku, SECTION_LIMITS, sectionMatched);
        if(rowRule(sudoku, position, number) && columnRule(sudoku, position, number) && sectionRule(section, number)) {
            isValidPlay = true;
        }
        return isValidPlay;
    }

    /**
     * A method that checks if the row rule is followed.
     * @param sudoku A sudoku matrix.
     * @param position An array that stores the positioning of the intended play.
     * @param number The number to write on an empty space.
     * @return Returns true if the play doesn't break the row rule.
     */
    public static boolean rowRule(int[][] sudoku, int[] position, int number) {
        boolean rowRule = true;
        int[] positionRow = sudoku[position[0]];
        if (numberInArray(positionRow, number)) {
            rowRule = false;
        }
        return rowRule;
    }

    /**
     * A method that checks if the column rule is followed.
     * @param sudoku A sudoku matrix.
     * @param position An array that stores the positioning of the intended play.
     * @param number The number to write on an empty space.
     * @return Returns true if the play doesn't break the column rule.
     */
    public static boolean columnRule(int[][] sudoku, int[] position, int number) {
        boolean columnRule = true;
        int[] positionColumn = new int[sudoku[0].length];
        int positionColumnIndex = 0;
        for (int[] row : sudoku) {
            positionColumn[positionColumnIndex] = row[position[1]];
            positionColumnIndex++;
        }
        if (numberInArray(positionColumn, number)) {
            columnRule = false;
        }
        return columnRule;
    }

    /**
     * A method that checks whether a number is present in an array.
     * @param array An array of type int.
     * @param number A number of type int.
     * @return Returns true if the number is found in the array.
     */
    public static boolean numberInArray(int[] array, int number) {
        boolean numberFound = false;
        for (int element = 0; element < array.length && !numberFound; element++) {
            if (array[element] == number) {
                numberFound = true;
            }
        }
        return numberFound;
    }

    /**
     * A method that checks if the section rule is followed.
     * @param section A section of the sudoku matrix.
     * @param number The number to write on an empty space.
     * @return Returns true if the play doesn't break the section rule.
     */
    public static boolean sectionRule(int[][] section, int number) {
        boolean sectionRule = true;
        for (int[] row : section) {
            for (int element : row) {
                if (element == number) {
                    sectionRule = false;
                }
            }
        }
        return  sectionRule;
    }

    /**
     * A method to copy a section of the sudoku matrix to an independent matrix.
     * @param sudoku A sudoku matrix.
     * @param SECTION_LIMITS A matrix of the limits of the 9 sections of the sudoku matrix.
     * @param sectionMatched The number of the section to copy.
     * @return A matrix with only the intended section.
     */
    public static int[][] isolateSectionContents(int[][] sudoku, int[][] SECTION_LIMITS, int sectionMatched) {
        int[] upDownLeftRightLimits = SECTION_LIMITS[sectionMatched - 1];
        final int SECTION_HEIGHT = 3;
        final int SECTION_WIDTH = 3;
        int[][] section = new int[SECTION_HEIGHT][SECTION_WIDTH];
        int rowSection = -1;
        for (int row = upDownLeftRightLimits[0]; row <= upDownLeftRightLimits[1]; row++) {
            int columnSection = 0;
            rowSection++;
            for (int column = upDownLeftRightLimits[2]; column <= upDownLeftRightLimits[3]; column++) {
                section[rowSection][columnSection] = sudoku[row][column];
                columnSection++;
            }
        }
        return section;
    }

    /**
     * A method to find to which section belongs a certain position.
     * @param SECTION_LIMITS A matrix of the limits of the 9 sections of the sudoku matrix.
     * @param position An array that stores the positioning of the intended play.
     * @return The number of the section.
     */
    public static int getSectionNumber(int[][] SECTION_LIMITS, int[] position) {
        final int SECTION_COUNT = 9;
        int sectionMatched = -1;
        for (int section = 0; section < SECTION_COUNT && sectionMatched == -1; section++) {
            int[] upDownLeftRightLimits = SECTION_LIMITS[section];
            if (withinSectionLimits(upDownLeftRightLimits, position)) {
                sectionMatched = section + 1;
            }
        }
        return sectionMatched;
    }

    /**
     * A method that checks if a specific position is inside a given set of section limits.
     * @param UpDownLeftRightLimits An array of the limits of one of the sections of the sudoku matrix.
     * @param position An array that stores the positioning of the intended play.
     * @return Returns true if the position is within the section limits.
     */
    public static boolean withinSectionLimits(int[] UpDownLeftRightLimits, int[] position) {
        boolean withinLimits = false;
        for (int row = UpDownLeftRightLimits[0]; row <= UpDownLeftRightLimits[1]; row++) {
            for (int column = UpDownLeftRightLimits[2]; column <= UpDownLeftRightLimits[3]; column++) {
                if (row == position[0] && column == position[1]) {
                    withinLimits = true;
                }
            }
        }
        return  withinLimits;
    }

    /**
     * A method to check whether the sudoku matrix is fully completed.
     * @param sudoku A sudoku matrix.
     * @return Returns true if the sudoku matrix is completed.
     */
    public static boolean finishedGame(int[][] sudoku) {
        final int EMPTY_SPACE = 1;
        boolean isFinished = true;
        int[][] emptySpacesMask = emptySpacesMask(sudoku);
        for (int row = 0; row < emptySpacesMask.length && isFinished; row++) {
            for (int column = 0; column < emptySpacesMask[0].length && isFinished; column++) {
                if (emptySpacesMask[row][column] == EMPTY_SPACE) {
                    isFinished = false;
                }
            }
        }
        return isFinished;
    }

    /**
     * A method which creates a mask that signals the empty spaces of the sudoku matrix.
     * @param sudoku A sudoku matrix.
     * @return A mask of the empty spaces of the sudoku matrix.
     */
    public static int[][] emptySpacesMask(int[][] sudoku) {
        final int EMPTY_SPACE = 0;
        int[][] emptySpacesMask = new int[sudoku.length][sudoku[0].length];
        for (int row = 0; row < sudoku.length; row++) {
            for (int column = 0; column < sudoku[0].length; column++) {
                if (sudoku[row][column] != EMPTY_SPACE) {
                    emptySpacesMask[row][column] = 0;
                }
                else {
                    emptySpacesMask[row][column] = 1;
                }
            }
        }
        return emptySpacesMask;
    }

    /**
     * A method that takes care of the necessary proceedings each turn.
     * @param SECTION_LIMITS A matrix of the limits of the 9 sections of the sudoku matrix.
     * @param sudoku A sudoku matrix.
     * @param position An array that stores the positioning of the intended play.
     * @param presetNumbersMask A mask of the initial numbers present in the sudoku matrix.
     */
    public static void newTurn(int[][] SECTION_LIMITS, int[][] sudoku, int[] position, int[][] presetNumbersMask) {
        Scanner read = new Scanner(System.in);
        while (!finishedGame(sudoku)) {
            printSudoku(sudoku);
            System.out.println("\nEnter 1 to play and 2 to erase.");
            int playOrErase = read.nextInt();
            System.out.println("Enter the index of a row.");
            position[0] = read.nextInt();
            System.out.println("Enter the index of a column.");
            position[1] = read.nextInt();
            sudoku = playOrErase(SECTION_LIMITS, sudoku, position, presetNumbersMask, playOrErase);
        }
    }

    /**
     * A method to execute the necessary proceedings in case the player chooses to play or erase a previous play.
     * @param SECTION_LIMITS A matrix of the limits of the 9 sections of the sudoku matrix.
     * @param sudoku A sudoku matrix.
     * @param position An array that stores the positioning of the intended play.
     * @param presetNumbersMask A mask of the initial numbers present in the sudoku matrix.
     * @param playOrErase An int signaling the player wants to play (1) or erase a previous play (2).
     * @return An updated Sudoku matrix.
     */
    public static int[][] playOrErase(int[][] SECTION_LIMITS, int[][] sudoku, int[] position, int[][] presetNumbersMask, int playOrErase) {
        Scanner read = new Scanner(System.in);
        if (playOrErase == 1) {
            int[][] filledSpacesMask = filledSpacesMask(sudoku);
            System.out.println("Enter a number.");
            int number = read.nextInt();
            if (validPlay(sudoku, SECTION_LIMITS, position, number)) {
                sudoku = play(sudoku, filledSpacesMask, position, number);
            }
        }
        else {
            int[][] playerActionsMask = playerActionsMask(sudoku, presetNumbersMask);
            sudoku = erasePlay(sudoku, playerActionsMask, position);
        }
        return sudoku;
    }

    /**
     * A method to print the sudoku matrix.
     * @param sudoku A sudoku matrix.
     */
    public static void printSudoku(int[][] sudoku) {
        for (int[] row : sudoku) {
            System.out.print("\n-------------------\n|");
            for (int element : row) {
                if (element == 0) {
                    System.out.print(" |");
                }
                else {
                    System.out.print(element + "|");
                }
            }
        }
        System.out.print("\n-------------------\n");
    }

    /**
     * A method which creates a mask that signals a chosen number in the sudoku matrix.
     * @param sudoku A sudoku matrix.
     * @param number A number to signal for.
     * @return A mask of the given number of the sudoku matrix.
     */
    public static int[][] specificNumberMask(int[][] sudoku, int number) {
        int[][] specificNumberMask = new int[sudoku.length][sudoku[0].length];
        for (int row = 0; row < sudoku.length; row++) {
            for (int column = 0; column < sudoku[0].length; column++) {
                if (sudoku[row][column] == number) {
                    specificNumberMask[row][column] = 1;
                }
                else {
                    specificNumberMask[row][column] = 0;
                }
            }
        }
        return specificNumberMask;
    }
}
