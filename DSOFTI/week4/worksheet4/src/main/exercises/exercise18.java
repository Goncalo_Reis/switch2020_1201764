public class exercise18 {

    public static void main(String[] args) {

        boolean isWordInMatrix, lettersInCommon;

        char[][] matrix = { {'A', 'R', 'B', 'G', 'D', 'F', 'R', 'U', 'G', 'Q'},
                            {'P', 'S', 'E', 'O', 'U', 'Z', 'U', 'L', 'V', 'R'},
                            {'X', 'R', 'F', 'T', 'R', 'I', 'H', 'S', 'P', 'E'},
                            {'J', 'Z', 'E', 'B', 'A', 'V', 'T', 'P', 'C', 'T'},
                            {'R', 'T', 'I', 'S', 'O', 'W', 'N', 'A', 'M', 'U'},
                            {'V', 'O', 'F', 'F', 'I', 'C', 'E', 'C', 'R', 'P'},
                            {'T', 'D', 'A', 'A', 'P', 'D', 'O', 'E', 'R', 'M'},
                            {'J', 'L', 'N', 'T', 'X', 'A', 'E', 'R', 'C', 'O'},
                            {'E', 'U', 'Q', 'A', 'C', 'B', 'A', 'N', 'H', 'C'},
                            {'T', 'V', 'E', 'C', 'M', 'T', 'F', 'M', 'T', 'Z'} };
        String word = "OFFICE";
        String word2 = "PRESIDENT";

        isWordInMatrix = isWordInMatrix(matrix, word);
        lettersInCommon = lettersInCommon(matrix, word, word2);

        System.out.println("Presence of " + word + ": " + isWordInMatrix + ".");
        System.out.println("Words overlap: " + lettersInCommon + ".");
    }

    public static int[][] mask(char[][] matrix, char character) {

        int[][] mask = new int[matrix.length][matrix[0].length];

        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[0].length; column++) {
                if (matrix[row][column] == character) {
                    mask[row][column] = 1;
                } else {
                    mask[row][column] = 0;
                }
            }
        }
        return mask;
    }

    public static int[][] mask(char[][]matrix, int[][] pathWord) {

        int[][] mask = new int[matrix.length][matrix[0].length];

        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[0].length; column++) {
                mask[row][column] = 0;
            }
        }

        for (int[] letterIndex : pathWord) {
            mask[letterIndex[0]][letterIndex[1]] = 1;
        }
        return mask;
    }

    public static boolean lettersInCommon(char[][] matrix, String word1, String word2) {

        boolean lettersInCommon = false;
        int[][] pathWord1 = wordInMatrixCoord(matrix, word1);
        int[][] pathWord2 = wordInMatrixCoord(matrix, word2);
        int[][] mask1 = mask(matrix, pathWord1);
        int[][] mask2 = mask(matrix, pathWord2);
        int[][] sumMask = exercise17.sum(mask1, mask2);

        for (int[] row : sumMask) {
            for (int element : row) {
                if (element == 2) {
                    lettersInCommon = true;
                }
            }
        }
        return lettersInCommon;
    }
    public static boolean isWordInMatrix(char[][] matrix, String word) {

        boolean isWordInMatrix = false;
        final int LETTER_INDEX = 2;
        int[] letter1Coord = new int[LETTER_INDEX];
        int[] letter2Coord = new int[LETTER_INDEX];
        int[][] path;

        int[][] mask = mask(matrix, word.charAt(0));
        for (int row = 0; row < mask.length && !isWordInMatrix; row++) {
            for (int column = 0; column < mask[0].length && !isWordInMatrix; column++) {
                if (mask[row][column] == 1) {
                    letter1Coord[0] = row;
                    letter1Coord[1] = column;
                    for (int i = -1; i < 2 && !isWordInMatrix; i++) {
                        for (int j = -1; j < 2 && !isWordInMatrix; j++) {
                            if (isValidCell(matrix, letter1Coord[0] + i, letter1Coord[1] + j)) {
                                if (isMatchingLetter(matrix[letter1Coord[0] + i][letter1Coord[1] + j], word.charAt(1), i, j)) {
                                    letter2Coord[0] = letter1Coord[0] + i;
                                    letter2Coord[1] = letter1Coord[1] + j;
                                    String direction = findWordDirection(letter1Coord, letter2Coord);
                                    path = getPathOfPotentialWord(word, letter1Coord, direction);
                                    int letter = 0;
                                    int eventCount = 0;
                                    for (int[] letterCoord : path) {
                                        if (isValidCell(matrix, letterCoord[0], letterCoord[1])) {
                                            if (matrix[letterCoord[0]][letterCoord[1]] == word.charAt(letter)) {
                                                eventCount++;
                                                letter++;
                                            }
                                        }
                                    }
                                    if (word.length() == eventCount) {
                                        isWordInMatrix = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return isWordInMatrix;
    }

    public static int[][] wordInMatrixCoord(char[][] matrix, String word) {

        final int LETTER_INDEX = 2;
        int[] letter1Coord = new int[LETTER_INDEX];
        int[] letter2Coord = new int[LETTER_INDEX];
        int[][] path;

        int[][] mask = mask(matrix, word.charAt(0));
        for (int row = 0; row < mask.length; row++) {
            for (int column = 0; column < mask[0].length; column++) {
                if (mask[row][column] == 1) {
                    letter1Coord[0] = row;
                    letter1Coord[1] = column;
                    for (int i = -1; i < 2 ; i++) {
                        for (int j = -1; j < 2; j++) {
                            if (isValidCell(matrix, letter1Coord[0] + i, letter1Coord[1] + j)) {
                                if (isMatchingLetter(matrix[letter1Coord[0] + i][letter1Coord[1] + j], word.charAt(1), i, j)) {
                                    letter2Coord[0] = letter1Coord[0] + i;
                                    letter2Coord[1] = letter1Coord[1] + j;
                                    String direction = findWordDirection(letter1Coord, letter2Coord);
                                    path = getPathOfPotentialWord(word, letter1Coord, direction);
                                    int letter = 0;
                                    int eventCount = 0;
                                    for (int[] letterCoord : path) {
                                        if (isValidCell(matrix, letterCoord[0], letterCoord[1])) {
                                            if (matrix[letterCoord[0]][letterCoord[1]] == word.charAt(letter)) {
                                                eventCount++;
                                                letter++;
                                            }
                                        }
                                    }
                                    if (word.length() == eventCount) {
                                        return path;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }


    public static boolean isWordInMatrix(char[][] matrix, int[] letter1Coord, int[] finalLetterCoord, String word) {

        boolean isWordInMatrix = false;
        String direction = findWordDirection(letter1Coord, finalLetterCoord);
        int[][] path = getPathOfPotentialWord(word, letter1Coord, direction);

        int letter = 0;
        int eventCount = 0;
        for (int[] letterCoord : path) {
            if (isValidCell(matrix, letterCoord[0], letterCoord[1])) {
                if (matrix[letterCoord[0]][letterCoord[1]] == word.charAt(letter)) {
                    eventCount++;
                    letter++;
                }
            }
        }
        if (word.length() == eventCount) {
            isWordInMatrix = true;
        }
        return  isWordInMatrix;
    }

    public static boolean isValidCell(char[][] matrix, int row, int column) {

        return row >= 0 && row < matrix.length && column >= 0 && column < matrix[0].length;
    }

    public static boolean isMatchingLetter(char matrixLetter, char wordLetter, int i, int j) {

        return matrixLetter == wordLetter && (i != 0 || j != 0);
    }

    public static String findWordDirection(int[] letter1Coordinates, int[] letter2Coordinates) {

        String wordDirection = "";
        if (isDirectionDiagonalLeftUp(letter1Coordinates, letter2Coordinates)) {
            wordDirection = "diagonal-left-up";
        } else if (isDirectionUp(letter1Coordinates, letter2Coordinates)) {
            wordDirection = "up";
        } else if (isDirectionDiagonalRightUp(letter1Coordinates, letter2Coordinates)) {
            wordDirection = "diagonal-right-up";
        } else if (isDirectionLeft(letter1Coordinates, letter2Coordinates)) {
            wordDirection = "left";
        } else if (isDirectionRight(letter1Coordinates, letter2Coordinates)) {
            wordDirection = "right";
        } else if (isDirectionDiagonalLeftDown(letter1Coordinates, letter2Coordinates)) {
            wordDirection = "diagonal-left-down";
        } else if (isDirectionDown(letter1Coordinates, letter2Coordinates)) {
            wordDirection = "down";
        } else if (isDirectionDiagonalRightDown(letter1Coordinates, letter2Coordinates)) {
            wordDirection = "diagonal-right-down";
        }
        return wordDirection;
    }

    public static boolean isDirectionDiagonalLeftUp(int[] letter1Coordinates, int[] letter2Coordinates) {

        boolean isDirectionDiagonalLeftUp = false;

        if (letter1Coordinates[0] > letter2Coordinates[0] && letter1Coordinates[1] > letter2Coordinates[1]) {
            isDirectionDiagonalLeftUp = true;
        }
        return isDirectionDiagonalLeftUp;
    }

    public static boolean isDirectionUp(int[] letter1Coordinates, int[] letter2Coordinates) {

        boolean isDirectionUp = false;

        if (letter1Coordinates[0] > letter2Coordinates[0] && letter1Coordinates[1] == letter2Coordinates[1]) {
            isDirectionUp = true;
        }
        return isDirectionUp;
    }

    public static boolean isDirectionDiagonalRightUp(int[] letter1Coordinates, int[] letter2Coordinates) {

        boolean isDirectionDiagonalRightUp = false;

        if (letter1Coordinates[0] > letter2Coordinates[0] && letter1Coordinates[1] < letter2Coordinates[1]) {
            isDirectionDiagonalRightUp = true;
        }
        return isDirectionDiagonalRightUp;
    }

    public static boolean isDirectionLeft(int[] letter1Coordinates, int[] letter2Coordinates) {

        boolean isDirectionLeft = false;

        if (letter1Coordinates[0] == letter2Coordinates[0] && letter1Coordinates[1] > letter2Coordinates[1]) {
            isDirectionLeft = true;
        }
        return isDirectionLeft;
    }

    public static boolean isDirectionRight(int[] letter1Coordinates, int[] letter2Coordinates) {

        boolean isDirectionRight = false;

        if (letter1Coordinates[0] == letter2Coordinates[0] && letter1Coordinates[1] < letter2Coordinates[1]) {
            isDirectionRight = true;
        }
        return isDirectionRight;
    }

    public static boolean isDirectionDiagonalLeftDown(int[] letter1Coordinates, int[] letter2Coordinates) {

        boolean isDirectionDiagonalLeftDown = false;

        if (letter1Coordinates[0] < letter2Coordinates[0] && letter1Coordinates[1] > letter2Coordinates[1]) {
            isDirectionDiagonalLeftDown = true;
        }
        return isDirectionDiagonalLeftDown;
    }

    public static boolean isDirectionDown(int[] letter1Coordinates, int[] letter2Coordinates) {

        boolean isDirectionDown = false;

        if (letter1Coordinates[0] < letter2Coordinates[0] && letter1Coordinates[1] == letter2Coordinates[1]) {
            isDirectionDown = true;
        }
        return isDirectionDown;
    }

    public static boolean isDirectionDiagonalRightDown(int[] letter1Coordinates, int[] letter2Coordinates) {

        boolean isDirectionDiagonalRightDown = false;

        if (letter1Coordinates[0] < letter2Coordinates[0] && letter1Coordinates[1] < letter2Coordinates[1]) {
            isDirectionDiagonalRightDown = true;
        }
        return isDirectionDiagonalRightDown;
    }


    public static int[][] getPathOfPotentialWord(String word, int[] letter1Coordinates, String direction) {

        int[][] path = new int[word.length()][2];
        path[0] = letter1Coordinates;

        if (direction.equals("diagonal-left-up")) {
            int index = 1;
            for (int row = letter1Coordinates[0] - 1, column = letter1Coordinates[1] - 1; index < word.length(); row--, column--) {
                path[index][0] = row;
                path[index][1] = column;
                index++;
            }
        } else if (direction.equals("up")) {
            int index = 1;
            for (int row = letter1Coordinates[0] - 1; index < word.length(); row--) {
                path[index][0] = row;
                path[index][1] = letter1Coordinates[1];
                index++;
            }
        } else if (direction.equals("diagonal-right-up")) {
            int index = 1;
            for (int row = letter1Coordinates[0] - 1, column = letter1Coordinates[1] + 1; index < word.length(); row--, column++) {
                path[index][0] = row;
                path[index][1] = column;
                index++;
            }
        } else if (direction.equals("left")) {
            int index = 1;
            for (int column = letter1Coordinates[1] - 1; index < word.length(); column--) {
                path[index][0] = letter1Coordinates[0];
                path[index][1] = column;
                index++;
            }
        } else if (direction.equals("right")) {
            int index = 1;
            for (int column = letter1Coordinates[1] + 1; index < word.length(); column++) {
                path[index][0] = letter1Coordinates[0];
                path[index][1] = column;
                index++;
            }
        } else if (direction.equals("diagonal-left-down")) {
            int index = 1;
            for (int row = letter1Coordinates[0] + 1, column = letter1Coordinates[1] - 1; index < word.length(); row++, column--) {
                path[index][0] = row;
                path[index][1] = column;
                index++;
            }
        } else if (direction.equals("down")) {
            int index = 1;
            for (int row = letter1Coordinates[0] + 1; index < word.length(); row++) {
                path[index][0] = row;
                path[index][1] = letter1Coordinates[1];
                index++;
            }
        } else if (direction.equals("diagonal-right-down")) {
            int index = 1;
            for (int row = letter1Coordinates[0] + 1, column = letter1Coordinates[1] + 1; index < word.length(); row++, column++) {
                path[index][0] = row;
                path[index][1] = column;
                index++;
            }
        }
        return path;
    }
}



















