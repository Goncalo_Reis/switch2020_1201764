import java.util.Scanner;

public class exercise14 {

    public static void main(String[] args) {

        int lines, columns;
        int[][] matrix;
        boolean isRectangular;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter number of lines.");
        lines = read.nextInt();
        System.out.println("Enter number of columns.");
        columns = read.nextInt();
        matrix = exercise12.readMatrix(lines, columns);

        isRectangular = isMatrixRectangular(matrix);

        if (isRectangular) {
            System.out.println("The matrix is a rectangle.");
        }
        else {
            System.out.println("The matrix is NOT a rectangle.");
        }
    }

    /**
     * Function to verify if a matrix forms a rectangular shape.
     * @param matrix Matrix of type integer.
     * @return Returns true if the matrix forms a rectangle.
     */
    public static boolean isMatrixRectangular(int[][] matrix) {

        boolean isRectangular = false;
        int countLineElements;

        countLineElements = exercise12.countLineElements(matrix);
        if (countLineElements != matrix.length && countLineElements != -1) {
            isRectangular = true;
        }

        return isRectangular;
    }

}
