import java.util.Arrays;

public class exercise4 {

    public static void main(String[] args) {

        int[] array;
        int[] evenArray;
        int[] oddArray;

        array = exercise3.readArrayFromUser();
        evenArray = getEvenArray(array);
        oddArray = getOddArray(array);

        System.out.println("Even numbers: " + Arrays.toString(evenArray) + "\nOdd numbers: " + Arrays.toString(oddArray));
    }

    /**
     * A method to count number of even elements present in a given array.
     * @param array An array of type int.
     * @return The count of even elements.
     */
    public static int countEvenElements(int[] array) {

        int evenElementsCount = 0;

        for (int i : array) {
            if (i % 2 == 0) {
                evenElementsCount ++;
            }
        }

        return evenElementsCount;
    }

    /**
     * A method to extract the even elements of an array.
     * @param array An array of type integer.
     * @return An array with only the even elements.
     */
    public static int[] getEvenArray(int[] array) {

        int evenElementsCount = exercise4.countEvenElements(array);
        int[] evenArray = new int[evenElementsCount];

        int evenArrayIndex = 0;
        for (int i : array) {
            if (i % 2 == 0) {
                evenArray[evenArrayIndex] = i;
                evenArrayIndex ++;
            }
        }

        return evenArray;
    }

    /**
     * A method to count number of odd elements present in a given array.
     * @param array An array of type integer.
     * @return The count of odd elements.
     */
    public static int countOddElements(int[] array) {

        int oddElementsCount = 0;

        for (int i : array) {
            if (i % 2 != 0) {
                oddElementsCount ++;
            }
        }

        return oddElementsCount;
    }

    /**
     * A method to extract the odd elements of an array.
     * @param array An array of type integer.
     * @return An array with only the odd elements.
     */
    public static int[] getOddArray(int[] array) {

        int oddElementsCount = exercise4.countOddElements(array);
        int[] oddArray = new int[oddElementsCount];

        int oddArrayIndex = 0;
        for (int i : array) {
            if (i % 2 != 0) {
                oddArray[oddArrayIndex] = i;
                oddArrayIndex ++;
            }
        }

        return oddArray;
    }
}
