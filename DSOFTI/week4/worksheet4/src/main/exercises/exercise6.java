import java.util.Arrays;
import java.util.Scanner;

public class exercise6 {

    public static void main(String[] args) {

        int[] array;
        int[] truncatedArray;
        int elementsCount;

        array = exercise3.readArrayFromUser();
        Scanner read = new Scanner(System.in);
        System.out.println("Enter number of elements to truncate.");
        elementsCount = read.nextInt();

        truncatedArray = getTruncatedArray(array, elementsCount);

        System.out.println(Arrays.toString(truncatedArray));
    }

    /**
     * A method that, given an array and number of elements it should hold, truncates it to the desired size.
     * @param array An array of type integer.
     * @param elementsCount The number of elements desired in the array.
     * @return The array truncated to the desired size.
     */
    public static int[] getTruncatedArray(int[] array, int elementsCount) {

        int[] truncatedArray = new int[elementsCount];

        for (int i = 0; i < elementsCount; i++) {
            truncatedArray[i] = array[i];
        }

        return truncatedArray;
    }

}
