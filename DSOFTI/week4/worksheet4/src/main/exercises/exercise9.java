import java.util.Scanner;

public class exercise9 {

    public static void main(String[] args) {

        int number;
        boolean isPalindrome;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter number to check if it is palindrome.");
        number = read.nextInt();

        isPalindrome = isNumberPalindrome(number);

        if (isPalindrome) {
            System.out.println("The number is a palindrome.");
        }
        else {
            System.out.println("The number is NOT a palindrome.");
        }
    }

    /**
     * A method to verify if a certain number corresponds to a palindrome.
     * @param number A number of type integer.
     * @return Returns true if the number is a palindrome.
     */
    public static boolean isNumberPalindrome(int number) {

        int[] digits = exercise2.getDigitsIntoArray(number);
        boolean isPalindrome = true;

        int equalDigitsCount = 0;
        for (int i = 0; i < digits.length / 2; i++) {
            if (digits[i] == digits[(digits.length - 1) - i]) {
                equalDigitsCount ++;
            }
        }
        if (equalDigitsCount != (digits.length / 2)) {
            isPalindrome = false;
        }

        return isPalindrome;
    }
}
