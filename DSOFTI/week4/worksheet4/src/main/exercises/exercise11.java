public class exercise11 {

    public static void main(String[] args) {
        int[] array1, array2;
        int scalarProduct;

        array1 = exercise3.readArrayFromUser();
        array2 = exercise3.readArrayFromUser();

        scalarProduct = getScalarProduct(array1, array2);

        System.out.println("The scalar product is " + scalarProduct + ".");
    }

    /**
     * A method that return the scalar product of two arrays.
     * @param array1 An array of type Integer.
     * @param array2 An array of type Integer.
     * @return The scalar product of the two arrays.
     */
    public static int getScalarProduct(int[] array1, int[] array2) {

        int[] products = new int[array1.length];
        int scalarProduct;

        for (int i = 0; i < array1.length; i++) {
            products[i] = array1[i] * array2[i];
        }
        scalarProduct = exercise3.sumArrayElements(products);

        return scalarProduct;
    }
}
