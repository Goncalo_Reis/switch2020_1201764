import java.util.Arrays;
import java.util.Scanner;

public class exercise17 {

    public static void main(String[] args) {

        int[][] matrix, matrixToSum, matrixToMultiply, productMatrixConstant, sumMatrix, productMatrices;
        int constant, rows, columns;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter constant.");
        constant = read.nextInt();

        System.out.println("Enter number of rows for matrix 1.");
        rows = read.nextInt();
        System.out.println("Enter number of columns for matrix 1.");
        columns = read.nextInt();
        matrix = exercise12.readMatrix(rows, columns);
        System.out.println("Now enter the values for the second matrix, to sum to the first one.");
        matrixToSum = exercise12.readMatrix(rows, columns);
        System.out.println("Enter number of columns for matrix to multiply.");
        rows = read.nextInt();
        matrixToMultiply = exercise12.readMatrix(columns, rows);


        productMatrixConstant = product(matrix, constant);
        sumMatrix = sum(matrix, matrixToSum);
        productMatrices = product(matrix, matrixToMultiply);


        System.out.println("constant x matrix:");
        for (int[] i : productMatrixConstant) {
            System.out.println(Arrays.toString(i));
        }
        System.out.println("matrix + matrix:");
        for (int[] i : sumMatrix) {
            System.out.println(Arrays.toString(i));
        }
        System.out.println("matrix x matrix:");
        for (int[] i : productMatrices) {
            System.out.println(Arrays.toString(i));
        }

    }

    public static int[][] product(int[][] matrix, int constant) {

        int[][] productMatrix = new int[matrix.length][matrix[0].length];

        for(int i = 0; i < productMatrix.length; i++) {
            for(int j = 0; j < productMatrix[0].length; j++) {
                productMatrix[i][j] = constant * matrix[i][j];
            }
        }

        return productMatrix;
    }

    public static int[][] sum(int[][] matrix1, int[][] matrix2) {

        int[][] sumMatrix = new int[matrix1.length][matrix1[0].length];

        for (int i = 0; i < sumMatrix.length; i++) {
            for (int j = 0; j < sumMatrix[0].length; j++) {
                sumMatrix[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }

        return sumMatrix;
    }

    public static int[][] product(int[][] matrix1, int[][] matrix2) {

        int[][] productMatrix = new int[matrix1.length][matrix2[0].length];

        for (int i = 0; i < productMatrix.length; i++) {
            for (int j = 0; j < productMatrix[0].length; j++) {
                for (int n = 0; n < matrix1[0].length; n++) {
                    productMatrix[i][j] += matrix1[i][n] * matrix2[n][j];
                }
            }
        }

        return productMatrix;
    }
}
