import java.util.Scanner;

public class exercise3 {

    public static void main(String[] args) {

        int [] array;
        int arrayElementsSum;

        array = readArrayFromUser();
        arrayElementsSum = exercise3.sumArrayElements(array);

        System.out.print(arrayElementsSum);
    }

    /**
     * An auxiliary method to accelerate reading an array entered in the terminal.
     * @return An array inputed from the user.
     */
    public static int[] readArrayFromUser() {

        int[] array;
        int numberElementsArray;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter number of elements in array.");
        numberElementsArray = read.nextInt();
        array = new int[numberElementsArray];
        int i = 0;
        while (i < numberElementsArray) {
            System.out.println("Enter element number " + (i + 1) + ".");
            array[i] = read.nextInt();
            i++;
        }

        return array;
    }

    /**
     * A method to sum all the elements of a given array.
     * @param array An array of type integer.
     * @return The sum of all elements of the array, of type integer.
     */
    public static int sumArrayElements(int[] array) {

        int arrayElementsSum = 0;

        for (int j : array) {
            arrayElementsSum += j;
        }

        return arrayElementsSum;
    }
}
