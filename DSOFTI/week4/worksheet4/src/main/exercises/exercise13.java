import java.util.Scanner;

public class exercise13 {

    public static void main(String[] args) {

        int lines, columns;
        int[][] matrix;
        boolean isMatrixSquare;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter number of lines.");
        lines = read.nextInt();
        System.out.println("Enter number of columns.");
        columns = read.nextInt();
        matrix = exercise12.readMatrix(lines, columns);

        isMatrixSquare = isMatrixSquare(matrix);

        if (isMatrixSquare) {
            System.out.println("The matrix is a square.");
        }
        else {
            System.out.println("The matrix is NOT a square.");
        }
    }

    /**
     * A method to verify if a given matrix forms a square.
     * @param matrix A matrix of type Integer.
     * @return Returns true if the matrix forms a square.
     */
    public static boolean isMatrixSquare(int[][] matrix) {

        boolean isMatrixSquare = false;
        int countLineElements;

        countLineElements = exercise12.countLineElements(matrix);
        if (countLineElements == matrix.length) {
            isMatrixSquare = true;
        }

        return isMatrixSquare;
    }

}
