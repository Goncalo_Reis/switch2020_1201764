import java.util.Arrays;
import java.util.Scanner;

public class exercise7 {

    public static void main(String[] args) {

        int lowerLimit, upperLimit, number;
        int[] multiplesOfNumberArray;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter the lower limit of the interval.");
        lowerLimit = read.nextInt();
        System.out.println("Enter the upper limit of the interval.");
        upperLimit = read.nextInt();
        System.out.println("Enter number to get the multiples of.");
        number = read.nextInt();

        multiplesOfNumberArray = arrayMultiplesOfNumber(lowerLimit, upperLimit, number);

        System.out.println(Arrays.toString(multiplesOfNumberArray));
    }

    /**
     * A method that, given an interval, returns the count of multiples of a specified number in that interval.
     * @param lowerLimit The first number of the interval.
     * @param upperLimit The last number of the interval.
     * @param number The number to found multiples of.
     * @return The count of existing multiples of the given number in the interval.
     */
    public static int countMultiplesOfNumber(int lowerLimit, int upperLimit, int number) {

        int multiplesOfNumberCount = 0;

        for (int i = lowerLimit; i <= upperLimit; i++) {
            if (i % number == 0) {
                multiplesOfNumberCount ++;
            }
        }

        return multiplesOfNumberCount;
    }

    /**
     * A method that extracts the multiples of a specified number in a given interval.
     * @param lowerLimit The first number of the interval.
     * @param upperLimit The last number of the interval.
     * @param number The number to found multiples of.
     * @return An array with the multiples of the number present in the given interval.
     */
    public static int[] arrayMultiplesOfNumber(int lowerLimit, int upperLimit, int number) {

        int multiplesOfNumberCount = countMultiplesOfNumber(lowerLimit, upperLimit, number);
        int[] multiplesOfNumberArray = new int[multiplesOfNumberCount];

        int arrayIndex = 0;
        for (int i = lowerLimit; i <= upperLimit; i++) {
            if (i % number == 0) {
                multiplesOfNumberArray[arrayIndex] = i;
                arrayIndex ++;
            }
        }

        return multiplesOfNumberArray;
    }
}
