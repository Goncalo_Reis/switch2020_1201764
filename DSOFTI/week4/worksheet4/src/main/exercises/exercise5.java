import java.util.Scanner;

public class exercise5 {

    public static void main(String[] args) {

        long number;
        int[] evenDigits;
        int sumEvenDigits;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter number.");
        number = read.nextLong();

        evenDigits = arrayEvenDigits(number);
        sumEvenDigits = sumEvenDigits(number);

        System.out.print(sumEvenDigits + " (");
        for (int i = 0; i < evenDigits.length; i++) {
            if (i == (evenDigits.length - 1)) {
                System.out.print(evenDigits[i] + ")");
            }
            else {
                System.out.print(evenDigits[i] + "+");
            }
        }
    }

    /**
     * A method that, given a number, copies its even digits into an array.
     * @param number A given number, of type integer.
     * @return The array of even digits.
     */
    public static int[] arrayEvenDigits(long number) {

        int[] digits;
        int[] evenDigits;

        digits = exercise2.getDigitsIntoArray(number);
        evenDigits = exercise4.getEvenArray(digits);

        return evenDigits;
    }

    /**
     * A method that, given a number, extracts only the even digits and sums them together.
     * @param number A number of type integer.
     * @return The sum of the even digits.
     */
    public static int sumEvenDigits(long number) {

        int[] digits;
        int[] evenDigits;
        int sumEvenDigits;

        digits = exercise2.getDigitsIntoArray(number);
        evenDigits = exercise4.getEvenArray(digits);
        sumEvenDigits = exercise3.sumArrayElements(evenDigits);

        return sumEvenDigits;
   }
}
