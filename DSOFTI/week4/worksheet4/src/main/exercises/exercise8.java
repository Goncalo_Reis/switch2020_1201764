import java.util.Arrays;
import java.util.Scanner;

public class exercise8 {

    public static void main(String[] args) {

        int lowerLimit, upperLimit;
        int [] numbers;
        int[] multiplesOfNumbers;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter the lower limit of the interval.");
        lowerLimit = read.nextInt();
        System.out.println("Enter the upper limit of the interval.");
        upperLimit = read.nextInt();
        numbers = exercise3.readArrayFromUser();

        multiplesOfNumbers = arrayOfMultiplesOfNumbersInArray(lowerLimit, upperLimit, numbers);

        System.out.println(Arrays.toString(multiplesOfNumbers));
    }

    /**
     * A method that counts the number of shared multiples of a specified set of numbers in a given interval.
     * @param lowerLimit The first number of the interval.
     * @param upperLimit The last number of the interval.
     * @param numbers An array of numbers to found shared multiples of.
     * @return The count of the shared multiples in the interval.
     */
    public static int countMultiplesOfNumbersInArray(int lowerLimit, int upperLimit, int[] numbers) {

        int multiplesOfNumbersCount = 0;
        int numbersCount = numbers.length;

        for (int multiple = lowerLimit; multiple <= upperLimit; multiple++) {
            int multipleCheck = 0;
            for (int j : numbers)
                if (multiple % j == 0) {
                    multipleCheck ++;
                }
            if (multipleCheck == numbersCount) {
                multiplesOfNumbersCount ++;
            }
        }

        return multiplesOfNumbersCount;
    }

    /**
     * A method that extracts the shared multiples of a specified set of numbers in a given interval.
     * @param lowerLimit The first number of the interval.
     * @param upperLimit The last number of the interval.
     * @param numbers An array of numbers to found shared multiples of.
     * @return An array with the shared multiples in the interval.
     */
    public static int[] arrayOfMultiplesOfNumbersInArray(int lowerLimit, int upperLimit, int[] numbers) {

        int multiplesOfNumbersCount = countMultiplesOfNumbersInArray(lowerLimit, upperLimit, numbers);
        int[] multiplesOfNumbers = new int[multiplesOfNumbersCount];
        int multipleCheck;
        int numbersCount = numbers.length;

        int multiplesOfNumbersIndex = 0;
        for (int multiple = lowerLimit; multiple <= upperLimit; multiple++) {
            multipleCheck = 0;
            for (int j : numbers)
                if (multiple % j == 0) {
                    multipleCheck ++;
                }
            if (multipleCheck == numbersCount) {
                multiplesOfNumbers[multiplesOfNumbersIndex] = multiple;
                multiplesOfNumbersIndex ++;
            }
        }

        return multiplesOfNumbers;
    }

}
