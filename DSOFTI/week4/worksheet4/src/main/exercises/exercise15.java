import java.util.Arrays;
import java.util.Scanner;

public class exercise15 {

    public static void main(String[] args) {

        int[][] matrix, transposeMatrix;
        float[][] inverseMatrix;
        int[] noRepeated, primeNumbers, mainDiagonal, secondaryDiagonal;
        int lines, columns, lowestElement, highestElement, product;
        float averageElements;
        boolean isIdentityMatrix;

        Scanner read = new Scanner(System.in);
        System.out.println("Enter number of lines.");
        lines = read.nextInt();
        System.out.println("Enter number of columns.");
        columns = read.nextInt();
        matrix = exercise12.readMatrix(lines, columns);

        lowestElement = getLowestElement(matrix);
        highestElement = getHighestElement(matrix);
        averageElements = getAverage(matrix);
        product = getProduct(matrix);
        noRepeated = excludeRepeatedElements(matrix);
        primeNumbers = getPrimeNumbers(matrix);
        mainDiagonal = getMainDiagonal(matrix);
        secondaryDiagonal = getSecondaryDiagonal(matrix);
        isIdentityMatrix = isIdentityMatrix(matrix);
        inverseMatrix = inverseMatrix(matrix);
        transposeMatrix = transposeMatrix(matrix);


        System.out.println("The lowest element is " + lowestElement + ".");
        System.out.println("The highest element is " + highestElement + ".");
        System.out.println("The average is " + averageElements + ".");
        System.out.println("The product is " + product + ".");
        System.out.println("The set of elements without repetitions is " + Arrays.toString(noRepeated) + ".");
        System.out.println("The set of prime elements is " + Arrays.toString(primeNumbers) + ".");
        System.out.println("The main diagonal is " + Arrays.toString(mainDiagonal) + ".");
        System.out.println("The main diagonal is " + Arrays.toString(secondaryDiagonal) + ".");
        System.out.println("Identity matrix: " + isIdentityMatrix + ".");
        if (Float.isNaN(inverseMatrix[0][0])) {
            System.out.println("This is a singular matrix, so it does not have an inverse matrix.");
        }
        else {
            System.out.println("The inverse matrix is:");
            for (float[] row : inverseMatrix) {
                System.out.println(Arrays.toString(row));
            }
        }
        System.out.println("The transpose matrix is:");
        for (int[] row : transposeMatrix) {
            System.out.println(Arrays.toString(row));
        }
    }

    /**
     * A method to return the lowest-value element of a matrix.
     * @param matrix A matrix of type integer.
     * @return The lowest-value element of the matrix.
     */
    public static int getLowestElement(int[][] matrix) {

        int lowestElement = matrix[0][0];

        for (int[] element : matrix) {
            for (int j : element) {
                if (lowestElement > j) {
                    lowestElement = j;
                }
            }
        }

        return lowestElement;
    }

    /**
     * A method to return the highest-value element of a matrix.
     * @param matrix A matrix of type integer.
     * @return The highest-value element of the matrix.
     */
    public static int getHighestElement(int[][] matrix) {

        int highestElement = matrix[0][0];

        for (int[] element : matrix) {
            for (int j : element) {
                if (highestElement < j) {
                    highestElement = j;
                }
            }
        }

        return highestElement;
    }

    /**
     * A method to return the average of the elements in a matrix.
     * @param matrix A matrix of type integer.
     * @return The average of the matrix elements.
     */
    public static float getAverage(int[][] matrix) {

        float averageElements;
        int sumAllElements = 0;
        int countElements = 0;

        for (int[] elements : matrix) {
            for (int j : elements) {
                sumAllElements += j;
                countElements++;
            }
        }
        averageElements = (float) sumAllElements / countElements;

        return averageElements;
    }

    /**
     * A method to return the product of all the elements in a matrix.
     * @param matrix A matrix of type integer.
     * @return The product of the elements of the matrix.
     */
    public static int getProduct(int[][] matrix) {

        int product = matrix[0][0];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (i != 0 || j != 0) {
                    product *= matrix[i][j];
                }
            }
        }

        return product;
    }

    /**
     * A method that returns the count of the repeated elements in a matrix.
     * @param matrix A matrix of type integer.
     * @return The count of the repeated elements.
     */
    public static int repeatedElementsCount(int[][] matrix) {

        int repeatCount = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (i != (matrix.length - 1) || j != (matrix[0].length - 1)) {
                    int eventCount = 0;
                    for (int i2 = 0; i + i2 < matrix.length && eventCount < 2; i2++) {
                        if (i == i + i2) {
                            for (int j2 = 0; j + j2 < matrix[0].length && eventCount < 2; j2++) {
                                if (matrix[i][j] == matrix[i + i2][j + j2]) {
                                    eventCount++;
                                    if (eventCount == 2) {
                                        repeatCount++;
                                    }
                                }
                            }
                        }
                        else {
                            for (int j2 = 0; j2 < matrix[0].length && eventCount < 2; j2++) {
                                if (matrix[i][j] == matrix[i + i2][j2]) {
                                    eventCount++;
                                    if (eventCount == 2) {
                                        repeatCount++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return repeatCount;
    }

    /**
     * A method that return an array of the indexes of repeated elements in a matrix.
     * @param matrix A matrix of type integer.
     * @return An array with the indexes of the repeated elements.
     */
    public static int[][] repeatedElementsIndexes(int[][] matrix) {

        int repeatCount = repeatedElementsCount(matrix);
        int[][] repeatedIndexes = new int[2][repeatCount];

        int index = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (i != (matrix.length - 1) || j != (matrix[0].length - 1)) {
                    int eventCount = 0;
                    for (int i2 = 0; i + i2 < matrix.length && eventCount < 2; i2++) {
                        if (i == i + i2) {
                            for (int j2 = 0; j + j2 < matrix[0].length && eventCount < 2; j2++) {
                                if (matrix[i][j] == matrix[i + i2][j + j2]) {
                                    eventCount++;
                                    if (eventCount == 2) {
                                        repeatedIndexes[0][index] = i + i2;
                                        repeatedIndexes[1][index] = j + j2;
                                        index++;
                                    }
                                }
                            }
                        }
                        else {
                            for (int j2 = 0; j2 < matrix[0].length && eventCount < 2; j2++) {
                                if (matrix[i][j] == matrix[i + i2][j2]) {
                                    eventCount++;
                                    if (eventCount == 2) {
                                        repeatedIndexes[0][index] = i + i2;
                                        repeatedIndexes[1][index] = j2;
                                        index++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return repeatedIndexes;
    }

    /**
     * A method that extracts the elements of a matrix without value repetitions.
     * @param matrix A matrix of type integer.
     * @return An array without repeated elements.
     */
    public static int[] excludeRepeatedElements(int[][] matrix) {

        int[][] repeatedElementsIndexes = repeatedElementsIndexes(matrix);
        int nonRepeatedCount = (matrix.length * matrix[0].length) - repeatedElementsIndexes[0].length;
        int[] nonRepeated = new int[nonRepeatedCount];

        int index = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                int eventCount = 0;
                for (int z = 0; z < repeatedElementsIndexes[0].length; z++) {
                    if (i != repeatedElementsIndexes[0][z] || j != repeatedElementsIndexes[1][z]) {
                        eventCount++;
                    }
                }
                if (eventCount == repeatedElementsIndexes[0].length) {
                    nonRepeated[index] = matrix[i][j];
                    index++;
                }
            }
        }

        return nonRepeated;
    }

    /**
     * A method to verify if a number is prime.
     * @param number A number of type Integer.
     * @return Returns true if number is a prime.
     */
    public static boolean isPrimeNumber(int number) {

        boolean isPrimeNumber = false;

        if (number != 1) {
            int[] multiples = new int[number - 1];
            int combinations = 0;
            int loop = 0;
            do {
                multiples[loop] = (number) - loop;
                combinations += (number - 1) - loop;
                loop++;
            } while (number - 1 - loop > 0);
            int eventCount = 0;
            for (int i = 0; i < multiples.length; i++) {
                for (int j = 0; i + j < multiples.length; j++) {
                    if (multiples[i] * multiples[i + j] != number) {
                        eventCount++;
                    }
                }
            }
            if (eventCount == combinations) {
                isPrimeNumber = true;
            }
        }

        return isPrimeNumber;
    }

    /**
     * A method to count prime numbers in a matrix.
     * @param matrix A matrix of type Integer.
     * @return The count of the prime numbers in the matrix.
     */
    public static int countPrimeNumbers(int[][]matrix) {

        int countPrimeNumbers = 0;

        for (int[] line : matrix) {
            for (int element : line) {
                if (isPrimeNumber(element)) {
                    countPrimeNumbers++;
                }
            }
        }

        return countPrimeNumbers;
    }

    /**
     * A method to extract the prime numbers in a matrix.
     * @param matrix A matrix of type Integer.
     * @return The array with only the prime numbers.
     */
    public static int[] getPrimeNumbers(int[][]matrix) {

        int primeNumbersCount = countPrimeNumbers(matrix);
        int[] primeNumbers = new int[primeNumbersCount];

        int index = 0;
        for (int[] line : matrix) {
            for (int element : line) {
                if (isPrimeNumber(element)) {
                    primeNumbers[index] = element;
                    index++;
                }
            }
        }

        return primeNumbers;
    }

    /**
     * A method that counts the number of elements that constitute the main diagonal of a matrix.
     * @param matrix A matrix of type Integer.
     * @return The count of the number of elements in the main diagonal.
     */
    public static int countMainDiagonal(int[][] matrix) {

        int mainDiagonalCount = 0;

        for (int line = 0; line < matrix.length; line++) {
            for (int column = 0; column < matrix[0].length; column++) {
                if (line == column) {
                    mainDiagonalCount++;
                }
            }
        }

        return mainDiagonalCount;
    }

    /**
     * A method that extracts the elements that constitute the main diagonal of a matrix.
     * @param matrix A matrix of type Integer.
     * @return An array of the elements in the main diagonal.
     */
    public static int[] getMainDiagonal(int[][] matrix) {

        int mainDiagonalCount = countMainDiagonal(matrix);
        int[] mainDiagonal = new int[mainDiagonalCount];

        int index = 0;
        for (int line = 0; line < matrix.length; line++) {
            for (int column = 0; column < matrix[0].length; column++) {
                if (line == column) {
                    mainDiagonal[index] = matrix[line][column];
                    index++;
                }
            }
        }

        return mainDiagonal;
    }

    /**
     * A method that counts the number of elements that constitute the secondary diagonal of a matrix.
     * @param matrix A matrix of type Integer.
     * @return The count of the number of elements in the secondary diagonal.
     */
    public static int countSecondaryDiagonal(int[][] matrix) {

        int secondaryDiagonalCount = 0;

        for (int line = 0; line < matrix.length; line++) {
            for (int column = 0; column < matrix[0].length; column++) {
                if (line + column == matrix[0].length - 1) {
                    secondaryDiagonalCount++;
                }
            }
        }

        return secondaryDiagonalCount;
    }

    /**
     * A method that extracts the elements that constitute the secondary diagonal of a matrix.
     * @param matrix A matrix of type Integer.
     * @return An array of the elements in the secondary diagonal.
     */
    public static int[] getSecondaryDiagonal(int[][] matrix) {

        int secondaryDiagonalCount = countMainDiagonal(matrix);
        int[] secondaryDiagonal = new int[secondaryDiagonalCount];

        int index = 0;
        for (int line = 0; line < matrix.length; line++) {
            for (int column = 0; column < matrix[0].length; column++) {
                if (line + column == matrix[0].length - 1) {
                    secondaryDiagonal[index] = matrix[line][column];
                    index++;
                }
            }
        }

        return secondaryDiagonal;
    }

    /**
     * A method that verifies if a matrix is an identity matrix.
     * @param matrix A matrix of type Integer.
     * @return Returns true if the matrix is a identity matrix.
     */
    public static boolean isIdentityMatrix(int[][] matrix) {

        boolean isIdentityMatrix = true;

        for (int line = 0; line < matrix.length; line++) {
            for (int column = 0; column < matrix[0].length; column++) {
                if (line == column) {
                    if (matrix[line][column] != 1) {
                        isIdentityMatrix = false;
                    }
                }
                else {
                    if (matrix[line][column] != 0) {
                        isIdentityMatrix = false;
                    }
                }
            }
        }

        return isIdentityMatrix;
    }

    /**
     * A method to reduce a matrix according to a reference point.
     * @param matrix A matrix of type Integer.
     * @param line The row to be eliminated.
     * @param column The column to be eliminated.
     * @return The reduced matrix.
     */
    public static int[][] reducedMatrix(int[][] matrix, int line, int column) {

        int[][] reducedMatrix = new int[matrix.length - 1][matrix[0].length - 1];

            int z = 0;
            for (int i = 0; i < matrix.length; i++) {
                if (i != line) {
                    int n = 0;
                    for (int j = 0; j < matrix[0].length; j++) {
                        if (j != column) {
                            reducedMatrix[i - z][j - n] = matrix[i][j];
                        } else {
                            n++;
                        }
                    }
                } else {
                    z++;
                }
            }

            return reducedMatrix;
    }

    /**
     * A method to get the determinant of a matrix.
     * @param matrix A matrix of type Integer.
     * @return The determinant of the matrix.
     */
    public static int determinant(int[][] matrix) {

        int line = 0;
        int determinant = 0;

        if (matrix.length == 1) {
            determinant = matrix[0][0];
        }
        else if (matrix.length == 2) {
            determinant = (matrix[0][0] * matrix[1][1]) - (matrix[1][0] * matrix[0][1]);
        }
        else {
            int n = 1;
            while (n <= matrix[0].length) {
                int column = matrix[0].length - n;
                if (column % 2 == 0) {
                    determinant += + (matrix[line][column]) * determinant(reducedMatrix(matrix, line, column));
                }
                else {
                    determinant += - (matrix[line][column]) * determinant(reducedMatrix(matrix, line, column));
                }
                n++;
            }
        }

        return determinant;
    }

    /**
     * A method to find the minors matrix of a matrix.
     * @param matrix A matrix of type Integer.
     * @return The minors matrix.
     */
    public static int[][] minorsMatrix(int[][] matrix) {

        int[][] minorsMatrix = new int[matrix.length][matrix[0].length];

        for (int i = 0; i < minorsMatrix.length; i++) {
            for (int j = 0; j < minorsMatrix.length; j++) {
                minorsMatrix[i][j] = determinant(reducedMatrix(matrix, i, j));
            }
        }

        return minorsMatrix;
    }

    /**
     * A method to find the cofactors matrix of a given matrix.
     * @param matrix A matrix of type Integer.
     * @return The cofactors matrix.
     */
    public static int[][] cofactorsMatrix(int[][] matrix) {

        int[][] minorsMatrix = minorsMatrix(matrix);
        int[][] cofactorsMatrix = new int[minorsMatrix.length][minorsMatrix[0].length];

        int n = 1;
        for (int line = 0; line < cofactorsMatrix.length; line++) {
            if (line != 0 && minorsMatrix[0].length % 2 == 0) {
                n *= -1;
            }
            for (int column = 0; column < cofactorsMatrix.length; column++) {
                cofactorsMatrix[line][column] = minorsMatrix[line][column] * n;
                n *= -1;
            }
        }

        return cofactorsMatrix;
    }

    /**
     * A method to transpose a matrix.
     * @param matrix A matrix of type Integer.
     * @return The transposed matrix.
     */
    public static int[][] transposeMatrix(int[][] matrix) {

        int[][] transposeMatrix = new int[matrix[0].length][matrix.length];

        for (int i = 0; i < transposeMatrix[0].length; i++) {
            for (int j = 0; j < transposeMatrix.length; j++) {
                transposeMatrix[i][j] = matrix[j][i];
            }
        }

        return transposeMatrix;
    }

    /**
     * A method to find the adjoint matrix of a matrix.
     * @param matrix A matrix of type Integer.
     * @return The adjoint matrix.
     */
    public static int[][] adjointMatrix(int[][] matrix) {

        int[][] cofactorsMatrix = cofactorsMatrix(matrix);
        int[][] adjointMatrix = transposeMatrix(cofactorsMatrix);

        return adjointMatrix;
    }

    /**
     * The method to find the inverse of a matrix.
     * @param matrix A matrix of type Integer.
     * @return The inverse matrix.
     */
    public static float[][] inverseMatrix(int[][] matrix) {

        int[][] adjointMatrix = adjointMatrix(matrix);
        float[][] inverseMatrix = new float[adjointMatrix.length][adjointMatrix[0].length];
        int determinant = determinant(matrix);

        for (int i = 0; i < inverseMatrix.length; i++) {
            for (int j = 0; j < inverseMatrix[0].length; j++) {
                inverseMatrix[i][j] = (float) (1.0 / determinant) * adjointMatrix[i][j];
            }
        }

        return inverseMatrix;
    }
}
